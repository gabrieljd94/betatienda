<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	//CONTROLADOR PARA MOSTRAR TODOS LOS PRODUCTOS DE LA TIENDA
	class Productos extends CI_Controller
	{
		function __construct()
		{
			parent::__construct(); 

			$this->load->model("Local_model", "localModel");
			$this->load->model("Cotizacion_model", "cotizacionModel");
			$this->load->model("Usuario_model", "usuarioModel");
        }

        public function index()
        {
        	$data = $this->acl->load_datos("no", false);

        	$this->load->library("cart");

        	$data['pagina']['titulo'] = 'Portal de compras por mayor';

        	if(count($data['tienda']) > 0)
        	{
        		$data['producto'] = $this->productoModel->productos("WHERE prt.Tie_IdTienda = ".$data['tienda']->Tie_IdTienda." ORDER BY pro.Pro_Oferta desc");

		        if($data['producto'])
		        {
		            $data['fotos'] = $this->productoModel->get_fotos_producto();
		            $data['SKU'] = $this->productoModel->get_sku();
		            $data['filtro'] = $this->productoModel->get_filtro();
		            $data['carrito'] = $this->cart->contents();  
		        } 
		        else 
		        {
		    	    $data['noproducto2'] = "no hay producto";
	        	}
        	}	        

      		$this->twig->parse('tienda/inicio.twig', $data);
        }

        //AGREGAR PRODUCTO A LA TIENDA
        public function add(){
	    $this->load->library("cart");
		    	
		    $sizePost = sizeof($_POST["product_id"]);

		    if ($this->input->is_ajax_request()) {
		    	for ($i=0; $i < $sizePost; $i++) {
		      	$data = array(
		       	"id"  => $_POST["product_id"][$i],
		       	"name"  => $_POST["product_name"][$i],
		      	"qty"  => $_POST["quantity"][$i],
		       	"price"  => $_POST["product_price"][$i],
		       	"variacion" => $_POST["variacion"][$i],
		       	"unidad" => $_POST["unidad"][$i],
		       	"img" => $_POST["img"][$i],
		       	"color" => $_POST["color"][$i],
		       	"tipovariacion" => $_POST["tipovariacion"][$i],
		       	"idtienda" => $_POST["idtienda"][$i]
		    		);
		    	$this->cart->insert($data); //return rowid 
		    	}
	      	}
		    	echo $this->view();
	    }

	    //CARGAR VISTA DEL CARRRITO DE COTIZACION
	    public function load(){
	      echo $this->view();
	    }

	    //ELIMINAR UN ELEMENTO DEL CARRITO DE COTIZACION 
	    public function remove(){
	    $this->load->library("cart");
	    $row_id = $_POST["row_id"];
		    $data = array(
		    'rowid'  => $row_id,
		    'qty'  => 0
		    );
	    $this->cart->update($data);
	    echo $this->view();
	    }

	    //ELIMINAR TODO DEL CARRITO DE COTIZACION
	    public function clear(){
		    $this->load->library("cart");
		    $this->cart->destroy();
		    echo $this->view();
	    }
	     

	    //ESTRUCTURA DE LA INFORMACION DEL CARRITO DE COTIZACION
	    public function view(){
		    $this->load->library("cart");
	        $data = $this->acl->load_datos("no", false);
		    $output = '';
		    $output .= '
		      <div>
		     	<div class="carroContent center">';
		    $count = 0;
		    foreach($this->cart->contents() as $items){
		    $count++;
			$output .='
		       	<div class="row">         
		            <div class="col s2 l3 valign-wrapper"><img style="border-radius:5px; margin: 0 auto;" src="'.$data['ruta_img'].$items["img"].'" height="50"></div>
		            <div class="col s4 l6">'.$items["name"].'<br><span class="varCartTipo">'.$items["tipovariacion"]." ".$items["variacion"]." - ".ucfirst(strtolower($items["color"])).'</span></div>
		            <div class="col s3 l2">'.$items["qty"].'<br><span class="undCartTipo">'.$items["unidad"].'</span></div>
		            <div class="col s3 l1"><a class="btn red lighten-1 remove_inventory" id="'.$items["rowid"].'"><i class="material-icons">clear</i></a></div>               
		        </div>
		       ';
		    }
		    $output .= '</div>
		    <div class="center">
		    <button class="waves-effect waves-light btn red clear_cart"><i class="material-icons left">delete_sweep</i>Vaciar</button>
		    <a href="'.base_url().'tienda/productos/pedido" class="btn">Cotizar</a>
		    </div></div>';

		   if($count == 0){
		    	$output = '<div class="row">
		    				<div class="center col s12 m12 l12" style="margin-top: 20px;">
		    					<img src="'.$data['ruta_img'].'emptyCart.png'.'" height="150"></img>
		    				</div>
		    				<div class="center col s12 m12 l12">
		    					<h5>Sin productos agregados</h5>
		    					<p class="enfasisB">No haz agregado productos para cotizar</p>
		    				</div>
		    				<div class="center col s12 m12 l12">
		    					<a class="btn center modal-close">Ir a comprar</a>
		    				</div>
		    				
		    				</div>';
		    }
		    return $output;
	   	}


	    public function resumen(){
		  //$data = $this->_load();
		  $data = $this->acl->load_datos("no", false);
	      $data['pagina']['titulo'] = 'Resumen';
	      $this->load->library("cart");
	      $data['carrito'] = $this->cart->contents();
	      $this->twig->parse('tienda/resumen.twig', $data);
		}

		public function pedido(){
// 		 $data = $this->_load();
		  $data = $this->acl->load_datos("no", false);
	      $data['pagina']['titulo'] = 'Pedido';
	      $this->load->library("cart");
	      $data['carrito'] = $this->cart->contents();
	      $data['region'] = $this->localModel->region();
	       	if (isset($data['usuario'])) {
	      	$data['usuario_inf'] = $this->usuarioModel->get_usuario($data['usuario']['id_usuario']);
	      	}
	      $this->twig->parse('tienda/pedido.twig', $data);
		}

		public function dp($Pro_IdProducto){
		 $data = $this->acl->load_datos("no", false);
		  $Pro_IdProducto = (int)$Pro_IdProducto;
	      $data['pagina']['titulo'] = 'Detalle del producto';
	      $this->load->library("cart");
	      $data['carrito'] = $this->cart->contents();
	      $data['producto'] = $this->productoModel->productos("WHERE pro.Pro_IdProducto = '".$Pro_IdProducto."'");
	      $data['fotos'] = $this->productoModel->get_fotos_producto($Pro_IdProducto);
	      $data['SKU'] = $this->productoModel->get_sku($Pro_IdProducto);
	      $data['comentarios_respuestas'] = $this->productoModel->get_comentarios_respuestas($Pro_IdProducto);
	      $data['btn_comentario'] = 1;
	     
	      $this->twig->parse('tienda/load/dp.twig', $data);
		}

		public function comentario(){
		 // $data = $this->_load();
			if ($this->input->is_ajax_request()) {
				$comentario = $this->input->post('comentario');				
				$Pro_IdProducto = $this->input->post('Pro_IdProducto');				
				$Usu_IdUsuario = $this->input->post('Usu_IdUsuario');				
				$registro = $this->productoModel->addComentario($comentario,$Pro_IdProducto,$Usu_IdUsuario);
		
				echo $Pro_IdProducto;
			
			}
		}		

		public function comentarioVer(){
	      if($this->input->is_ajax_request())
	      {
	        $data = $this->acl->load_datos("no", false);
		  $Pro_IdProducto = (int)$this->input->post('data');
		    // $data['subcategoria'] = $this->input->post('datos');
	      $data['comentarios'] = $this->productoModel->get_comentarios($Pro_IdProducto);
	        // $this->twig->parse('usuario/model/selSubcategoria.twig', $data);	        
	      $this->twig->parse('tienda/load/comentario.twig', $data);
	      }
	      else
	      {
	        show_404();
	      }
		}

		//CAPTURA DE PROVINCIA DESDE BASE DE DTATOS SEGÚN EL ID DE LA REGION
		public function load_provincia()
		{
		  
			// $data = $this->_load();
			if ($this->input->is_ajax_request()) {
				$idRegion = $this->input->post('idRegion');				
					$data['provincia'] = $this->localModel->provincia($idRegion);
					print_r(json_encode($data['provincia']));
			}
      	  
		}

		//VISTA DEL SELECT CON LAS PROVINCIAS CARGADAS SEGÚN EL ID DE LA REGION
		public function Select_Provincia()
		{
			if($this->input->is_ajax_request())
			{
// 			$data = $this->_load();
			$data['provincia'] = $this->input->post('datos');

			$this->twig->parse('usuario/model/selProvincia.twig', $data);

			}
			else
			{
			show_404();
			}
		}

		//CAPTURA DE DISTRITO DESDE BASE DE DATOS SEGÚN EL ID DE LA PROVINCIA
		public function load_distrito()
		{
		  
// 			$data = $this->_load();
			if ($this->input->is_ajax_request()) {
				$idProvincia = $this->input->post('idProvincia');
				$idRegion = $this->input->post('idRegion');
				
					$data['distrito'] = $this->localModel->distrito($idProvincia,$idRegion);

					print_r(json_encode($data['distrito']));
			}
			  
		}

		//VISTA DEL SELECT DISTRITO SEGÚN EL ID DE LA PROVINCIA
	    public function Select_Distrito()
	    {
	      if($this->input->is_ajax_request())
	      {
	       // $data = $this->_load();
		    $data['distrito'] = $this->input->post('datos');

	        $this->twig->parse('usuario/model/selDistrito.twig', $data);
	        
	      }
	      else
	      {
	        show_404();
	      }
	    }

		//VISTA DEL SELECT DISTRITO SEGÚN EL ID DE LA PROVINCIA
	    public function filtro_producto_variacion()
	    {
	      if($this->input->is_ajax_request())
	      {
	       	// $data = $this->_load();
	      	$data = $this->acl->load_datos("no", false);
	        $idVariacion = $this->input->post('idVariacion');
		    $data['producto'] = $this->productoModel->productos_filtro($idVariacion,$data['tienda']->Tie_IdTienda);
        	$data['SKU'] = $this->productoModel->get_sku();
        	$data['fotos'] = $this->productoModel->get_fotos_producto();
        	$data['colores'] = $this->configuracionModel->get_coloresProducto($data['tienda']->Tie_IdTienda);
	        $this->twig->parse('tienda/load/listProducto.twig', $data);
	      }
	      else
	      {
	        show_404();
	      }
	    }	    

	    public function bp($nombre_producto)
	    {
	        //$data = $this->_load();
	         $this->load->library("cart");
	        $data = $this->acl->load_datos("no", false);
	        $nombre_producto = $nombre_producto;
     	 	$data['dtienda'] = $this->tiendaModel->get_detalleTienda($data['tienda']->Tie_IdTienda);
      		$data['local'] = $this->tiendaModel->get_local_info($data['tienda']->Tie_IdTienda);
		    $data['producto'] = $this->productoModel->filtro_producto_nombre($nombre_producto,$data['tienda']->Tie_IdTienda);
		    if($data['producto'])
	        {
	            $data['fotos'] = $this->productoModel->get_fotos_producto();
	            $data['SKU'] = $this->productoModel->get_sku();
	            $data['variaciones'] = $this->productoModel->get_variaciones_filtro($data['tienda']->Tie_IdTienda);
	            $data['carrito'] = $this->cart->contents();  
	        }
	        else 
	        {
	          $data['noproducto2'] = "no hay producto";
	        }       

	      	$this->twig->parse('tienda/inicio.twig', $data);	        
	
	    }

	    public function enviarPedido()
	    {			    
	   		if ($this->input->is_ajax_request()) 
			{
				$data = $this->acl->load_datos("no", "si");

				$arrData = $this->input->post('arrData');
				$this->load->library("cart");
				$data['carrito'] = $this->cart->contents();

				$array_id_tienda = array();

				foreach ($data['carrito'] as $cart) 
				{
					$array_id_tienda[] = $cart['idtienda'];	
				}

				$array_id_tienda = array_unique($array_id_tienda);
				
				$tienda_usuario = $data['usuario']['id_tienda'];
				$Usu_IdUsuario = $data['usuario']['id_usuario'];				

				$Ped_IdPedido = $this->cotizacionModel->enviarPedido($arrData, $data['carrito'], $Usu_IdUsuario, $tienda_usuario, $array_id_tienda);

				if (is_null($Ped_IdPedido))
				{
					echo 0;
				}
				else 
				{
					$this->enviarPedidoCliente($arrData[2], $arrData[0], $data['carrito'], $data['ruta_img']);

					foreach ($array_id_tienda as $Tie_IdTienda)
					{
						$tienda = $this->cotizacionModel->get_datos_correo($Tie_IdTienda);
						$this->enviarPedidoTienda($Tie_IdTienda, $tienda->Usu_Correo, $tienda->Per_Nombre, $arrData[0], $data['carrito'], $Ped_IdPedido, $tienda->Tie_Subdominio, $data['ruta_img'], $data['dominio']);
					}
					$this->cart->destroy();
					echo 1;
				}
			}
		}

		public function enviarPedidoCliente($correo, $nombreCliente, $dataCarrito, $ruta_img)
		{
			$this->load->library('correo');
           	
            $datos['nombre_usuario'] = $nombreCliente;
            $datos['dataCarrito'] = $dataCarrito;
            $datos['ruta_img'] = $ruta_img;

            $contenido_correo = $this->twig->parse('correo/cotizacion_cliente.twig', $datos, true);

            $this->correo->enviar($correo, "Nueva cotización solicitada", $contenido_correo);
		}

		public function enviarPedidoTienda($Tie_IdTienda, $correo, $nombreUsuario, $nombreCliente, $dataCarrito, $idPedido, $Tie_Subdominio, $ruta_img, $dominio)
		{
			$this->load->library('correo');

            $datos['Tie_IdTienda'] = $Tie_IdTienda;
            $datos['nombre_usuario'] = $nombreUsuario;
            $datos['nombre_cliente'] = $nombreCliente;
            $datos['dominio'] = $dominio;
           
            $datos['dataCarrito'] = $dataCarrito;
            $datos['idPedido'] = $idPedido;
            $datos['Tie_Subdominio'] = $Tie_Subdominio;
            $datos['ruta_img'] = $ruta_img;

            $contenido_correo = $this->twig->parse('correo/cotizacion_tienda.twig', $datos, true);

            $this->correo->enviar($correo, "Nueva cotización solicitada", $contenido_correo);
		}
    }

?>