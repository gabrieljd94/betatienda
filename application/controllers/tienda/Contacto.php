<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	//CONTROLADOR PARA VISTA CONTACTO (TRAER NUMERO Y DIRECCION DE LOCALES)

	class Contacto extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();

			$this->load->model("Tienda_model", "tiendaModel");
			$this->load->model("Configuracion_model", "configuracionModel");
			$this->load->model("Local_model", "localModel");
        }		

		//CONTACTO DE LA TIENDA DIRECCION

        public function index()
        {
		    $data = $this->acl->load_datos("no", false);

			$data['pagina']['titulo'] = 'Contacto';

			// $data['tienda'] = $this->tiendaModel->get_tienda_info();

			$data['local'] = $this->tiendaModel->get_local_info($data['tienda']->Tie_IdTienda);

			$this->twig->parse('tienda/contacto.twig', $data);
		}

	    // //INFORMACIÓN GENERAL DE LA TIENDA

	    // public function _load(){	      

		   //  $data['configuracion'] = $this->configuracionModel->configuracion();

		   //  $data['tienda'] = $this->tiendaModel->get_tienda_info();

		   //  return $data;

	    // }

      

    }



?>