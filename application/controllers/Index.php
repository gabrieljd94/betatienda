<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Index extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
		}

    public function index()
    {
      $data = $this->acl->load_datos("no", false);
      // print_r($data);
      // exit();
      $data['pagina']['titulo'] = 'Tienda';
      // if(count($data['tienda']) > 0)
      // {
      $data['dtienda'] = $this->tiendaModel->get_detalleTienda($data['tienda']->Tie_IdTienda);
            // }
      $this->load->library("cart");
      $data['pagina']['titulo'] = 'Portal de compras por mayor';
      $data['local'] = $this->tiendaModel->get_local_info($data['tienda']->Tie_IdTienda);
      // if(count($data['tienda']) > 0)      // {
        $data['producto'] = $this->productoModel->productos("WHERE prt.Tie_IdTienda = ".$data['tienda']->Tie_IdTienda." ORDER BY pro.Pro_Oferta desc");
        if($data['producto'])
        {
            $data['fotos'] = $this->productoModel->get_fotos_producto();
            $data['SKU'] = $this->productoModel->get_sku();        
            $data['filtro'] = $this->productoModel-> get_filtro_tienda($data['tienda']->Tie_IdTienda);
            $data['carrito'] = $this->cart->contents();  
            $data['colores'] = $this->configuracionModel->get_coloresProducto($data['tienda']->Tie_IdTienda);
        }
        else 
        {
          $data['noproducto2'] = "no hay producto";
        }
      // } 
      $this->twig->parse('tienda/inicio.twig', $data);
    }

    function cerrar_sesion()
    {
      $this->session->sess_destroy();
      header("Location: https://pormayor.pe/usuario/login");
    }
	}

?>