<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Index extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model("Producto_model", "productoModel");
			$this->load->model("Tienda_model", "tiendaModel");
			$this->load->model("Configuracion_model", "configuracionModel");
			$this->load->model("Local_model", "localModel"); 
			$this->load->model("Cotizacion_model", "cotizacionModel"); 
        }

		public function index()
		{
 			$data = $this->acl->load_datos("no", "no", "comprador");
	        $data['pagina']['titulo'] = 'Hola!';
	        $this->twig->parse('comprador/index.twig', $data);
		}

		public function pedidos() 
		{
 			$data = $this->acl->load_datos("no", "no", "comprador");
 			$data['pagina']['titulo'] = 'Mis pedidos';
	        // print_r($data);
	        $data['pedidos'] = $this->cotizacionModel->get_pedidos_usuario($data['usuario']['id_usuario']);
	        $data['dpedidos'] = $this->cotizacionModel->get_pedidos_usuario_detalle($data['usuario']['id_usuario']);
	        //print_r($data['pedidos']);
	        $data['cotizacion'] = $this->cotizacionModel->get_cotizacion_usuario($data['usuario']['id_usuario']);
	        //print_r($data['cotizacion']);
	        $this->twig->parse('comprador/pedidos.twig', $data);
		}

		public function dCotizacion($Cot_IdCotizacion)
		{
 			$Cot_IdCotizacion = (int)$Cot_IdCotizacion;

 			$data = $this->acl->load_datos("no", "no", "comprador");

	        $data['pagina']['titulo'] = 'Cotización';

	        $data['dcotizacion'] = $this->cotizacionModel->get_dcotizacion($Cot_IdCotizacion);
	        $data['Cot_IdCotizacion'] = $Cot_IdCotizacion;

	        if($data['dcotizacion'][0]->Cot_Estado == 1)
	        {
	        	$this->twig->parse('comprador/dCotizacion.twig', $data);
	        }
	        else
	        {
	        	$cotizacion = $this->cotizacionModel->get_estado_cotizacion_venta($Cot_IdCotizacion);

	        	redirect('comprador/compra/detalle/'.$cotizacion->Ven_IdVenta);
	        }
		}

		public function respuesta_comentario()
		{
 			$data = $this->acl->load_datos("no", "no", false);
	        $data['pagina']['titulo'] = 'Respuestas';
	        $data['respuesta_comentario'] = $this->cotizacionModel->get_comentarios_usuario($data['usuario']['id_usuario']);
	        //print_r($data['respuesta_comentario']);
	        $data['producto_img'] = $this->cotizacionModel->get_comentarios_usuario_img($data['usuario']['id_usuario']);
	        $this->twig->parse('comprador/rc.twig', $data);
		}

		//ajax

		public function ajax_detalle_pedido()
		{
			if($this->input->is_ajax_request())
			{
				$data = $this->acl->load_datos("no", "no", "comprador");

				$Ped_IdPedido = $this->input->post('Ped_IdPedido');

				$data['detalle_pedido'] = $this->cotizacionModel->get_detalle_pedido($Ped_IdPedido);
				$data['tiendas_pedido'] = $this->cotizacionModel->get_tiendas_pedido($Ped_IdPedido);

				$this->twig->parse('comprador/ajax/detalle_pedido.twig', $data);
			}
			else
			{
				show_404();
			}
		}
	}
?>