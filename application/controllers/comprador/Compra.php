<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Compra extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			// $this->load->model("Producto_model", "productoModel");
			$this->load->model("Venta_model", "ventaModel");
        }

        public function index()
        {
    		$data = $this->acl->load_datos("no", "no", "comprador");

    		$data['pagina']['menu'] = 'gestion';
	      	$data['pagina']['submenu'] = 'ventas';

    		$Usu_IdUsuario = $data['usuario']['id_usuario'];
    	
        	$data['pagina']['titulo'] = 'Mis compras';
		
			$this->load->model("Venta_Detalle_model", "ventaDetalleModel");

	        $data['ventas'] = $this->ventaModel->get_ventas($data['usuario']['id_usuario']);
	        $data['ventas_detalle'] = $this->ventaDetalleModel->get_detalle_venta_x_usuario($data['usuario']['id_usuario']);

	        $this->twig->parse('comprador/compras.twig', $data);
        }

        public function detalle($Ven_IdVenta=false)
        {
        	if($Ven_IdVenta)
        	{
        		$Ven_IdVenta = (int)$Ven_IdVenta;
	 			$data = $this->acl->load_datos("no", "no", "comprador");
		        $data['pagina']['titulo'] = 'Resumen de compra';

		        $this->load->model("Venta_Detalle_model", "ventaDetalleModel");
		        $data['venta'] = $this->ventaModel->get_venta($Ven_IdVenta);
		       	$data['venta_detalle'] = $this->ventaDetalleModel->get_detalle_venta($Ven_IdVenta);
		       	$data['venta_info'] = $this->ventaModel->get_venta_info($Ven_IdVenta);
		        $data['idventa'] = $Ven_IdVenta;
		        $data['sicalifico'] = $this->ventaModel->get_calificacion($Ven_IdVenta);
		        $this->twig->parse('comprador/compra_detalle.twig', $data);
        	}
        	else
        	{
        		show_404();
        	}
        }

        public function calificar($Ven_IdVenta=false)
        {
        	if($Ven_IdVenta)
        	{
        		$Ven_IdVenta = (int)$Ven_IdVenta;
	 			$data = $this->acl->load_datos("no", "no", "comprador");
		        $data['pagina']['titulo'] = 'Calificación de compra';

		        $data['idventa'] = $Ven_IdVenta;
		        $venta = $this->ventaModel->get_venta($Ven_IdVenta);
		        $data['idtienda'] = $venta->Tie_IdTienda;
		        $data['idusuario'] = $venta->Usu_IdUsuario;

		        $this->twig->parse('comprador/calificacion.twig', $data);
        	}
        	else
        	{
        		show_404();
        	}
        }

        public function reportar($Ven_IdVenta=false)
        {
        	if($Ven_IdVenta)
        	{
        		$Ven_IdVenta = (int)$Ven_IdVenta;
	 			$data = $this->acl->load_datos("no", "no", "comprador");
		        $data['pagina']['titulo'] = 'Calificación de compra';

		        $data['idventa'] = $Ven_IdVenta;
		        $venta = $this->ventaModel->get_venta($Ven_IdVenta);
		        $data['idtienda'] = $venta->Tie_IdTienda;
		        $data['idusuario'] = $venta->Usu_IdUsuario;

		        $this->twig->parse('comprador/reportar.twig', $data);
        	}
        	else
        	{
        		show_404();
        	}
        }

        public function califica_compra_tienda()
        {
        	if ($this->input->is_ajax_request())
			{
     			$data = $this->acl->load_datos("no", "no", "comprador");
     			$Cal_Compra = $this->input->post('cCompra');
				$Cal_Tienda = $this->input->post('cTienda');
                $Cal_Comentario = $this->input->post('comentario'); 
                $Ven_IdVenta = $this->input->post('idVenta'); 
                $Tie_IdTienda = $this->input->post('idTienda');
                $Usu_IdUsuario = $this->input->post('idUsuario');

                $registrar_calificacion = $this->ventaModel->insert_calificacion($Cal_Compra,$Cal_Tienda,$Usu_IdUsuario,$Tie_IdTienda,$Ven_IdVenta,$Cal_Comentario);
                if(is_null($registrar_calificacion))
				{
					echo false;
				}
				else
				{
					echo 1;
				}
			}
			else
			{
				show_404();
			}
        }

		public function ajax_procesar_compra()
		{
			if ($this->input->is_ajax_request())
			{
				$data = $this->acl->load_datos("no", "no", "comprador");

				$array_compra = $this->input->post('array_compra');
				$Ven_Total = $this->input->post('total');
				$Cot_IdCotizacion = $this->input->post('id_cotizacion');

				$Ven_IdVenta = $this->ventaModel->registrar_venta($array_compra, $Ven_Total, $Cot_IdCotizacion, $data['usuario']['id_usuario']);

				if(is_null($Ven_IdVenta))
				{
					echo false;
				}
				else
				{
					echo $Ven_IdVenta;
				}				
			}
			else
			{
				show_404();
			}
		}
	}
?>