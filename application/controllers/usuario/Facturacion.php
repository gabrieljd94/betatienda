<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Facturacion extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model("Pago_model", "pagoModel"); 
        }
		public function index()
		{
			$data = $this->acl->load_datos("no", "duenio");

	        $data['pagina']['titulo'] = 'Facturación';

	        $data['pagos'] = $this->pagoModel->get_pagos("WHERE Tie_IdTienda = ".$data['usuario']['id_tienda']);

	        //Meses
	        $data['meses']['nombre'] = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre');
	        $this->twig->parse('usuario/facturacion.twig', $data);
		}
	}
?>