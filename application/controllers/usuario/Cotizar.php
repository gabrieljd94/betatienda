<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	//CONTROLADOR PARA COTIZACION
	class Cotizar extends CI_Controller
	{
		function __construct()
		{
			parent::__construct(); 
			$this->load->model("Producto_model", "productoModel");
			$this->load->model("Tienda_model", "tiendaModel");
			$this->load->model("Configuracion_model", "configuracionModel");
			$this->load->model("Local_model", "localModel");
			$this->load->model("Cotizacion_model", "cotizacionModel");
        }

        public function index()
		{
			$data = $this->acl->load_datos();

			$data['pagina']['menu'] = 'gestion';
	      	$data['pagina']['submenu'] = 'pedidos';

		  	$data['pedido'] = $this->cotizacionModel->get_pedidos_x_tienda($data['tienda']->Tie_IdTienda);
	      	$data['pagina']['titulo'] = 'Gestion de Pedidos';
	      	$this->twig->parse('usuario/gestion_pedido.twig', $data);
		}

		public function gestion_pedido_detalle($Ped_IdPedido)
		{
	  		$data = $this->acl->load_datos();

	      	$Ped_IdPedido = (int)$Ped_IdPedido;
	      	$data['pagina']['titulo'] = 'Detalle de Pedidos';
	      	$data['pagina']['menu'] = 'gestion';
	      	$data['pagina']['submenu'] = 'pedidos';

	      	$data['pedido'] = $this->cotizacionModel->get_datos_comprador_pedido($Ped_IdPedido);

	      	$data['pdetalle'] = $this->cotizacionModel->get_detalle_pedido_x_tienda($Ped_IdPedido, $data['tienda']->Tie_IdTienda);
	      	
	      	$data['pedido_respondido'] = $this->cotizacionModel->cotizacionRespondido($Ped_IdPedido);
	      	$data['variacion_pedido'] = $this->cotizacionModel->getPedidoDetalleVar($Ped_IdPedido);

	    	$this->twig->parse('usuario/gestion_pedido_detalle.twig', $data);
		}		

		public function guia()
		{
	  		if ($this->input->is_ajax_request())
			{
				$data = $this->acl->load_datos();
				$Ven_IdVenta = (int)$this->input->post('Ven_IdVenta');
				$nguia = (string)$this->input->post('nguia');

				// print_r($Ven_IdVenta);
				// exit();

				$Gui_IdGuia =  $this->cotizacionModel->insertGuia($Ven_IdVenta,$nguia);

				echo $Gui_IdGuia;
				exit();

			}
		}

		public function subirGuia($Ven_IdVenta)
	    {
	      sleep(3);
	      
	      $Ven_IdVenta = (int)$Ven_IdVenta;

	      $nombre = $Ven_IdVenta;



		  if($_FILES["files"]["name"] != '')
		  {
		   $output = [];
		   $config["upload_path"] = '../img';
		   $config["allowed_types"] = 'gif|jpg|png';
		   $config["file_name"] = 'guia_'.$nombre;
		   $config["overwrite"] = TRUE;
		   $this->load->library('upload', $config);
		   $this->upload->initialize($config);
		   for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
		   {
		    $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
		    $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
		    $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
		    $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
		    $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
		    if($this->upload->do_upload('file'))
		    {		    
		    $data = $this->upload->data();

		    $config['image_library'] = 'gd2';
	        $config['source_image'] = $data['full_path'];
	        $config['maintain_ratio'] = TRUE;
	        $config['width']     = 900;
	        $config['height']   = 600;

	        $this->load->library('image_lib', $config); 
	        $this->image_lib->resize();

			$Ven_Foto = $this->cotizacionModel->addGuia($Ven_IdVenta,$data["file_name"]);
		    }
		    echo $Ven_IdVenta;
		   }
		  }
	    }

		public function enviarCotizacion()
		{
			if ($this->input->is_ajax_request())
			{
				$data = $this->acl->load_datos();

				$correo = (string)$this->input->post('correo');
				$idPedido = (string)$this->input->post('idPedido');
				$precios = $this->input->post('arrayPrecios');
				$preciosDscto = $this->input->post('arrayPreciosDscto');
				
				$data['pdetalle'] = $this->cotizacionModel->get_detalle_pedido_x_tienda($idPedido, $data['tienda']->Tie_IdTienda);
				$data['variacion_pedido'] = $this->cotizacionModel->getPedidoDetalleVar($idPedido);
				$pedido = $this->cotizacionModel->get_datos_comprador_pedido($idPedido);
		
				//Datos tienda - Usuario
				$datosTienda = $this->tiendaModel->get_tienda_info($data['tienda']->Tie_IdTienda);
				$datosUsuario = $this->cotizacionModel->get_usuario_porIdTienda($data['tienda']->Tie_IdTienda);

				//Datos productos
				$datosLocales= $this->tiendaModel->get_local_info($data['tienda']->Tie_IdTienda);

				$registrarCoti = $this->cotizacionModel->insertCotizacion($data['pdetalle'], $precios, $preciosDscto);
				
				$this->enviarCotizacionCliente($data['pdetalle'], $correo, $idPedido, $precios, $datosLocales, $datosTienda, $datosUsuario, $data['dominio'], $data['ruta_img'], $pedido->Per_Nombre);

				if ($registrarCoti)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
		}

		public function enviarCotizacion2()
		{
			if ($this->input->is_ajax_request())
			{
				$data = $this->acl->load_datos();

				$correo = (string)$this->input->post('correo');
				$idPedido = (string)$this->input->post('idPedido');
				$precios = $this->input->post('arrayPrecios');
				$preciosDscto = $this->input->post('arrayPreciosDscto');
				$idCotizacion = $this->input->post('idCotizacion');

				$data['pdetalle'] = $this->cotizacionModel->get_detalle_pedido_x_tienda($idPedido, $data['tienda']->Tie_IdTienda);
				$data['variacion_pedido'] = $this->cotizacionModel->getPedidoDetalleVar($idPedido);	
				$pedido = $this->cotizacionModel->get_datos_comprador_pedido($idPedido);
				//Datos tienda - Usuario
				$datosTienda= $this->tiendaModel->get_tienda_info($data['tienda']->Tie_IdTienda);
				$datosUsuario = $this->cotizacionModel->get_usuario_porIdTienda($data['tienda']->Tie_IdTienda);
				//Datos productos
				$datosLocales= $this->tiendaModel->get_local_info($data['tienda']->Tie_IdTienda);

				$registrarCoti = $this->cotizacionModel->updateCotizacion($data['pdetalle'], $precios, $preciosDscto, $idCotizacion);

				$this->enviarCotizacionCliente($data['pdetalle'], $correo, $idPedido, $preciosDscto, $datosLocales, $datosTienda, $datosUsuario, $data['dominio'], $data['ruta_img'], $pedido->Per_Nombre);

				if ($registrarCoti)
				{
					echo 1;
				} 
				else
				{
					echo 0;
				}
			}
		}

		public function cambiarPorcentaje()
		{
			if ($this->input->is_ajax_request())
			{
				$data = $this->acl->load_datos();

				$Tie_PorcentajeCotizacion = $this->input->post('porcentaje');

				$actualizado = $this->tiendaModel->actualizar_porcentaje_tienda($Tie_PorcentajeCotizacion, $data['tienda']->Tie_IdTienda);

				echo $actualizado;
			}
		}
 		
 		public function enviarCotizacionCliente($pdetalle, $correo, $idPedido, $preciosDscto, $datosLocales, $datosTienda, $datosUsuario, $dominio, $ruta_img, $nombre_usuario)
 		{
 			$this->load->library('correo');
           	
            $datos['pdetalle'] = $pdetalle;
            $datos['idPedido'] = $idPedido;
            $datos['precios'] = $preciosDscto;
            $datos['datosLocales'] = $datosLocales;
            $datos['datosTienda'] = $datosTienda;
            $datos['datosUsuario'] = $datosUsuario;
            $datos['nombre_usuario'] = $nombre_usuario;
            $datos['dominio'] = $nombre_usuario;
            $datos['ruta_img'] = $ruta_img;

            $contenido_correo = $this->twig->parse('correo/cotizacion_cliente2.twig', $datos, true);

            $this->correo->enviar($correo, "Cotización de pedido".$idPedido, $contenido_correo);
		}
    }
?>