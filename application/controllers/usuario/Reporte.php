<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Reporte extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model("Producto_model", "productoModel");
			$this->load->model("Tienda_model", "tiendaModel");
			$this->load->model("Configuracion_model", "configuracionModel");
			$this->load->model("Local_model", "localModel"); 
        }
		public function index()
		{
 			$data = $this->acl->load_datos("no", "duenio");
	        $data['pagina']['titulo'] = 'Reporte de tienda';
	        $this->twig->parse('usuario/reporte.twig', $data);
		}

		public function load_reporte_pedidos()
		{
			if ($this->input->is_ajax_request())
			{
				$this->load->model("Cotizacion_model", "cotizacionModel");

				$data = $this->acl->load_datos();

				$fecha_inicio = $this->input->post('fecha_inicio');
				$fecha_fin = $this->input->post('fecha_fin');

				$data['pedidos'] = $this->cotizacionModel->get_pedido_reporte($data['tienda']->Tie_IdTienda, "AND Ped_FechaCreacion BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."'");
				if(count($data['pedidos']) != 0){
					$this->twig->parse('usuario/load/reporte_pedidos.twig', $data);
				}
			}
		}

		public function load_reporte_productos()
		{
			if ($this->input->is_ajax_request())
			{
				$this->load->model("Cotizacion_model", "cotizacionModel");

				$data = $this->acl->load_datos();

				$fecha_inicio = $this->input->post('fecha_inicio');
				$fecha_fin = $this->input->post('fecha_fin');

				$data['pdetalle'] = $this->cotizacionModel->get_producto_reporte($data['tienda']->Tie_IdTienda, "AND ped.Ped_FechaCreacion BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."'");
	      		
	      		if(count($data['pdetalle']) != 0){
					$this->twig->parse('usuario/load/reporte_productos.twig', $data);
				}
				
			}
		}
	}
?>