<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	//CONTROLADOR PARA INICIAR SESION
	class Login extends CI_Controller
	{
		function __construct()
		{
            parent::__construct();
            
			$this->load->model("Usuario_model", "usuarioModel");
            $this->load->model('redes_sociales/Facebook_model', 'facebookModel');

            $this->load->helper(array('form', 'url', 'security'));

            $this->load->library(array('form_validation', 'facebook'));

            $this->load->library('tank_auth');
            $this->lang->load('tank_auth');
        }
        public function index($url='')
		{
            $data = $this->acl->load_datos("no", false);
            
            if($this->session->userdata('logueado'))
            {
                redirect('index');
            }
            else
            {
                $data['pagina']['titulo'] = 'Login';


                if($this->input->post())
                {
                    $this->form_validation->set_rules('correo', 'Correo', 'trim|required|valid_email');
                    $this->form_validation->set_rules('clave', 'Clave', 'trim|required');

                    if($this->form_validation->run())
                    {
                        $this->_login($this->form_validation->set_value('correo'), $this->form_validation->set_value('clave'), 0, true, $url);
                    }
                }

                $data['facebook']['id'] = $this->config->item('facebook_app_id');
                $data['facebook']['permisos'] = $this->config->item('facebook_permissions');

                $this->twig->parse('usuario/login.twig', $data);
            }
		}

        function fb_signin($accessToken)
        {
            // get the facebook user and save in the session
            if (!isset($accessToken)) 
            {
              echo 'No cookie set or no OAuth data could be obtained from cookie.';
              exit;
            }

            $fb_user = $this->facebookModel->getUser($accessToken);

            if(isset($fb_user))
            {
                $this->session->set_userdata('facebook_id', $fb_user['id']);

                $nombres = explode(" ", $fb_user['name']);

                //$nombre_usuario = strtolower(($nombres[0]));
                $Per_Nombre = $fb_user['name'];
                $Usu_Correo = $fb_user['email'];

                $use_username = $this->config->item('use_username', 'tank_auth');
                $email_activation = $this->config->item('email_activation', 'tank_auth');

                $usuario = $this->usuarioModel->get_usuario_by_correo($Usu_Correo);

                if(count($usuario) > 0)
                {
                    $this->session->set_userdata(array('id_usuario' => $usuario->Usu_IdUsuario, 'nombre_usuario' => $Usu_Correo, 'rol_usuario' => $usuario->Rol_IdRol,  'tienda_usuario' => $usuario->Tie_IdTienda, 'logueado' => ($usuario->Usu_Activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED));

                    $this->_redirigir_login();
                }               
            }
            else 
            { 
                echo 'cannot find the Facebook user'; 
            }
        }

        function olvido_contrasenia()
        {
            if ($this->tank_auth->is_logged_in()) 
            {
                redirect('');

            } 
            elseif ($this->tank_auth->is_logged_in(FALSE)) 
            {
                redirect('/auth/send_again/');
            } 
            else 
            {
                $this->form_validation->set_rules('correo', 'Correo', 'trim|required|xss_clean');

                $data['errors'] = array();

                if ($this->form_validation->run()) 
                {                               
                    if (!is_null($data = $this->tank_auth->forgot_password($this->form_validation->set_value('correo')))) 
                    {
                        $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                        $this->_send_email('forgot_password', $data['correo'], $data);

                        $this->_show_message($this->lang->line('auth_message_new_password_sent'));
                    } 
                    else 
                    {
                        $errors = $this->tank_auth->get_error_message();
                        foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                    }
                }

                $this->twig->parse('usuario/olvido_contrasenia.twig', $data);
            }
        }


        function reset_password()
        {
            $user_id = $this->uri->segment(4);
            $new_pass_key = $this->uri->segment(5);

            $data['error'] = true;

            $this->form_validation->set_rules('clave', 'Nueva Contraseña', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
            $this->form_validation->set_rules('clave2', 'Confirmar nueva contraseña', 'trim|required|xss_clean|matches[clave]');

            $this->form_validation->set_message("matches", "Las contraseñas no coinciden");

            $data['errors'] = array();

            if ($this->form_validation->run()) 
            {
                $data['error'] = false;

                if (!is_null($data = $this->tank_auth->reset_password($user_id, $new_pass_key, $this->input->post('clave'))))
                {
                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                    $this->_send_email('reset_password', $data['correo'], $data);

                    $this->_show_message($this->lang->line('auth_message_new_password_activated').' '.anchor('/usuario/login/', 'Login'));
                }
                else
                {
                    $this->_show_message($this->lang->line('auth_message_new_password_failed'));
                }
            }
            else 
            {
                // Try to activate user by password key (if not activated yet)
               // if ($this->config->item('email_activation', 'tank_auth')) 
               //  {
               //      $this->tank_auth->activate_user($user_id, $new_pass_key, FALSE);
               //  }

               //  if (!$this->tank_auth->can_reset_password($user_id, $new_pass_key)) 
               //  {
               //      $this->_show_message($this->lang->line('auth_message_new_password_failed'));
               //  } 
            }

            $this->twig->parse('usuario/reset_password.twig', $data);
        }

        function _login($correo, $clave, $remember, $redirect=1, $url='')
        {

            $data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND  $this->config->item('use_username', 'tank_auth'));
            $data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

            if ($this->tank_auth->login($correo, $clave, $remember, $data['login_by_username'], $data['login_by_email'])) 
            {
                if($redirect == 1)
                {
                    $this->_redirigir_login($url);
                }
            } 
            else 
            {
                $errors = $this->tank_auth->get_error_message();
                
                if (isset($errors['banned'])) 
                {
                    $this->_show_message($this->lang->line('auth_message_banned').' '.$errors['banned']);

                }
                elseif (isset($errors['not_activated'])) 
                {
                    redirect('/usuario/send_again/');

                } 
                else 
                {
                    foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                }
            }
        }

        function _redirigir_login($url='')
        {
            if(trim($url) == '')
            {
                if ($this->tank_auth->get_rol() == 2 or $this->tank_auth->get_rol() == 3)
                {
                    $tienda = $this->tiendaModel->get_tienda($this->tank_auth->get_tienda());

                    redirect('//'.$tienda->Tie_Subdominio.'.pormayor.pe/usuario/tienda');
                }
                else
                {
                    redirect('');
                }
            }
            else
            {
                //$url = str_replace('-', '/', $url);
                redirect($url);
            }
        }

        function _send_email($type, $email, &$data)
        {
            $this->load->library('email');
            $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->to($email);
            $this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
            $this->email->message($this->load->view('usuario/email/'.$type.'-html', $data, TRUE));
            $this->email->set_alt_message($this->load->view('usuario/email/'.$type.'-txt', $data, TRUE));
            $this->email->send();
        }

        function _show_message($message)
        {
            $this->session->set_flashdata('message', $message);
            redirect('usuario/login');
        }
    }
?>