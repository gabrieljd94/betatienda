<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	//CONTROLADOR PARA INFORMACIÓN Y CONFIGURACIÓN DE TIENDA
	class Tienda extends CI_Controller
	{ 
		function __construct()
		{
			parent::__construct();
			$this->load->model("Producto_model", "productoModel");
			$this->load->model("Tienda_model", "tiendaModel");
			$this->load->model("Configuracion_model", "configuracionModel");
			$this->load->model("Local_model", "localModel");
			$this->load->model("Cotizacion_model", "cotizacionModel");
			$this->load->model("Usuario_model", "usuarioModel");
			$this->load->model("General_model", "generalModel");
        }

		public function index()
		{
			$data = $this->acl->load_datos();
			$data['pagina']['titulo'] = 'Panel Administrador';
			$data['pagina']['menu'] = 'tienda';
			$data['local'] = $this->tiendaModel->get_local_info($data['tienda']->Tie_IdTienda);
			$data['dtienda'] = $this->tiendaModel->get_detalleTienda($data['tienda']->Tie_IdTienda);
			$this->twig->parse('usuario/informacion_tienda.twig', $data);
		}

		public function ubicacion_local($Tie_IdTienda)
		{
			$data = $this->acl->load_datos();
			$data['pagina']['titulo'] = 'Ubicacion de la tienda';
			$data['pagina']['menu'] = 'tienda';
			
			$this->twig->parse('usuario/ubicacion_local.twig', $data);
		}

		public function configuracion()
		{
			$data = $this->acl->load_datos("no", "duenio");

			$data['pagina']['titulo'] = 'Panel Administrador';
	      	$data['color'] = $this->configuracionModel->get_colores();
	      	$clave = '123456';
	      	$data['clave'] = $this->usuarioModel->clave_hash($clave);
	      	$this->twig->parse('usuario/configuracion.twig', $data);
		}

		public function addLocal()
		{
		  $data = $this->acl->load_datos();
	      $data['pagina']['titulo'] = 'Agregar Local';
	      $this->twig->parse('usuario/load/addLocal.twig', $data);
		}

		public function editLocal($Loc_IdLocal = false)
		{
			$data = $this->acl->load_datos();
	      	$data['pagina']['titulo'] = 'Editar Local';

	      	$data['local'] = $this->localModel->get_local($Loc_IdLocal);
	      	$this->twig->parse('usuario/load/editLocal.twig', $data);
		}

		public function dTienda()
		{
			$data = $this->acl->load_datos();
	      	$data['pagina']['titulo'] = 'Detalle de informacion tienda';
	    	if(count($data['tienda']) > 0)
	      	{
	    		$data['dtienda'] = $this->tiendaModel->get_detalleTienda($data['tienda']->Tie_IdTienda);
	      	}
	      	$this->twig->parse('usuario/load/dTienda.twig', $data);
		}
		public function eliminar_detalle_tienda()
		{
			if ($this->input->is_ajax_request()) 
			{
				$Tid_IdTiendaDetalle = $this->input->post('Tid_IdTiendaDetalle');
				$registro = $this->tiendaModel->eliminar_detalle_tienda($Tid_IdTiendaDetalle);
				if ($registro) 
				{
					echo 1;
				} 
				else 
				{
					echo 0;
				}
			}
		}
		public function edit_nombre_tienda()
		{
			if ($this->input->is_ajax_request()) 
			{
				$Tie_IdTienda = $this->input->post('Tie_IdTienda');
				$nTienda = $this->input->post('nTienda');
				$registro= $this->tiendaModel->edit_nombre_tienda($Tie_IdTienda,$nTienda);
				if ($registro) 
				{
					echo 1;
				} 
				else 
				{
					echo 0;
				}
			}
		}
		public function configuracion_tienda()
		{
			if ($this->input->is_ajax_request()) 
			{
				$Mostrar_Precio = $this->input->post('Mostrar_Precio');
				$colorHex = $this->input->post('colorHex');
				$colorHex2 = $this->input->post('colorHex2');
				$Tie_IdTienda = $this->input->post('Tie_IdTienda');
				$existe = $this->configuracionModel->existeTienda($Tie_IdTienda);
				if ($existe) 
				{
					$registro = $this->configuracionModel->registrarColorPrecio($Mostrar_Precio,$colorHex,$colorHex2,$Tie_IdTienda);
				}
				else
				{
					$registro = $this->configuracionModel->insertColorPrecio($Mostrar_Precio,$colorHex,$colorHex2,$Tie_IdTienda);
				}
				if ($registro) 
				{
					echo 1;
				} 
				else 
				{
					echo 0;
				}
			}
		}

		public function insert_detalle_tienda()
		{
			if ($this->input->is_ajax_request()) 
			{
				$Tie_IdTienda = $this->input->post('Tie_IdTienda');
				$DetalleTienda = $this->input->post('DetalleTienda');
				$Tid_IdTiendaDetalle = $this->tiendaModel->insertDetalleTienda($Tie_IdTienda,$DetalleTienda);
				echo $Tid_IdTiendaDetalle;
			}
		}		

		public function update_detalle_tienda()
		{
			if ($this->input->is_ajax_request()) 
			{
				$Tid_IdTiendaDetalle = $this->input->post('Tid_IdTiendaDetalle');
				$DetalleTienda = $this->input->post('DetalleTienda');
				$Tid_IdTiendaDetalle = $this->tiendaModel->updateDetalleTienda($Tid_IdTiendaDetalle,$DetalleTienda);
				echo $Tid_IdTiendaDetalle;
			}
		}

		public function subirImagenTienda($Tid_IdTiendaDetalle)
	    {
	    	sleep(3);
	    	$Tid_IdTiendaDetalle = (int)$Tid_IdTiendaDetalle;
		  	if($_FILES["files"]["name"] != '')
		  	{
		   	   $output = 0;
			   $config["upload_path"] = '../img';
			   $config["allowed_types"] = 'gif|jpg|png';
			   $config["file_name"] = 'dt_'.$Tid_IdTiendaDetalle;
			   $config["overwrite"] = TRUE;			   
			   $this->load->library('upload', $config);
			   $this->upload->initialize($config);
			   
		   for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
		   {
		    $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
		    $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
		    $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
		    $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
		    $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
		    if($this->upload->do_upload('file'))
		    {

		     $data = $this->upload->data();

	        $config['image_library'] = 'gd2';
	        $config['source_image'] = $data['full_path'];
	        $config['maintain_ratio'] = TRUE;
	        $config['width']     = 1000;
	        $config['height']   = 333;

	        $this->load->library('image_lib', $config); 

	        $this->image_lib->resize();
			 $producto_fotos = $this->tiendaModel->addDetalleFotos($Tid_IdTiendaDetalle,$data["file_name"]);
		     $output = 1;
		    }
		   }
		  }
	    }
	    
	    public function subirImagenLogo($Tie_IdTienda)
        {
          sleep(3);
          $Tie_IdTienda = (int)$Tie_IdTienda;
          if($_FILES["files"]["name"] != '')
          {
           $output = [];
           $config["upload_path"] = '../img';
           $config["allowed_types"] = 'gif|jpg|png';
           $config["file_name"] = 'logo_'.$Tie_IdTienda;
           $config["overwrite"] = TRUE;
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
           {
            $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
            $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
            $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
            $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
            $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
            if($this->upload->do_upload('file'))
            {           
            $data = $this->upload->data();
            
            $config['image_library'] = 'gd2';
	        $config['source_image'] = $data['full_path'];
	        $config['maintain_ratio'] = TRUE;
	        $config['width']     = 500;
	        $config['height']   = 166;

	        $this->load->library('image_lib', $config); 
	        $this->image_lib->resize();

            $SKU_Foto = $this->tiendaModel->addLogoFoto($Tie_IdTienda,$data["file_name"]);
            }
           }
          }
        }
		public function editar_local()
		{
			if ($this->input->is_ajax_request()) { 
				$Loc_IdLocal = $this->input->post('IdLocal');
				$Lon_Numero = $this->input->post('tlfLocal');
				$Lod_Direccion = $this->input->post('dirLocal');
				$dirCode = str_replace(" ", "+", $Lod_Direccion);
				$url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=$dirCode&key=AIzaSyCnqEDXE9ekKeGG778KOndF2wM5P3fFSug";
				$response = file_get_contents($url);
				$json = json_decode($response,TRUE);
				$lat = $json['results'][0]['geometry']['location']['lat'];
				$long = $json['results'][0]['geometry']['location']['lng'];
				$registro= $this->tiendaModel->updateLocal($Loc_IdLocal,$Lod_Direccion,$Lon_Numero,$lat,$long);
				if ($registro) {
					echo 1;
				} else {
					echo 0;
				}
			}
		}
		public function deleteLocal()
		{
			if ($this->input->is_ajax_request()) {
				$id = $this->input->post('IdLocal');
				$nombre_tabla = 'local';
				$nombre_variable = 'Loc_IdLocal';
				$registro= $this->tiendaModel->deleteLocal($nombre_tabla,$id,$nombre_variable);
				if ($registro) {
					echo 1;
				} else {
					echo 0;
				}
			}
		}

		public function ajax_obtener_direccion()
		{
			if ($this->input->is_ajax_request())
			{
				$latitud = $this->input->post('latitud');
				$longitud = $this->input->post('longitud');

				$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitud,$longitud&key=AIzaSyCnqEDXE9ekKeGG778KOndF2wM5P3fFSug";
				$response = file_get_contents($url);

				$array = json_decode($response);

				$direccion = $array->results[0]->formatted_address;

				echo $direccion;
			}
		}

		public function insert_local()
		{
			if ($this->input->is_ajax_request()) 
			{
				$Tie_IdTienda = (int)$this->input->post('Tie_IdTienda');
				//Construyendo lat y long
				$dirLocal = $this->input->post('dirLocal');
				$lat = $this->input->post('latitud');
				$lang = $this->input->post('longitud');

				// $dirLatLong = $this->input->post('dirLocal');
				// $Lod_Direccion = str_replace(" ", "+", $dirLatLong);
				// $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=$Lod_Direccion&key=AIzaSyCnqEDXE9ekKeGG778KOndF2wM5P3fFSug";
				// $response = file_get_contents($url);
				// $json = json_decode($response,TRUE);
				// $lat = $json['results'][0]['geometry']['location']['lat'];
				// $lang = $json['results'][0]['geometry']['location']['lng'];
				$nombreLocal = $this->input->post('nombreLocal');
				$tlfLocal = $this->input->post('tlfLocal');
				$registro= $this->tiendaModel->insertLocal($Tie_IdTienda, $nombreLocal, $dirLocal, $tlfLocal, $lat, $lang);

				if ($registro)
				{
					echo 1;
				} 
				else 
				{
					echo 0;
				}
			}
		}

		public function update_local()
		{
			if ($this->input->is_ajax_request())
			{
				$Loc_IdLocal = (int)$this->input->post('Loc_IdLocal');
				
				$Loc_Nombre = $this->input->post('nombre');
				$Loc_Direccion = $this->input->post('direccion');
				$Loc_Lat = $this->input->post('latitud');
				$Loc_Long = $this->input->post('longitud');
				$Loc_Numero = $this->input->post('telefono');

				$registro = $this->tiendaModel->updateLocal($Loc_IdLocal, $Loc_Nombre, $Loc_Direccion, $Loc_Numero, $Loc_Lat, $Loc_Long);
				
				if ($registro)
				{
					echo 1;
				} 
				else
				{
					echo 0;
				}
			}
		}
    }
?>