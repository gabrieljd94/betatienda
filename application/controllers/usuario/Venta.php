<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Venta extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			// $this->load->model("Producto_model", "productoModel");
			$this->load->model("Venta_model", "ventaModel");
        }

        public function index()
        {
    		$data = $this->acl->load_datos("no", "no", "comprador");

    		$data['pagina']['menu'] = 'gestion';
	      	$data['pagina']['submenu'] = 'ventas';

    		$Usu_IdUsuario = $data['usuario']['id_usuario'];
    		
    		$data['pagina']['titulo'] = 'Ventas';
    		
	        $this->load->model("Venta_Detalle_model", "ventaDetalleModel");

	        $data['ventas'] = $this->ventaModel->get_ventas_x_tienda($data['usuario']['id_tienda']);
	        $data['ventas_detalle'] = $this->ventaDetalleModel->get_detalle_venta_x_tienda($data['usuario']['id_tienda']);

	        $this->twig->parse('usuario/ventas.twig', $data);
        }

        public function detalle($Ven_IdVenta = false)
        {
        	if($Ven_IdVenta)
        	{
        		$Ven_IdVenta = (int)$Ven_IdVenta;
	 			$data = $this->acl->load_datos("no", "no", "comprador");
		        $data['pagina']['titulo'] = 'Detalle de la compra';

		        $this->load->model("Venta_Detalle_model", "ventaDetalleModel");
		        $data['venta'] = $this->ventaModel->get_venta($Ven_IdVenta);
		       	$data['venta_detalle'] = $this->ventaDetalleModel->get_detalle_venta($Ven_IdVenta);
		        
		        $this->twig->parse('comprador/compra_detalle.twig', $data);
        	}
        	else
        	{
        		show_404();
        	}
        }

		public function ajax_procesar_compra()
		{
			if ($this->input->is_ajax_request())
			{
				$data = $this->acl->load_datos("no", "no", "comprador");

				$array_compra = $this->input->post('array_compra');
				$Ven_Total = $this->input->post('total');
				$Cot_IdCotizacion = $this->input->post('id_cotizacion');

				$Ven_IdVenta = $this->ventaModel->registrar_venta($array_compra, $Ven_Total, $Cot_IdCotizacion, $data['usuario']['id_usuario']);

				if(is_null($Ven_IdVenta))
				{
					echo false;
				}
				else
				{
					echo $Ven_IdVenta;
				}				
			}
			else
			{
				show_404();
			}
		}
	}
?>