<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	//CONTROLADOR PARA todo lo relacionado con inventario (agregar editar y eliminar producto)
	class Inventario extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model("Producto_model", "productoModel");
			$this->load->model("Tienda_model", "tiendaModel");
			$this->load->model("Configuracion_model", "configuracionModel");
			$this->load->model("Local_model", "localModel");
			$this->load->model("Cotizacion_model", "cotizacionModel");

        } 

        public function index($nropagina = FALSE)
		{		  
		  //$data = $this->_load();
		  $data = $this->acl->load_datos();
	      $data['pagina']['titulo'] = 'Panel Administrador';
	      $data['categoria'] = $this->productoModel->get_Categoria();
	      $data['subcategorias'] = $this->productoModel->get_Categorias_defaul();
		  $data['unidad'] = $this->productoModel->get_UnidadMedida();
	      $data['producto'] = $this->productoModel->productos("WHERE prt.Tie_IdTienda = ".$data['tienda']->Tie_IdTienda);
	      if($data['producto']){
	      $this->load->library('pagination');	  
            $inicio = 0;
            $mostrarpor = 8;             
            if ($nropagina)
            {
                $inicio = ($nropagina - 1) * $mostrarpor;
            }            
            $limit = (int)$inicio;
            $limit2= (int)$mostrarpor;
            $config['base_url'] = base_url()."usuario/inventario/pag/";  
            $config['total_rows'] = $this->productoModel->get_sku_cantidad($data['tienda']->Tie_IdTienda)->Cantidad;
            $config['per_page'] = $mostrarpor; 
            $config['uri_segment'] = 4;
            $config['num_links'] = 4;
            $config['use_page_numbers'] = TRUE;
            $config['first_url'] = base_url()."usuario/inventario/";
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = "<li class='waves-effect linkTag'>";
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='active grey darken-2'><a href='javascript:void(0)' >";
            $config['cur_tag_close'] = "<span class='waves-effect'></span></a></li>";
            $config['next_link'] = "<i class='material-icons left'>navigate_next</i>";
            $config['prev_link'] = "<i class='material-icons left'>navigate_before</i>";
            $config['next_tag_open'] = "<li class='waves-effect lastTag'>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li class='waves-effect'>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li class='waves-effect lastTag'>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li class='waves-effect lastTag'>";
            $config['last_tagl_close'] = "</li>";
            $config['first_link'] = 'Primera';
            $config['last_link'] = 'Última';

            $this->pagination->initialize($config);

            $data['ver_paginacion'] = $this->pagination->create_links();

      	  	$data['SKU'] = $this->productoModel->get_sku3($limit,$limit2,$data['tienda']->Tie_IdTienda);

	      $this->twig->parse('usuario/inventario.twig', $data);
	      } else{
	          $this->twig->parse('usuario/inventario.twig', $data);
	      }
            

		}

		//MODEL PARA AGREGAR EL PRODUCTO
		public function modal_detalle_agregar_producto()
	    {
	      if($this->input->is_ajax_request())
	      {
	       // $data = $this->_load();
	       $data = $this->acl->load_datos();
		    $data['categoria'] = $this->productoModel->get_Categoria();

	        $this->twig->parse('usuario/model/model_AgregarProducto.twig', $data);
	        
	      }
	      else
	      {
	        show_404();
	      }
	    }

	    public function buscar_cat_sub()
	    {
	      if($this->input->is_ajax_request())
	      {
	       	$data = $this->acl->load_datos();
			$nombreProducto = $this->input->post('arrNombrePro');

		    $data['preCatSuc'] = $this->productoModel->get_cat_sub($nombreProducto);

	      	print_r(json_encode($data['preCatSuc']));
	        
	      }
	    }
		
		public function addProducto()
	    {
	      
	       $data = $this->acl->load_datos();
	        $data['pagina']['titulo'] = 'Agregar Producto';
		    $data['categoria'] = $this->productoModel->get_Categoria();
		    $data['unidad'] = $this->productoModel->get_UnidadMedida();

	        $this->twig->parse('usuario/agregarProducto.twig', $data);
	        
	      }	

		public function lProductos($nropagina = FALSE)
	    {
	      
	      $data = $this->acl->load_datos();
	      $data['pagina']['titulo'] = 'Panel Administrador';
	      $data['categoria'] = $this->productoModel->get_Categoria();
		  $data['unidad'] = $this->productoModel->get_UnidadMedida();

	      $this->load->library('pagination');
	      	  
            $inicio = 0;
            $mostrarpor = 5;             
            if ($nropagina)
            {
                $inicio = ($nropagina - 1) * $mostrarpor;
            }            
            $limit = (int)$inicio;
            $limit2= (int)$mostrarpor;
            $config['base_url'] = base_url()."usuario/inventario/pagp/";  
            $config['total_rows'] = $this->productoModel->get_producto_cantidad($data['tienda']->Tie_IdTienda)->Cantidad;
            $config['per_page'] = $mostrarpor; 
            $config['uri_segment'] = 4;
            $config['num_links'] = 4;
            $config['use_page_numbers'] = TRUE;
            $config['first_url'] = base_url()."usuario/inventario/lProductos";
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = "<li class='waves-effect linkTag'>";
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='active grey darken-2'><a href='javascript:void(0)' >";
            $config['cur_tag_close'] = "<span class='waves-effect'></span></a></li>";
            $config['next_link'] = "<i class='material-icons left'>navigate_next</i>";
            $config['prev_link'] = "<i class='material-icons left'>navigate_before</i>";
            $config['next_tag_open'] = "<li class='waves-effect lastTag'>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li class='waves-effect'>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li class='waves-effect lastTag'>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li class='waves-effect lastTag'>";
            $config['last_tagl_close'] = "</li>";
            $config['first_link'] = 'Primera';
            $config['last_link'] = 'Última';

            $this->pagination->initialize($config);

            $data['ver_paginacion'] = $this->pagination->create_links();

	      	$data['producto'] = $this->productoModel->productos2("WHERE prt.Tie_IdTienda = ".$data['tienda']->Tie_IdTienda ." GROUP BY pro.Pro_IdProducto LIMIT ".$limit.", ".$limit2);

      	  	// $data['SKU'] = $this->productoModel->get_sku($data['tienda']->Tie_IdTienda);

	      $this->twig->parse('usuario/lproducto.twig', $data);	
	        
	      }
	 		
		public function productoDetalle($Pro_IdProducto)
	    {
	      
	        $data = $this->acl->load_datos();
	        $data['pagina']['titulo'] = 'Agregar Producto';
		    $data['producto'] = $this->productoModel->productos("WHERE pro.Pro_IdProducto = '".$Pro_IdProducto."'");
		    $data['fotos'] = $this->productoModel->get_fotos_producto($Pro_IdProducto);
	     	$data['SKU'] = $this->productoModel->get_sku($Pro_IdProducto);
	     	$data['Des_IdDetalle_SubCategoria'] = $this->productoModel->get_p_c_d_s($Pro_IdProducto);


	        $this->twig->parse('usuario/productoDetalle.twig', $data);
	        
	      }

		public function sku($SKU_IdSKU)
	    {
	      
	        $data = $this->acl->load_datos();
	        $data['pagina']['titulo'] = 'Agregar Producto';		
	     	$data['SKU'] = $this->productoModel->get_sku2($SKU_IdSKU);

	        $this->twig->parse('usuario/sku.twig', $data);
	        
	      }

		public function eliminar_variacion_sku()
		    {
		      $numero = (int)$this->input->post("numero");
		     // print_r($_SESSION['provariacion']);
			  $data = $this->acl->load_datos();

		      unset($_SESSION['provariacion'][$numero]);

		       $data['producto_variacion'] = $_SESSION['provariacion'];

	        $this->twig->parse('usuario/load/lista_variacion.twig', $data);
		    }

		public function eliminarPro()
		{
		 $data = $this->acl->load_datos();
		  if ($this->input->is_ajax_request()) {
				$Pro_IdProducto = $this->input->post('Pro_IdProducto');
				
	      		$registro = $this->productoModel->eliminarPro($Pro_IdProducto);

	      		if($registro){
	      			echo 1;
	      		}else{
	      			echo 0;
	      		}
	      		
			}
      	  
		}

		public function eliminarSku()
		{
		 $data = $this->acl->load_datos();
		  if ($this->input->is_ajax_request()) {
				$SKU_IdSKU = $this->input->post('SKU_IdSKU');
				
	      		$registro = $this->productoModel->eliminarSku($SKU_IdSKU);

	      		if($registro){
	      			echo 1;
	      		}else{
	      			echo 0;
	      		}
	      		
			}
      	  
		}

		public function oferta()
		{
		 $data = $this->acl->load_datos();
		  if ($this->input->is_ajax_request()) {
				$Pro_IdProducto = $this->input->post('Pro_IdProducto');
				$Pro_Oferta = $this->input->post('oferta');
				
	      		$registro = $this->productoModel->ofertaProducto($Pro_IdProducto,$Pro_Oferta);

	      		if($registro){
	      			echo 1;
	      		}else{
	      			echo 0;
	      		}
	      		
			}
      	  
		}
	 
	
	    //1 BUSCA LAS SUBCATEGORIAS SEGUN EL ID DE LA CATEGORIA
		public function select_subc()
		{
		$data = $this->acl->load_datos();
		if ($this->input->is_ajax_request()) {
			$Cat_IdCategoria = $this->input->post('Cat_IdCategoria');
				
	      	$data['subcategoria'] = $this->productoModel->get_subCategoria($Cat_IdCategoria);

	      		print_r(json_encode($data['subcategoria']));
			}
      	  
		}

		public function nueva_variacion()
		{
		$data = $this->acl->load_datos();
		if ($this->input->is_ajax_request()) {
			$Var_Nombre = $this->input->post('nombVar');
			$Array_Vao_Nombre = $this->input->post('arrVAO');			
			$SubCategoriaDetalle = $this->input->post('SubCategoriaDetalle');			
				
	      	$Var_IdVariacion = $this->productoModel->insertNuevaVariacion($Var_Nombre,$Array_Vao_Nombre,$data['tienda']->Tie_IdTienda,$SubCategoriaDetalle);
	      	// if ($Var_IdVariacion) {	      		
		      // 	$data['nueva_variacion'] = $this->productoModel->getVariacionNueva($Var_IdVariacion,$data['tienda']->Tie_IdTienda);
		      // 	}
		     $data['var_cat'] = $this->productoModel->get_var_cat($SubCategoriaDetalle,$data['tienda']->Tie_IdTienda);

	      	print_r(json_encode($data['var_cat']));

			}
      	  
		}

		public function ver_nueva_variacion()
		{
			$data = $this->acl->load_datos();
		if ($this->input->is_ajax_request()) {
			$data['var_cat'] = $this->input->post('datos');
		    $Des_IdDetalle_SubCategoria = $this->input->post('SubCategoriaDetalle');
		    $data['variacion'] = $this->productoModel->get_variacion_sub($Des_IdDetalle_SubCategoria);
	        $this->twig->parse('usuario/load/nueva_var.twig', $data);
      	  
			}
		}

		public function skuEdit()
		{
		$data = $this->acl->load_datos();
		if ($this->input->is_ajax_request()) {
			$SKU_IdSku = $this->input->post('SKU_IdSku');
			$SKU_Cantidad = (int)$this->input->post('SKU_Cantidad');
				
	      	$registro = $this->productoModel->skuEdit($SKU_IdSku,$SKU_Cantidad);

	      	if($registro){
	      		echo 1;
	      	}else
	      	{
	      		echo 0;
	      	}

	      
			}
      	  
		}

	    //2 VISTA DE LAS SUBCATEGORIAS SEGUN EL ID DE LA CATEGORIA
		public function Select_subCategoria()
	    {
	      if($this->input->is_ajax_request())
	      {
	        $data = $this->acl->load_datos();
		    $data['subcategoria'] = $this->input->post('datos');

	        $this->twig->parse('usuario/model/selSubcategoria.twig', $data);	        
	      }
	      else
	      {
	        show_404();
	      }
	    }

	    //1 BUSCA EL DETALLE DE LA SUBCATEGORIAS SEGUN EL ID DE LA SUBCATEGORIA
	    public function select_subdetalle()
		{
		 $data = $this->acl->load_datos();
		  if ($this->input->is_ajax_request()) {
				$subcategoria_Suc_IdSubCategoria = $this->input->post('Suc_IdSubCategoria');
				
	      		$data['subCategoria_detalle'] = $this->productoModel->get_subCategoria_detalle($subcategoria_Suc_IdSubCategoria);

	      		print_r(json_encode($data['subCategoria_detalle']));
			}
      	  
		}

	    //2 VISTA DE LAS SUBCATEGORIAS SEGUN EL ID DE LA CATEGORIA
	    public function selSubcategoriaDetalle()
	    {
	      if($this->input->is_ajax_request())
	      {
	       $data = $this->acl->load_datos();
		    $data['detalle_subcategoria'] = $this->input->post('datos');

	        $this->twig->parse('usuario/model/selSubcategoriaDetalle.twig', $data);
	        
	      }
	      else
	      {
	        show_404();
	      }
	    }


	    //BUSCA LAS POSIBLES VARIACIONES SEGÚN EL DETALLE DE LA SUBCATEGORIA
	    public function variaciones_categorias()
		{		  
		  if ($this->input->is_ajax_request()) {
		  		$data = $this->acl->load_datos();
				$Des_IdDetalle_SubCategoria = $this->input->post('Des_IdDetalle_SubCategoria');
				
	      		$data['var_cat'] = $this->productoModel->get_var_cat($Des_IdDetalle_SubCategoria,$data['tienda']->Tie_IdTienda);

	      		print_r(json_encode($data['var_cat']));
			}      	  
		}


	    //VISTA DE LAS VARIACIONES SEGÚN EL DETALLE DE LA SUBCATEGORIA
		public function var_cat()
	    {
	      if($this->input->is_ajax_request())
	      {
	      	$data = $this->acl->load_datos();
		    $data['var_cat'] = $this->input->post('datos');
		    $Des_IdDetalle_SubCategoria = $this->input->post('Des_IdDetalle_SubCategoria');
		    $data['variacion'] = $this->productoModel->get_variacion_sub($Des_IdDetalle_SubCategoria);
		    $data['colores'] = $this->configuracionModel->get_colores();

	        $this->twig->parse('usuario/model/variacion_categoria.twig', $data);
	        
	      }
	      else
	      {
	        show_404();
	      }
	    }
	    	    //VISTA DE LAS VARIACIONES SEGÚN EL DETALLE DE LA SUBCATEGORIA
		public function var_cat2()
	    {
	      if($this->input->is_ajax_request())
	      {
	  $data = $this->acl->load_datos();
		    $data['var_cat'] = $this->input->post('datos');
		    $Des_IdDetalle_SubCategoria = $this->input->post('Des_IdDetalle_SubCategoria');
		    $data['IdProducto'] = $this->input->post('Pro_IdProducto');
		    $data['variacion'] = $this->productoModel->get_variacion_sub($Des_IdDetalle_SubCategoria);
		    $data['colores'] = $this->configuracionModel->get_colores();

	        $this->twig->parse('usuario/model/variacion_categoria2.twig', $data);
	        
	      }
	      else
	      {
	        show_404();
	      }
	    }		

	    public function colores_no()
	    {
	      if($this->input->is_ajax_request())
	      {
	      	$data = $this->acl->load_datos();
		    $PVO_IdPVO = $this->input->post('IdVAO');
		    $Pro_IdProducto = $this->input->post('idpro');

		    $data['colores'] = $this->configuracionModel->get_colores();
		    $data['colores_ya'] = $this->configuracionModel->get_colores_2($PVO_IdPVO,$Pro_IdProducto);

		    // print_r($data['colores'][0]);
		    // print_r($data['colores_ya'][0]);
	        $this->twig->parse('usuario/load/select_Colores.twig', $data);
	        
	      }
	      else
	      {
	        show_404();
	      }
	    }

	   	public function agregar_producto()
		{
		  
		 $data = $this->acl->load_datos();
		if ($this->input->is_ajax_request()) {
				$Pro_Nombre = $this->input->post('Pro_Nombre');
				$Pro_Descripcion = $this->input->post('Pro_Descripcion');
			
				$precioMin = $this->input->post('precioMin');
				$precioMax = $this->input->post('precioMax');
				$Pro_PrecioRango = $precioMin .'-'. $precioMax;
			

	$Pro_IdProducto = (int)$this->productoModel->addProducto($Pro_Nombre,$Pro_Descripcion,$Pro_PrecioRango);
	
				$Unidad = (int)$this->input->post('Unidad');
				if ($Unidad == 3 or $Unidad == 4 or $Unidad == 5 or $Unidad == 6 ) {
					$valorUnm = $this->input->post('valorUnm');
				} else {
					$valorUnm = 1;
				}
				$producto_unidad = (int)$this->productoModel->addProductoUnidad($Pro_IdProducto,$Unidad,$valorUnm);

				$Cat_IdCategoria = (int)$this->input->post('Cat_IdCategoria');
				$Suc_IdSubCategoria = (int)$this->input->post('Suc_IdSubCategoria');
				$Des_IdDetalle_SubCategoria = (int)$this->input->post('Des_IdDetalle_SubCategoria');

	$producto_categoria = $this->productoModel->addProductoCategoria($Pro_IdProducto,$Cat_IdCategoria,$Suc_IdSubCategoria,$Des_IdDetalle_SubCategoria);
	
	$producto_tienda = $this->productoModel->addProductoTienda($Pro_IdProducto,$data['tienda']->Tie_IdTienda);

				$CombinacionId = $this->input->post('combId');
				$CombinacionCant = $this->input->post('combCantidad');

		        if(isset($_SESSION['provariacion']) && count($_SESSION['provariacion']) > 0)
		        {
		        	for ($i=0; $i < count($CombinacionId); $i++) { 
		        		$_SESSION['provariacion'][$CombinacionId[$i]]['cantidadVariacion'] = (int)$CombinacionCant[$i];
		        	}
		         
		         $array_detalle = $_SESSION['provariacion'];

		        }
		        else
		        {
		          $array_detalle = array();
		        }
	$producto_variacion_vao = $this->productoModel->addProductoVariacionVao($array_detalle,$Pro_IdProducto);	        
				$a = 0;

				if($producto_variacion_vao)
			        {
			          unset($_SESSION['provariacion']);
			          $a = 1;
			        }

			    $b = 0;

			    echo $Pro_IdProducto;

		}
	}	

	public function agregar_variacion_nueva()
		{
		  
		$data = $this->acl->load_datos();
		if ($this->input->is_ajax_request()) {
				$Pro_IdProducto = (int)$this->input->post('Pro_IdProducto');
				$CombinacionId = $this->input->post('combId');
				$CombinacionCant = $this->input->post('combCantidad');

		        if(isset($_SESSION['provariacion']) && count($_SESSION['provariacion']) > 0)
		        {
		        	for ($i=0; $i < count($CombinacionId); $i++) { 
		        		$_SESSION['provariacion'][$CombinacionId[$i]]['cantidadVariacion'] = (int)$CombinacionCant[$i];
		        	}
		         
		         $array_detalle = $_SESSION['provariacion'];

		        }
		        else
		        {
		          $array_detalle = array();
		        }
	$producto_variacion_vao = $this->productoModel->addProductoVariacionVao($array_detalle,$Pro_IdProducto);	        
				$a = 0;

				if($producto_variacion_vao)
			        {
			          unset($_SESSION['provariacion']);
			          $a = 1;
			        }

			    $b = 0;
		}
	}


	    public function subirImagen($Pro_IdProducto)
	    {
	      // sleep(3);
	      
	      $Pro_IdProducto = (int)$Pro_IdProducto;

		  if($_FILES["files"]["name"] != '')
		  {
		   $output = 0;
		   $config["upload_path"] = '../img';
		   $config["allowed_types"] = 'gif|jpg|png';
		   $config["file_name"] = 'items_'.$Pro_IdProducto;
		   $this->load->library('upload', $config);
		   $this->upload->initialize($config);
		   for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
		   {
		    $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
		    $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
		    $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
		    $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
		    $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
		    if($this->upload->do_upload('file'))
		    {
		    
		    $data = $this->upload->data();
		    // $config['image_library'] = 'gd2';
	     //    $config['source_image'] = $data['full_path'];
	     //    $config['maintain_ratio'] = TRUE;
	     //    $config['width'] = 1500;
	     //    $config['height'] = 500;

	     //    $this->load->library('image_lib', $config); 
	     //    $this->image_lib->resize();

			 $producto_fotos = $this->productoModel->addProductoFotos($Pro_IdProducto,$data["file_name"]);
		     $output = 1;
		    }
		   }
		   echo $Pro_IdProducto;
		  }
	    } 
	    

	    public function subirImagenSku($SKU_IdSKU)
	    {
	      sleep(3);
	      
	      $SKU_IdSKU = (int)$SKU_IdSKU;

	      $nombre = $SKU_IdSKU;

		  if($_FILES["files"]["name"] != '')
		  {
		   $output = [];
		   $config["upload_path"] = '../img';
		   $config["allowed_types"] = 'gif|jpg|png';
		   $config["file_name"] = 'item_'.$nombre;
		   $config["overwrite"] = TRUE;
		   $this->load->library('upload', $config);
		   $this->upload->initialize($config);
		   for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
		   {
		    $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
		    $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
		    $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
		    $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
		    $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
		    if($this->upload->do_upload('file'))
		    {		    
		    $data = $this->upload->data();

		    $config['image_library'] = 'gd2';
	        $config['source_image'] = $data['full_path'];
	        $config['maintain_ratio'] = TRUE;
	        $config['width']     = 800;
	        $config['height']   = 266;

	        $this->load->library('image_lib', $config); 
	        $this->image_lib->resize();

			$SKU_Foto = $this->productoModel->addSkuFoto($SKU_IdSKU,$data["file_name"]);
		    }
		    echo $SKU_IdSKU;
		   }
		  }
	    }


	     public function agregar_variacion()
	    {

	      if($this->input->is_ajax_request())
	      {
	        $vaoId = $this->input->post('vaoId');
	        $varId = $this->input->post('varId');
	        $vaoNombre = $this->input->post('vaoNombre');
	        $varNombre = $this->input->post('varNombre');   
	        $colorVar = $this->input->post('colorVar');
			$a = array_combine($vaoId, $vaoNombre);
			$variacionID = array_unique($varId);
			$varNomUni= array_unique($varNombre); 

			$options = [
			            $a,
			            $colorVar
			        ];
			$options2 = [
			            $vaoId,
			            $colorVar
			        ];


	        $combinations = [[]];
	        $combidColor = [[]];

            for ($count = 0; $count < count($options2); $count++) {
                $tmp = [];
                foreach ($combidColor as $v1) {
                    foreach ($options2[$count] as $v2)
                        $tmp[] = array_merge($v1, [$v2]);

                }
                $combidColor = $tmp;
            }

            for ($count = 0; $count < count($options); $count++) {
                $tmp = [];
                foreach ($combinations as $v1) {
                    foreach ($options[$count] as $v2)
                        $tmp[] = array_merge($v1, [$v2]);

                }
                $combinations = $tmp;
            }

	    for ($i=0; $i < count($combinations); $i++) {
	        if (isset($_SESSION['provariacion']))
	        {

	          if(count($_SESSION['provariacion']) > 0)
	          {
	            $ultimo = end($_SESSION['provariacion']);
	            $count = $ultimo['numero'] + 1;
	          }
	          else
	          {
	            $count = count($_SESSION['provariacion']) + 1;
	          } 
	          
	          	$_SESSION['provariacion'][$count] = array('numero'=>$count,'varId'=>$variacionID[0],'vaoId'=>$combidColor[$i][0],'vaoNombre'=>$combinations[$i][0],'colorVar'=>$combinations[$i][1],'cantidadVariacion'=>0, 'varNombre'=>$varNomUni[0]);

	        }
	        else
	        {

	          $count = 1;
	     

        	$_SESSION['provariacion'][$count] = array('numero'=>$count,'varId'=>$variacionID[0],'vaoId'=>$combidColor[$i][0],'vaoNombre'=>$combinations[$i][0],'colorVar'=>$combinations[$i][1],'cantidadVariacion'=>0, 'varNombre'=>$varNomUni[0]);

			
	        }
	    }
        

	        $data['producto_variacion'] = $_SESSION['provariacion'];

	        //print_r($_SESSION['provariacion']);

	        $this->twig->parse('usuario/load/lista_variacion.twig', $data);
	      }
	      else
	      {
	        show_404();
	      }
	    }

	    public function eliminar_variacion()
	    {
	    	if($_SESSION['provariacion']){
	    		unset($_SESSION['provariacion']);
	    	}
	    }

	   
// 		public function _load()
// 	    {	      
// 	      $data['configuracion'] = $this->configuracionModel->configuracion();
// 	      $data['tienda'] = $this->configuracionModel->tienda();
// 	      return $data;
// 	    }
    }

?>