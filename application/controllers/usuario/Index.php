<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Index extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();

			$this->load->model("Local_model", "localModel");
			$this->load->model("Usuario_model", "usuarioModel"); 

			$this->load->helper(array('form', 'security'));
			$this->load->library(array('form_validation', 'tank_auth'));
			$this->lang->load('tank_auth');
        }

        public function perfil()
        {
            $data = $this->acl->load_datos('no', 'si');
            
            $data['pagina']['titulo'] = 'Perfil';
            $data['pagina']['menu'] = 'perfil';
            $data['bancos'] = $this->localModel->bancos();

            if($this->input->post('btnActualizar'))
            {
                $data['error'] = true;

                $this->form_validation->set_rules('dni', 'DNI', 'trim|required|xss_clean|min_length[8]|alpha_dash');
                $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
                
                $data['errors'] = array();

                if ($this->form_validation->run())
                {
                    $data['error'] = false;

                    $Per_IdPersona = $this->input->post('Per_IdPersona');
                    $Per_Dni = $this->input->post('dni');
                    $Per_Nombre = $this->input->post('nombre');
                    $Per_Sexo = $this->input->post('selSexo');
                    $Per_FechaNacimiento = $this->input->post('fecha_nacimiento');
                    $Per_Telefono = $this->input->post('telefono');
                    $Per_Celular = $this->input->post('celular');

                    $registro = $this->usuarioModel->editar_usuario($Per_IdPersona, $Per_Dni, $Per_Nombre, $Per_Sexo, $Per_FechaNacimiento, NULL, $Per_Telefono, $Per_Celular, NULL);

                    if($registro)
                    {
                        redirect('usuario/index/perfil');
                    }
                }
            }
            
            if($this->input->post('btnCambiarClave'))
            {
                $data['error'] = true;

                $this->form_validation->set_rules('clave', 'Contraseña Antigua', 'trim|required|xss_clean');
                $this->form_validation->set_rules('clave_nueva', 'Nueva Contraseña', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
                $this->form_validation->set_rules('clave_nueva2', 'Confirmar Nueva Contraseña', 'trim|required|xss_clean|matches[clave_nueva]');

                $this->form_validation->set_message("matches", "Las contraseñas no coinciden");

                $data['errors'] = array();

                if ($this->form_validation->run())
                {
                    $data['error'] = false;

                    $old_password = $this->form_validation->set_value('clave');
                    $new_password = $this->form_validation->set_value('clave_nueva');

                    if ($this->tank_auth->change_password($old_password, $new_password))
                    {
                        $this->_show_message($this->lang->line('auth_message_password_changed'));
                    } 
                    else 
                    {
                        $errors = $this->tank_auth->get_error_message();
                        foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                    }
                }
            }

            $data['datos_usuario'] = $this->usuarioModel->get_usuario($data['usuario']['id_usuario']);
            $data['banco_usuario'] = $this->usuarioModel->get_usuario_banco($data['usuario']['id_usuario']);
            
            $this->twig->parse('usuario/perfil.twig', $data);
        }

        public function vendedor()
        {
            $data = $this->acl->load_datos("no", "duenio");
            
            $data['pagina']['titulo'] = 'Gestión de Vendedores';
            $data['pagina']['menu'] = 'gestion';
            $data['pagina']['submenu'] = 'vendedores';

            $data['vendedores'] = $this->usuarioModel->get_vendedores_X_tienda($data['tienda']->Tie_IdTienda);

            $this->twig->parse('usuario/index/vendedor.twig', $data);
        }

        public function banco_usuario()
        {
        	if ($this->input->is_ajax_request()) 
            {
                $data = $this->acl->load_datos();
                $Ban_IdBanco = (int)$this->input->post('Ban_IdBanco');
                $Usb_Cuenta = $this->input->post('Usb_Cuenta');
                $Usb_Tipo = (int)$this->input->post('Usb_Tipo');

                $registrar = $this->usuarioModel->registrarCuentaUsuario($data['usuario']['id_usuario'],$Ban_IdBanco,$Usb_Cuenta,$Usb_Tipo);

                if ($registrar){
                    echo 1;
                }else{
                    echo 0;
                }

            }
        }

        public function crear_vendedor()
        {
        	$data = $this->acl->load_datos("no", "duenio");
	        
	        $data['pagina']['titulo'] = 'Registrar Vendedor';

	        if($this->input->post())
	        {
	        	$data['error'] = FALSE;
	        	$use_username = false;
                //$use_username = $this->config->item('use_username', 'tank_auth');
                if ($use_username) 
                {
                    $this->form_validation->set_rules('usuario', 'USUARIO', 'trim|required|min_length[4]|max_length[10]|alpha_dash');
                }
                $this->form_validation->set_rules('dni', 'DNI', 'trim|required');
                $this->form_validation->set_rules('nombre', 'NOMBRE', 'trim|required');
                $this->form_validation->set_rules('correo', 'CORREO', 'trim|required|valid_email|is_unique[usuario.Usu_Correo]');
                $this->form_validation->set_rules('clave', 'CLAVE', 'trim|required|min_length[3]|alpha_dash');

                $this->form_validation->set_message("required", "El campo %s es requerido");
                $this->form_validation->set_message("is_unique", "El campo %s ya está en uso");
                
                $data['errors'] = array();

                $email_activation = $this->config->item('email_activation', 'tank_auth');

                if($this->form_validation->run())
                {
                    $this->load->model("Persona_model", "personaModel");
                    $Per_Dni = $this->form_validation->set_value('dni');
                    $Per_Nombre = $this->form_validation->set_value('nombre');
                    $Per_Celular = $this->form_validation->set_value('celular');
                    $Usu_Correo = $this->form_validation->set_value('correo');
                    $Usu_Clave = $this->form_validation->set_value('clave');

                    $Per_IdPersona = $this->personaModel->get_id_persona_x_dni($Per_Dni);

                    if (!is_null($data = $this->tank_auth->create_usuario('', $Per_IdPersona, $Per_Dni, $Per_Nombre, $Per_Celular, $Usu_Correo, $Usu_Clave, 3, $data['tienda']->Tie_IdTienda,  $email_activation)))
                    {
                        $data['site_name'] = $this->config->item('website_name', 'tank_auth');
                        if($email_activation)
                        {
                            $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;
                            $this->_send_email('activate', $Usu_Correo, $data);
                            unset($data['Usu_Clave']); 
                            $data['estado_registrado'] = TRUE;
                            $data['mensaje'] = $this->lang->line('auth_message_registration_completed_1');
                        }
                        else
                        {
                            $data['login_redes'] = TRUE;
                            $data['email'] = $Usu_Correo;

                            if ($this->config->item('email_account_details', 'tank_auth')) 
                            {   
                                // $this->_send_email('welcome', $Usu_Correo, $data);
                            }

                            unset($data['Usu_Clave']);

                            redirect('usuario/index/vendedor');
                        }
                    }
                    else
                    {
                        $errors = $this->tank_auth->get_error_message();
                        foreach ($errors as $k => $v)   $data['errors'][$k] = $this->lang->line($v);
                    }
                }
                else
                {
                	$data["error"] = TRUE;
                }
	        }

	        $this->twig->parse('usuario/index/crear_vendedor.twig', $data);
        }

        function editar_vendedor($Usu_IdUsuario=false)
        {
            $Usu_IdUsuario = (int)$Usu_IdUsuario;

            $data = $this->acl->load_datos("no", "duenio");
            
            $data['pagina']['titulo'] = 'Editar Vendedor';
            
            if($this->input->post('btnActualizar'))
            {
                $data['error'] = true;

                $this->form_validation->set_rules('dni', 'DNI', 'trim|required|xss_clean|min_length[8]|alpha_dash');
                $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
                
                $data['errors'] = array();

                if ($this->form_validation->run())
                {
                    $data['error'] = false;

                    $Per_IdPersona = $this->input->post('Per_IdPersona');
                    $Per_Dni = $this->input->post('dni');
                    $Per_Nombre = $this->input->post('nombre');
                    $Per_Sexo = $this->input->post('selSexo');
                    $Per_FechaNacimiento = $this->input->post('fecha_nacimiento');
                    $Per_Telefono = $this->input->post('telefono');
                    $Per_Celular = $this->input->post('celular');

                    $registro = $this->usuarioModel->editar_usuario($Per_IdPersona, $Per_Dni, $Per_Nombre, $Per_Sexo, $Per_FechaNacimiento, NULL, $Per_Telefono, $Per_Celular, NULL);

                    if($registro)
                    {
                        redirect('usuario/index/vendedor');
                    }
                }
            }
            
            $data['datos_usuario'] = $this->usuarioModel->get_usuario($Usu_IdUsuario);
            
            $this->twig->parse('usuario/index/editar_vendedor.twig', $data);
        }

        function post_actualizar_estado()
        {
            if ($this->input->post()) 
            {
                 $data = $this->acl->load_datos("no", "duenio");

                $Usu_IdUsuario = $this->input->post('id');
                $Usu_Activated = $this->input->post('estado');

                $registra = $this->usuarioModel->cambiar_estado_usuario($Usu_IdUsuario, $Usu_Activated, $data['usuario']['id_usuario']);

                if ($registra) 
                {
                    echo "1";
                }
            } 
            else
            {
                show_404();
            }
        }

        function _send_email($type, $email, &$data)
        {
            $this->load->library('email');
            $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->to($email);
            $this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
            $this->email->message($this->load->view('usuario/email/'.$type.'-html', $data, TRUE));
            $this->email->set_alt_message($this->load->view('usuario/email/'.$type.'-txt', $data, TRUE));
            $this->email->send();
        }

        function _show_message($message)
        {
            $this->session->set_flashdata('message', $message);
            redirect('usuario/index/perfil');
        }

		public function cerrar_session()
		{
            $this->session->sess_destroy();

            redirect('');
		}
	}
?>