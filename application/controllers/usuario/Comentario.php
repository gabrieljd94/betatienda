<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Comentario extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();

			$this->load->model("Producto_model", "productoModel");
			$this->load->model("Tienda_model", "tiendaModel");
			$this->load->model("Configuracion_model", "configuracionModel");
			$this->load->model("Local_model", "localModel"); 

        }

		public function index()
		{
	        $data = $this->acl->load_datos();
	        
	        $data['pagina']['titulo'] = 'Comentarios';
	        // $data['comentarios'] = $this->productoModel->get_comentarios_tienda($data['tienda']->Tie_IdTienda);
	        $data['comentarios'] = $this->productoModel->get_respuestas_tienda($data['tienda']->Tie_IdTienda);
	        $data['productos'] = $this->productoModel->productos("WHERE prt.Tie_IdTienda = ".$data['tienda']->Tie_IdTienda." ORDER BY pro.Pro_IdProducto desc");
	        $this->twig->parse('usuario/comentario.twig', $data);
		}

		public function responder()
		{
	        if ($this->input->is_ajax_request()) {
		    $data = $this->acl->load_datos();
			$Com_IdComentario = $this->input->post('Com_IdComentario');
			$Respuesta = $this->input->post('Respuesta');
			$registro = $this->productoModel->responderComentario($Com_IdComentario, $Respuesta);

				if ($registro) {
					echo 1;
				} else {
					echo 0;
				}
			}
		}


	}
?>