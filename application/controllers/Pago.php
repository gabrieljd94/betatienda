<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pago extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
		}

    function culqi()
    {
      if($this->input->post())
      {
        $data = $this->acl->load_datos("no", "duenio");

        $this->load->model("Pago_model", "pagoModel");

        $Tie_IdTienda = $data['usuario']['id_tienda'];
        $Usu_IdUsuario = $data['usuario']['id_usuario'];
        $Pag_Descripcion = $this->input->post('descripcion');
        $Pag_Monto = $this->input->post('monto');
        $Pag_Cuota = $this->input->post('cuota');
        $Pag_Correo = $this->input->post('correo');
        $token = $this->input->post('token');

        $SECRET_KEY = 'sk_test_1RygIgFFeJWK8ZI3';

        require_once(APPPATH.'libraries/culqi.php');
        require_once(APPPATH.'libraries/Requests.php');
        
        Requests::register_autoloader();

        $culqi = new Culqi\Culqi(array('api_key' => $SECRET_KEY));

        $charge = $culqi->Charges->create(
          array(
            "amount" => $Pag_Monto*100,
            "capture" => true,
            "currency_code" => "PEN",
            "description" => $Pag_Descripcion,
            "email" => $Pag_Correo,
            "installments" => 0,
            "source_id" => $token
          )
        );

        if($charge)
        {
          $registro = $this->pagoModel->registrar_pago($Tie_IdTienda, $data['usuario']['id_usuario'], $Pag_Descripcion, $Pag_Correo, $Pag_Monto, $Pag_Cuota);

          if($registro)
          {
            echo 1;
          }
          else
          {
            echo 0;
          }
        }
      }
    }
	}

?>