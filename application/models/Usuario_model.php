<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Usuario_model extends CI_Model
    {
        private $nombre_tabla = 'usuario';
        function __construct()
        {           
            parent::__construct();
        }
        
        function crear_usuario($Usu_Clave)
        {
            return  $this->clave_hash($Usu_Clave);
        }

        function usuario_login($Usu_Correo, $Usu_Clave)
        {
            $this->db->select('Usu_Clave');
            $this->db->from($this->nombre_tabla);
            $this->db->where('Usu_Correo', $Usu_Correo);
            //return true;
            $hash = $this->db->get()->row('Usu_Clave');
            return $this->verificar_clave_hash($Usu_Clave, $hash);
        }

        function verificar_usuario($Usu_Nombre)
        {
            $sql = "SELECT EXISTS(SELECT * FROM usuario WHERE Usu_Nombre = '".$Usu_Nombre."' LIMIT 1) as a";
            $query = $this->db->query($sql);
            return $query->row();
        }

        function get_vendedores_X_tienda($Tie_IdTienda)
        {
            $sql = "SELECT usu.Usu_IdUsuario, per.Per_Nombre, usu.Usu_Correo, usu.Usu_Activated   
                    FROM usuario usu
                    INNER JOIN persona per ON per.Per_IdPersona = usu.Per_IdPersona
                    WHERE usu.Rol_IdRol = 3 AND usu.Tie_IdTienda = $Tie_IdTienda";

            $query = $this->db->query($sql);

            return $query->result();
        }

        function get_usuario_by_nombre($Usu_Correo)
        {
            $this->db->select('Usu_IdUsuario');
            $this->db->from($this->nombre_tabla);
            $this->db->where('Usu_Correo', $Usu_Correo);
            return $this->db->get()->row('Usu_IdUsuario');
        }

        function get_usuario_by_correo($Usu_Correo)
        {
            $this->db->select('Usu_IdUsuario');
            $this->db->from($this->nombre_tabla);
            $this->db->where('Usu_Correo', $Usu_Correo);
            return $this->db->get()->row('Usu_IdUsuario');
        }

        function get_usuario($Usu_IdUsuario)
        {
            $this->db->select('Usu_IdUsuario, usu.Per_IdPersona, Usu_Correo, per.Per_Dni, per.Per_Nombre, per.Per_Sexo, per.Per_FechaNacimiento, per.Per_Telefono, per.Per_Celular, per.Per_Ocupacion, Tie_IdTienda, Rol_IdRol');
            $this->db->from('usuario as usu');
            $this->db->join('persona AS per', 'per.Per_IdPersona = usu.Per_IdPersona');
            $this->db->where('Usu_IdUsuario', $Usu_IdUsuario);
            $query = $this->db->get();
            return $query->row();
        }

        function get_usuario_banco($Usu_IdUsuario)
        {
            $this->db->select('*');
            $this->db->from('usuario_banco as usb');
            $this->db->join('banco as ban', 'ban ON ban.Ban_IdBanco = usb.Ban_IdBanco');
            $this->db->where('Usu_IdUsuario', $Usu_IdUsuario);
            $query = $this->db->get();
            return $query->row();
        }

        function clave_hash($Usu_Clave)
        {
            return password_hash($Usu_Clave, PASSWORD_BCRYPT);
        }

        function verificar_clave_hash($Usu_Clave, $hash)
        {
            return password_verify($Usu_Clave, $hash);
        }

        function get_usuarios($Cen_IdCentro) 
        {
            $this->db->select('usu.Usu_IdUsuario, usu.Per_Dni, per.Per_Nombre, per.Per_ApellidoPaterno, per.Per_ApellidoMaterno, per.Per_Telefono, per.Per_Sexo, per.Per_Direccion, per.Per_Celular, usu.Usu_Estado, hue.Hue_IdHuella');
            $this->db->from('usuario as usu');
            $this->db->join('persona AS per', 'per.Per_Dni = usu.Per_Dni');
            $this->db->join('huella AS hue', 'hue.Per_Dni = usu.Per_Dni', 'left');
            $this->db->where('usu.Cen_IdCentro', $Cen_IdCentro);
            $this->db->where('usu.Tiu_IdTipoUsuario', 4);
            $this->db->order_by("usu.Per_Dni", "asc");
            $query = $this->db->get();
            return $query->result();
        }
         
        function verificar_clave($Usu_Nombre, $Usu_Clave) 
        {        
            $this->db->select('Usu_Clave');
            $this->db->from($this->nombre_tabla);
            $this->db->where('Usu_Nombre', $Usu_Nombre);
            $hash = $this->db->get()->row('Usu_Clave');
            return password_verify($Usu_Clave, $hash);
        }

        function registrar_usuario($Usu_Tipo, $Per_Dni, $Per_Nombre, $Per_ApellidoPaterno, $Per_ApellidoMaterno, $Per_Sexo, $Per_FechaNacimiento, $Per_Ocupacion, $Per_Direccion, $Per_Correo, $Per_Telefono, $Per_Celular, $Cen_IdCentro, $tabla_persona) 
        {
            try 
            {
                $usu_n = $Per_Nombre[0];
                $Usu_Nombre = $usu_n . $Per_ApellidoPaterno;
                $Usu_Nombre = strtolower($Usu_Nombre);
                $Usu_Clave = password_hash($Per_Dni, PASSWORD_BCRYPT);
                if($tabla_persona == 'si')
                {
                    $this->db->query("INSERT INTO persona(Per_Dni, Per_Nombre, Per_ApellidoPaterno, Per_ApellidoMaterno, Per_Sexo, Per_FechaNacimiento, Per_Direccion, Per_Telefono, Per_Celular, Per_Ocupacion, Per_Correo) VALUES (" . $this->db->escape($Per_Dni) . ", " . $this->db->escape($Per_Nombre) . ", " . $this->db->escape($Per_ApellidoPaterno) . ", " . $this->db->escape($Per_ApellidoMaterno) . ", " . $this->db->escape($Per_Sexo) . ", " . $this->db->escape($Per_FechaNacimiento) . ", " . $this->db->escape($Per_Direccion) . ", " . $this->db->escape($Per_Telefono) . ", " . $this->db->escape($Per_Celular) . ", " . $this->db->escape($Per_Ocupacion) . ", " . $this->db->escape($Per_Correo) . ")");
                }
                
                $this->db->query("INSERT INTO usuario(Per_Dni, Usu_Nombre, Usu_Clave, Tiu_IdTipoUsuario, Cen_IdCentro, Usu_Estado) VALUES (" . $this->db->escape($Per_Dni) . ", " . $this->db->escape($Usu_Nombre) . ", " . $this->db->escape($Usu_Clave) . ", " . $this->db->escape($Usu_Tipo) . ", " . $this->db->escape($Cen_IdCentro) . ", " . $this->db->escape(1) . ")");
                $this->db->trans_start();
                $this->db->trans_complete();
                if ($this->db->trans_status() === TRUE)
                {
                    return true;
                }
                return false;
            }
            catch (Exception $e) 
            {
                return false;
            }
        }

        function editar_usuario($Per_IdPersona, $Per_Dni, $Per_Nombre, $Per_Sexo, $Per_FechaNacimiento, $Per_Direccion, $Per_Telefono, $Per_Celular, $Per_Ocupacion)
        {
            try
            {
                $this->db->trans_start();

                $this->db->set('Per_Dni', $Per_Dni);
                $this->db->set('Per_Nombre', $Per_Nombre);
                $this->db->set('Per_Sexo', $Per_Sexo);
                $this->db->set('Per_FechaNacimiento', $Per_FechaNacimiento);
                $this->db->set('Per_Direccion', $Per_Direccion);
                $this->db->set('Per_Telefono', $Per_Telefono);
                $this->db->set('Per_Celular', $Per_Celular);
                $this->db->set('Per_Ocupacion', $Per_Ocupacion);
                $this->db->where('Per_IdPersona', $Per_IdPersona);
                $this->db->update('persona');

                $this->db->trans_complete();

                if ($this->db->trans_status() === TRUE)
                {
                    return true;
                }

                return false;
            }
            catch (Exception $e)
            {
                return false;
            }
        }

        function modificar_clave($Usu_IdUsuario, $Usu_Clave)
        {
            try 
            {
                $this->db->trans_start();
                //cambiar clave
                $this->db->set('Usu_Clave', $this->clave_hash($Usu_Clave));
                $this->db->where('Usu_IdUsuario', $Usu_IdUsuario);
                $this->db->update($this->nombre_tabla);
                
                //registrar en la tabla bitacora
                $this->registrar_bitacora($this->nombre_tabla, 'cambiar_clave', 'Usu_IdUsuario', $Usu_IdUsuario, $Usu_Clave, $Usu_IdUsuario);
            
                $this->db->trans_complete();
                if($this->db->trans_status() == TRUE)
                {
                    return true;
                }
                return false;
            } 
            catch (Exception $e) 
            {
                return false;
            }
        }
        function login($Usu_Nombre) 
        {
            try
            {
                $sql = "CALL sp_login(?)";
                $query = $this->db->query($sql, array($Usu_Nombre));
                $query->next_result(); 
                return $query->row();               
            }
            catch(Exception $ex)
            {
                return $e->getMessage();
            }
        }
        
        function cambiar_estado_usuario($Usu_IdUsuario, $Usu_Activated, $Usu_IdUsuario_)
        {
            try 
            {
                $this->db->trans_start();

                //cambiar estado
                $this->db->set('Usu_Activated', $Usu_Activated);
                $this->db->where('Usu_IdUsuario', $Usu_IdUsuario);
                $this->db->update($this->nombre_tabla);
                
                //registrar en la tabla bitacora
                $this->bitacoraModel->registrar_bitacora($this->nombre_tabla, 'cambiar_estado_usuario', 'Usu_IdUsuario', $Usu_IdUsuario, '', '', $Usu_IdUsuario_);

                $this->db->trans_complete();

                if($this->db->trans_status() == TRUE)
                {
                    return true;
                }

                return false;
            } 
            catch (Exception $e) 
            {
                return false;
            }
        }
        
        function registrar_bitacora($Bit_Tabla, $Bit_Accion, $Bit_Campo, $Bit_IdRegistro, $Bit_Valor, $Usu_IdUsuario)
        {
            $this->db->set('Bit_Tabla', $Bit_Tabla);
            $this->db->set('Bit_Accion', $Bit_Accion);
            $this->db->set('Bit_Campo', $Bit_Campo);
            $this->db->set('Bit_IdRegistro', $Bit_IdRegistro);
            $this->db->set('Bit_Valor', $Bit_Valor);
            $this->db->set('Usu_IdUsuario', $Usu_IdUsuario);
            
            $this->db->insert('bitacora');
        }        

        function registrarCuentaUsuario($Usu_IdUsuario,$Ban_IdBanco,$Usb_Cuenta,$Usb_Tipo)
        {
            $this->db->set('Usu_IdUsuario', $Usu_IdUsuario);
            $this->db->set('Ban_IdBanco', $Ban_IdBanco);
            $this->db->set('Usb_Cuenta', $Usb_Cuenta);
            $this->db->set('Usb_Tipo', $Usb_Tipo);         
            $this->db->insert('usuario_banco');
        }
    }
?>