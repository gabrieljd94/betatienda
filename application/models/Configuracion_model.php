<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Configuracion_model extends CI_Model
    {
        private $nombre_tabla = 'configuracion';
        function __construct()
        {           
            parent::__construct();
        }
        
        function configuracion($Tienda_Tie_IdTienda)
        {
            $this->db->select('*');
            $this->db->from($this->nombre_tabla); 
            $this->db->where('tienda_Tie_IdTienda',$Tienda_Tie_IdTienda);  
            return $this->db->get()->row();
        }
                      
        function get_colores()
        {
            $this->db->select('*');
            $this->db->from('colores');  
            $query = $this->db->get();
            return $query->result();
        }

        function get_coloresProducto($Tie_IdTienda)
        {
            $this->db->select('col.Col_Hex, sku.SKU_Color, sku.producto_Pro_IdProducto');
            $this->db->from('sku as sku');  
            $this->db->join('producto_tienda as prt','prt on prt.Pro_IdProducto = sku.producto_Pro_IdProducto');  
            $this->db->join('colores as col','col on col.Col_Nombre = sku.SKU_Color');  
            $this->db->where('prt.Tie_IdTienda',$Tie_IdTienda); 
            //$this->db->group_by('');
            $query = $this->db->get();
            return $query->result();
        }
                                 
        function get_colores_2($PVO_IdPVO,$Pro_IdProducto)
        {
            $sql = "SELECT sku.SKU_Color from sku as sku inner join sku_pvo AS spv ON spv.SKU_SKU_IdSKU = sku.SKU_IdSKU inner join producto_variacion AS prv ON prv.Prv_IdProductoVariacion = spv.producto_variacion_Prv_IdProductoVariacion inner join producto_variacion_opcion AS pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO where sku.producto_Pro_IdProducto = $Pro_IdProducto and pvo.PVO_IdVao = $PVO_IdPVO";
            $query = $this->db->query($sql);   
            return $query->result();
        }
                
        function tienda()
        {
            $this->db->select('*');
            $this->db->from('tienda');
            return $this->db->get()->row();
        }
                
        function existeTienda($Tie_IdTienda)
        {
            $this->db->select('*');
            $this->db->from('configuracion');
            $this->db->where('Tienda_Tie_IdTienda', $Tie_IdTienda);
            return $this->db->get()->row();
        }
        function registrarColorPrecio($Mostrar_Precio, $hex, $hex2, $Tie_IdTienda){
            $this->db->set('Con_ColorHexa', $hex);
            $this->db->set('Con_ColorBtn', $hex2);
            $this->db->set('Mostrar_Precio', $Mostrar_Precio);
            $this->db->where('Tienda_Tie_IdTienda', $Tie_IdTienda);
            $this->db->update('configuracion');
        }
        function insertColorPrecio($Mostrar_Precio, $hex, $hex2, $Tie_IdTienda){
            $this->db->set('Con_ColorHexa', $hex);
            $this->db->set('Con_ColorBtn', $hex2);
            $this->db->set('Mostrar_Precio', $Mostrar_Precio);
            $this->db->set('Tienda_Tie_IdTienda', $Tie_IdTienda);
            $this->db->insert('configuracion');
        }
        
    }
?>