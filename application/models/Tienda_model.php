<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Tienda_model extends CI_Model
    {
        private $nombre_tabla = 'tienda';

        function __construct()
        {           
            parent::__construct();
        }

        function get_tienda($Tie_IdTienda)
        {
            $this->db->select('Tie_Subdominio');
            $this->db->from('tienda');
            $this->db->where('Tie_IdTienda', $Tie_IdTienda);

            $query = $this->db->get();

            return $query->row();
        }
        
        function get_tienda_info($Tie_IdTienda)
        {
            $this->db->select('*');
            $this->db->from('tienda');
            $this->db->where('Tie_IdTienda',$Tie_IdTienda);
        
            return $this->db->get()->row();
        }

        function get_tienda_x_subdominio($Tie_Subdominio)
        {
            $this->db->select('*');
            $this->db->from('tienda');
            $this->db->where('Tie_Subdominio', $Tie_Subdominio);
        
            return $this->db->get()->row();
        }

        function get_detalleTienda($Tie_IdTienda)
        {
            $this->db->select('*');
            $this->db->from('tienda_detalle');
            $this->db->where('tienda_Tie_IdTienda',$Tie_IdTienda);
        
            $query = $this->db->get();
            return $query->row();
        }

        function get_cantidad_pagos_x_tienda($Tie_IdTienda)
        {
            $sql = "SELECT COUNT(*) as Cantidad
                    FROM pago pag
                    WHERE Tie_IdTienda = $Tie_IdTienda";
                    
            $query = $this->db->query($sql);

            return $query->row();
        }

        function eliminar_detalle_tienda($Tid_IdTiendaDetalle)
        {
            $this->db->where('Tid_IdTiendaDetalle', $Tid_IdTiendaDetalle);
            $this->db->delete('tienda_detalle');
        }

        function edit_nombre_tienda($Tie_IdTienda,$nTienda)
        {
            $this->db->set('Tie_Nombre', $nTienda);
            $this->db->where('Tie_IdTienda', $Tie_IdTienda);
            $this->db->update('tienda');
 
        }

        function actualizar_porcentaje_tienda($Tie_PorcentajeCotizacion, $Tie_IdTienda)
        {
            $this->db->set('Tie_PorcentajeCotizacion', $Tie_PorcentajeCotizacion);
            $this->db->where('Tie_IdTienda', $Tie_IdTienda);
            $this->db->update($this->nombre_tabla);

            if ($this->db->affected_rows() >= 0) 
            {
                return TRUE;
            }
            
            return FALSE;
        }

        function insertLocal($Tie_IdTienda,$nombreLocal,$dirLocal,$tlfLocal,$lat,$long)
        {
            $this->db->trans_start();
            $this->db->set('Loc_Nombre', $nombreLocal);
            $this->db->set('tienda_Tie_IdTienda', $Tie_IdTienda);

            if ($this->db->insert('local'))
            {
                $Loc_IdLocal = $this->db->insert_id();
            };

            $this->db->set('Loc_Direccion', $dirLocal);
            $this->db->set('Loc_lat', $lat);
            $this->db->set('Loc_long', $long);
            $this->db->set('local_Loc_IdLocal', $Loc_IdLocal);
            $this->db->insert('local_direccion');

            $this->db->set('Loc_Numero', $tlfLocal);
            $this->db->set('local_Loc_IdLocal', $Loc_IdLocal); 
            $this->db->insert('local_numero');

            $this->db->trans_complete();

            $this->db->trans_status();
        }

        function updateLocal($Loc_IdLocal, $Loc_Nombre, $Loc_Direccion, $Loc_Numero, $Loc_Lat, $Loc_Long)
        {
            $this->db->trans_start();

            $this->db->set('Loc_Nombre', $Loc_Nombre);
            $this->db->where('Loc_IdLocal', $Loc_IdLocal);
            $this->db->update('local');

            $this->db->set('Loc_Direccion', $Loc_Direccion);
            $this->db->set('Loc_Lat', $Loc_Lat);
            $this->db->set('Loc_Long', $Loc_Long);
            $this->db->where('local_Loc_IdLocal', $Loc_IdLocal);
            $this->db->update('local_direccion');

            $this->db->set('Loc_Numero', $Loc_Numero);
            $this->db->where('local_Loc_IdLocal', $Loc_IdLocal);
            $this->db->update('local_numero');

            $this->db->trans_complete();

            $this->db->trans_status();
        }

        function insertDetalleTienda($Tie_IdTienda,$DetalleTienda)
        {
            $this->db->set('tienda_Tie_IdTienda', $Tie_IdTienda);
            $this->db->set('Tid_Descripcion', $DetalleTienda);
            if ($this->db->insert('tienda_detalle')){
                $Tid_IdTiendaDetalle = $this->db->insert_id();
            };
            return $Tid_IdTiendaDetalle;
        }

        function updateDetalleTienda($Tid_IdTiendaDetalle,$DetalleTienda)
        {
            $this->db->set('Tid_Descripcion', $DetalleTienda);
            $this->db->where('Tid_IdTiendaDetalle', $Tid_IdTiendaDetalle);
            $this->db->update('tienda_detalle');   
           
            return $Tid_IdTiendaDetalle;
        }

        function addDetalleFotos($Tid_IdTiendaDetalle,$Tid_Img)
        {
            $this->db->set('Tid_Img', $Tid_Img);
            $this->db->where('Tid_IdTiendaDetalle', $Tid_IdTiendaDetalle);
            $this->db->update('tienda_detalle');    
        }

        function addLogoFoto($Tie_IdTienda,$Tie_Logo)
        {
            $this->db->set('Tie_Logo', $Tie_Logo);
            $this->db->where('Tie_IdTienda', $Tie_IdTienda);
            $this->db->update('tienda');    
        }

        function deleteLocal($nombre_tabla,$id,$nombre_variable)
        {
            $this->db->trans_start();
            
            $this->db->where($nombre_variable, $id);
            $this->db->delete($nombre_tabla);      
            $this->db->where('local_Loc_IdLocal', $id);
            $this->db->delete('local_direccion');
            $this->db->where('local_Loc_IdLocal', $id);
            $this->db->delete('local_numero');
            $this->db->trans_complete();

            if ($this->db->trans_status() == TRUE)
            {
               return true;
            }

            return false;
        }

        function get_local_info($Tie_IdTienda)
        {
            $sql = "SELECT tie.Tie_Nombre, tie.Tie_Descripcion, tie.Tie_ruc_dni, loc.Loc_Nombre,loc.Loc_IdLocal, lod.Loc_Direccion, lon.Loc_Numero, lod.Loc_Long, lod.Loc_Lat FROM tienda as tie 
                INNER JOIN local as loc ON loc.Tienda_Tie_IdTienda = tie.Tie_IdTienda 
                INNER JOIN local_direccion as lod ON lod.Local_Loc_IdLocal = loc.Loc_IdLocal 
                INNER JOIN local_numero as lon ON lon.Local_Loc_IdLocal = loc.Loc_IdLocal  where tie.Tie_IdTienda = $Tie_IdTienda";
                    
            $query = $this->db->query($sql);

            return $query->result();
        }

        function get_cantidad_pedidos_pendientes($Tie_IdTienda)
        {
            $sql = "SELECT COUNT(*) as Cantidad
                    FROM tienda_pedido tip
                    WHERE Tie_IdTienda = $Tie_IdTienda AND Tip_Estatus = 1";
                    
            $query = $this->db->query($sql);

            return $query->row();
        }
    }
?>