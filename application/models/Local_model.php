<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Local_model extends CI_Model
    {
        private $nombre_tabla = 'regions';
        
        function __construct()
        {
            parent::__construct();
        }
        
        function get_local($Loc_IdLocal)
        {
            $sql = "SELECT loc.Loc_IdLocal, loc.Loc_Nombre, loc.Loc_IdLocal, lod.Loc_Direccion, lon.Loc_Numero, lod.Loc_Long, lod.Loc_Lat 
                FROM local as loc
                INNER JOIN local_direccion as lod ON lod.Local_Loc_IdLocal = loc.Loc_IdLocal
                INNER JOIN local_numero as lon ON lon.Local_Loc_IdLocal = loc.Loc_IdLocal
                WHERE loc.Loc_IdLocal = $Loc_IdLocal";

            $query = $this->db->query($sql);

            return $query->row();
        }

        function region()
        {
           $this->db->select('id,name');
           $this->db->from('regions');
        
           $query = $this->db->get();
           return $query->result();
        }

        function provincia($idRegion)
        {
            $this->db->select('id,name');
            $this->db->from('provinces');
            $this->db->where('region_id',$idRegion);
        
            $query = $this->db->get();
            return $query->result();
        }

        function distrito($idProvincia,$idRegion)
        {
            $this->db->select('id,name');
            $this->db->from('districts');
            $this->db->where('region_id',$idProvincia);
        
            $query = $this->db->get();
            return $query->result();
        }

        function bancos()
        {
            $this->db->select('*');
            $this->db->from('banco');
        
            $query = $this->db->get();
            return $query->result();
        }
    }
?>