<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Cotizacion_model extends CI_Model
    {
        private $nombre_tabla = 'pedido';

        function __construct()
        {
            parent::__construct();
        }
        
        function enviarPedido($arrData, $carrito, $Usu_IdUsuario, $tienda_usuario, $array_id_tienda)
        {
            $this->db->trans_start();

            //registro pedido
            $this->db->set('Ped_Datos', $arrData[0]);
            $this->db->set('Ped_Telefono', $arrData[1]);
            $this->db->set('Ped_Correo', $arrData[2]);
            $this->db->set('Region_Id', $arrData[3]);
            $this->db->set('Provincia_Id', $arrData[4]);
            $this->db->set('Distrito_Id', $arrData[5]);
            $this->db->set('Ped_Direccion', $arrData[6]);
            $this->db->set('Ped_Estatus', 1);
            $this->db->set('Ped_Comentario', $arrData[7]);
            $this->db->set('Usu_IdUsuario', $Usu_IdUsuario);
            
            if ($this->db->insert('pedido'))
            {
                $Ped_IdPedido = $this->db->insert_id();
            };

            foreach ($carrito as $car)
            {
                $tienda = $this->get_id_tienda_x_sku($car['id']);

                if($tienda_usuario)
                {
                    if($tienda_usuario != $tienda->Tie_IdTienda)
                    {
                        $this->registrar_pedido_detalle($Ped_IdPedido, $car['id'], $car['qty']);
                    }
                }
                else
                {
                    $this->registrar_pedido_detalle($Ped_IdPedido, $car['id'], $car['qty']);
                }
            }

            foreach ($array_id_tienda as $Tie_IdTienda) 
            {
                if($tienda_usuario)
                {
                    if($tienda_usuario != $Tie_IdTienda)
                    {
                        $this->registrar_tienda_pedido($Tie_IdTienda, $Ped_IdPedido);
                    }
                }
                else
                {
                    $this->registrar_tienda_pedido($Tie_IdTienda, $Ped_IdPedido);
                }
            }
            
            $this->db->trans_complete();
            
            if ($this->db->trans_status() == TRUE) 
            {
               return $Ped_IdPedido;
            }

            return null;
        }
        
        function getPedidos($Tie_IdTienda)
        {
            $this->db->select('*');
            $this->db->from('pedido');
            $this->db->where('tienda_Tie_IdTienda',$Tie_IdTienda);
        
            $query = $this->db->get();
            return $query->result();
        }

        function insertGuia($Ven_IdVenta,$nguia)
        {
            
            $this->db->set('Ven_IdVenta',$Ven_IdVenta);
            $this->db->set('Gui_Numero',$nguia);
            if ($this->db->insert('guia_venta'))
            {
                $Gui_Idguia = $this->db->insert_id();
            };

            $this->db->set('Ven_Estado',3);
            $this->db->where('Ven_IdVenta',$Ven_IdVenta);
            $this->db->update('venta');


            return $Gui_Idguia;
        }

        function addGuia($Guia_IdGuia,$Foto_Img)
        {
            $this->db->set('Gui_Foto',$Foto_Img);
            $this->db->where('Gui_IdGuia',$Guia_IdGuia);
            $this->db->update('guia_venta');

        }

        function get_pedidos_x_tienda($Tie_IdTienda)
        {
            $sql = "SELECT ped.*, tip.Tip_Estatus, per.Per_Nombre
                    FROM pedido ped
                    INNER JOIN tienda_pedido tip ON tip.Ped_IdPedido = ped.Ped_IdPedido 
                    INNER JOIN usuario usu ON usu.Usu_IdUsuario = ped.Usu_IdUsuario 
                    INNER JOIN persona per ON per.Per_IdPersona = usu.Per_IdPersona
                    WHERE tip.Tie_IdTienda = $Tie_IdTienda";
            $query = $this->db->query($sql);   
            return $query->result();   
        }

        function get_pedido_reporte($Tie_IdTienda, $condicion = "")
        {            
            $sql = "SELECT * 
                    FROM pedido WHERE tienda_Tie_IdTienda = $Tie_IdTienda $condicion";
            $query = $this->db->query($sql);
            return $query->result();
        }

        function get_producto_reporte($Tie_IdTienda, $condicion = "")
        {            
            $sql = "SELECT ped.Ped_IdPedido, pro.Pro_Nombre, unm.Unm_Nombre, ped.Ped_Datos, ped.Ped_Correo,ped.Ped_Direccion, ped.Ped_Telefono, 
                    ped.Ped_Estatus, ped.Ped_FechaCreacion, sku.SKU_IdSKU, pde.Ped_Cantidad, sku.SKU_Img, ped.tienda_Tie_IdTienda, SUM(pde.Ped_Cantidad) as Cantidad, var.Var_Nombre, vao.Vao_Nombre
                    FROM pedido as ped 
                    INNER JOIN pedido_detalle as pde ON pde.Pedido_Ped_IdPedido = ped.Ped_IdPedido 
                    INNER JOIN sku as sku ON sku.SKU_IdSku = pde.SKU_IdSKU
                    INNER JOIN sku_pvo as spv ON spv.SKU_SKU_IdSKU = sku.SKU_IdSku 
                    INNER JOIN producto_variacion as prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion
                    INNER JOIN producto_variacion_opcion as pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO  
                    INNER JOIN producto as pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto 
                    INNER JOIN producto_unidad as pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto 
                    INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad
                    INNER JOIN variacion as var ON var.Var_IdVariacion = prv.Prv_IdVariacion
                    INNER JOIN variacion_opcion as vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao 
                    WHERE ped.tienda_Tie_IdTienda = $Tie_IdTienda $condicion
                    GROUP BY pde.SKU_IdSKU";
            $query = $this->db->query($sql);
            return $query->result();
        }

        function getPedidoDetalle($idPedido)
        {
            $sql = "SELECT ped.Ped_IdPedido, pro.Pro_Nombre, unm.Unm_Nombre, ped.Ped_Datos, ped.Ped_Correo,ped.Ped_Direccion, ped.Ped_Telefono, ped.Ped_Estatus, ped.Ped_FechaCreacion, sku.SKU_IdSKU, pde.Ped_Cantidad, sku.SKU_Img, ped.tienda_Tie_IdTienda FROM pedido as ped INNER JOIN pedido_detalle as pde ON pde.Pedido_Ped_IdPedido = ped.Ped_IdPedido INNER JOIN sku as sku ON sku.SKU_IdSku = pde.SKU_IdSKU INNER JOIN producto as pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto INNER JOIN producto_unidad as pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad WHERE ped.Ped_IdPedido = $idPedido";
            $query = $this->db->query($sql);
            return $query->result();
        }

        function get_datos_comprador_pedido($Ped_IdPedido)
        {
            $sql = "SELECT ped.Ped_IdPedido, per.Per_Nombre, ped.Ped_Direccion, ped.Ped_Telefono, ped.Ped_Estatus, ped.Ped_FechaCreacion, per.Per_Correo
                FROM pedido as ped 
                INNER JOIN usuario usu ON usu.Usu_IdUsuario = ped.Usu_IdUsuario
                INNER JOIN persona per ON per.Per_IdPersona = usu.Per_IdPersona
                WHERE ped.Ped_IdPedido = $Ped_IdPedido";

            $query = $this->db->query($sql);

            return $query->row(); 
        }

        function get_detalle_pedido_x_tienda($idPedido, $Tie_IdTienda)
        {
            $sql = "SELECT tip.Tip_IdTiendaPedido, ped.Ped_IdPedido, pro.Pro_IdProducto, pro.Pro_Nombre, unm.Unm_Nombre, ped.Ped_Datos, ped.Ped_Estatus, ped.Ped_FechaCreacion, sku.SKU_IdSKU, pde.Ped_Cantidad, sku.SKU_Img, ped.tienda_Tie_IdTienda, pro.Pro_PrecioRango
                FROM pedido as ped 
                INNER JOIN tienda_pedido as tip ON tip.Ped_IdPedido = ped.Ped_IdPedido AND tip.Tie_IdTienda = $Tie_IdTienda
                INNER JOIN pedido_detalle as pde ON pde.Pedido_Ped_IdPedido = ped.Ped_IdPedido 
                INNER JOIN sku as sku ON sku.SKU_IdSku = pde.SKU_IdSKU
                INNER JOIN producto as pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto
                INNER JOIN producto_tienda as prt ON prt.Pro_IdProducto = pro.Pro_IdProducto
                INNER JOIN producto_unidad as pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto 
                INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad
                WHERE ped.Ped_IdPedido = $idPedido AND prt.Tie_IdTienda = $Tie_IdTienda";

            $query = $this->db->query($sql);
            return $query->result();
        }
        
        function getPedidoDetalleVar($idPedido)
        {
            $sql = "SELECT ped.Ped_IdPedido, ped.Ped_Datos, ped.Ped_Correo,ped.Ped_Direccion, ped.Ped_Estatus, ped.Ped_FechaCreacion, sku.SKU_IdSKU, pde.Ped_Cantidad, sku.SKU_Img, pro.Pro_Nombre, var.Var_Nombre, vao.Vao_Nombre  FROM pedido as ped 
                INNER JOIN pedido_detalle as pde ON pde.Pedido_Ped_IdPedido = ped.Ped_IdPedido 
                INNER JOIN sku as sku ON sku.SKU_IdSku = pde.SKU_IdSKU 
                INNER JOIN sku_pvo as spv ON spv.SKU_SKU_IdSKU = sku.SKU_IdSku 
                INNER JOIN producto_variacion as prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion 
                INNER JOIN producto_variacion_opcion as pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO 
                INNER JOIN producto as pro ON prv.Producto_Pro_IdProducto = pro.Pro_IdProducto
                INNER JOIN variacion as var ON var.Var_IdVariacion = prv.Prv_IdVariacion
                INNER JOIN variacion_opcion as vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao 
                WHERE ped.Ped_IdPedido = $idPedido";
            $query = $this->db->query($sql);
            return $query->result();
        }
        
        /////// CODIGO DE PERFIL DE USUARIO COMPRADOR

                
        function get_pedidos_usuario($Usu_IdUsuario)
        {
            $this->db->select('*');
            $this->db->from('pedido as ped');
            $this->db->where('Usu_IdUsuario',$Usu_IdUsuario);
            $query = $this->db->get();
            return $query->result();
        }

        function get_id_tienda_x_sku($SKU_IdSKU)
        {
            $sql = "SELECT prt.Tie_IdTienda
                    FROM sku AS sku
                    INNER JOIN producto AS pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto
                    INNER JOIN producto_tienda AS prt ON prt.Pro_IdProducto = pro.`Pro_IdProducto`
                    WHERE sku.SKU_IdSKU = $SKU_IdSKU";

            $query = $this->db->query($sql);

            return $query->row();
        }

        function get_tiendas_pedido($Ped_IdPedido)
        {
            $sql = "SELECT tip.Tie_IdTienda, tip.Tip_Estatus, cot.Cot_IdCotizacion
                    FROM tienda_pedido as tip
                    LEFT JOIN cotizacion cot ON cot.Tip_IdTiendaPedido = tip.Tip_IdTiendaPedido 
                    WHERE tip.Ped_IdPedido = $Ped_IdPedido";

            $query = $this->db->query($sql);
            return $query->result();
        }

        function get_detalle_pedido($Ped_IdPedido)
        {
            $sql = "SELECT pde.pedido_Ped_IdPedido, pro.Pro_Nombre, unm.Unm_Nombre, sku.SKU_IdSKU, pde.Ped_Cantidad, sku.SKU_Img, sku.SKU_Color, var.Var_Nombre, vao.Vao_Nombre, tie.Tie_Nombre, prt.Tie_IdTienda
            FROM pedido_detalle as pde
            INNER JOIN sku as sku ON sku.SKU_IdSku = pde.SKU_IdSKU
            INNER JOIN sku_pvo as skp ON skp.sku_SKU_IdSKU = sku.SKU_IdSKU
            INNER JOIN producto as pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto
            INNER JOIN producto_unidad as pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto
            INNER JOIN producto_variacion as prv ON skp.producto_variacion_Prv_IdProductoVariacion = prv.Prv_IdProductoVariacion
            INNER JOIN producto_variacion_opcion as pvo ON skp.producto_variacion_opcion_PVO_IdPVO = pvo.PVO_IdPVO
            INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad
            INNER JOIN variacion AS var ON var.Var_IdVariacion = prv.Prv_IdVariacion
            INNER JOIN variacion_opcion AS vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao
            INNER JOIN producto_tienda AS prt ON prt.Pro_IdProducto = pro.Pro_IdProducto
            INNER JOIN tienda AS tie ON tie.Tie_IdTienda = prt.Tie_IdTienda
            WHERE pde.pedido_Ped_IdPedido = $Ped_IdPedido
            ORDER BY prt.Tie_IdTienda";
            $query = $this->db->query($sql);
            return $query->result();
        }

        function get_pedidos_usuario_detalle($Usu_IdUsuario)
        {
            $sql = "SELECT ped.Ped_IdPedido, pro.Pro_Nombre, unm.Unm_Nombre, ped.Ped_Datos, ped.Ped_Correo,ped.Ped_Direccion, ped.Ped_Telefono, ped.Ped_Estatus, ped.Ped_FechaCreacion, sku.SKU_IdSKU, pde.Ped_Cantidad, sku.SKU_Img, sku.SKU_Color, var.Var_Nombre, vao.Vao_Nombre, tie.Tie_Nombre
            FROM pedido as ped
            INNER JOIN pedido_detalle as pde ON pde.Pedido_Ped_IdPedido = ped.Ped_IdPedido 
            INNER JOIN sku as sku ON sku.SKU_IdSku = pde.SKU_IdSKU
            INNER JOIN sku_pvo as skp ON skp.sku_SKU_IdSKU = sku.SKU_IdSKU
            INNER JOIN producto as pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto 
            INNER JOIN producto_unidad as pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto
            INNER JOIN producto_variacion as prv ON skp.producto_variacion_Prv_IdProductoVariacion = prv.Prv_IdProductoVariacion
            INNER JOIN producto_variacion_opcion as pvo ON skp.producto_variacion_opcion_PVO_IdPVO = pvo.PVO_IdPVO  
            INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad 
            INNER JOIN variacion AS var ON var.Var_IdVariacion = prv.Prv_IdVariacion
            INNER JOIN variacion_opcion AS vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao 
            INNER JOIN producto_tienda AS prt ON prt.Pro_IdProducto = pro.Pro_IdProducto 
            INNER JOIN tienda AS tie ON tie.Tie_IdTienda = prt.Tie_IdTienda 
            WHERE ped.Usu_IdUsuario = $Usu_IdUsuario ORDER BY prt.Tie_IdTienda";
            $query = $this->db->query($sql);
            return $query->result();
        }

        function get_datos_correo($Tie_IdTienda)
        {
            $sql = "SELECT usu.`Usu_Correo`, per.Per_Nombre, tie.Tie_Subdominio
                    FROM usuario AS usu
                    INNER JOIN tienda AS tie ON tie.`Tie_IdTienda` = usu.`Tie_IdTienda`
                    INNER JOIN persona AS per ON per.`Per_IdPersona` = usu.`Per_IdPersona`
                    WHERE usu.`Rol_IdRol` = 2 AND usu.Tie_IdTienda = $Tie_IdTienda";

            $query = $this->db->query($sql);

            return $query->row();
        }

        function insertCotizacion($arrPedido, $arrPrecio, $arrPrecioDscto)
        {
            $this->db->trans_start();
            $this->db->set('Cot_Estado', 1);
            $this->db->set('pedido_Ped_IdPedido', $arrPedido[0]->Ped_IdPedido);
            $this->db->set('Tip_IdTiendaPedido', $arrPedido[0]->Tip_IdTiendaPedido);

            if ($this->db->insert('cotizacion'))
            {
                $Cot_IdCotizacion = $this->db->insert_id();
            };

            $this->db->set('Tip_Estatus', 2);
            $this->db->where('Tip_IdTiendaPedido', $arrPedido[0]->Tip_IdTiendaPedido);
            $this->db->update('tienda_pedido');

            $count = 0;

            foreach ($arrPedido as $arp) 
            {
                $this->db->set('cotizacion_Cot_IdCotizacion',$Cot_IdCotizacion);
                $this->db->set('SKU_IdSKU',$arp->SKU_IdSKU);
                $this->db->set('Precio_Cotizacion',$arrPrecio[$count]);
                $this->db->set('PrecioDscto_Cotizacion',$arrPrecioDscto[$count]);
                $this->db->set('Cantidad_Cotizacion',$arp->Ped_Cantidad);
                $this->db->insert('cotizacion_detalle');  
                $count++;
            }
            
            $this->db->trans_complete();

            if ($this->db->trans_status() == TRUE) 
            {
                return true;
            }

            return false;
        }    

        function updateCotizacion($arrPedido, $arrPrecio, $arrPrecioDscto, $idCotizacion)
        {
            $this->db->trans_start();

            $this->db->set('Cot_Estado', 1);
            $this->db->set('Cot_FechaModificacion', date("Y-m-d H:i:s"));
            $this->db->set('pedido_Ped_IdPedido', $arrPedido[0]->Ped_IdPedido);
            $this->db->where('Cot_IdCotizacion', $idCotizacion);
            $this->db->update('cotizacion');

            $count = 0;

            foreach ($arrPedido as $arp) 
            {
                $this->db->where('cotizacion_Cot_IdCotizacion', $idCotizacion);
                $this->db->delete('cotizacion_detalle');
                $count++;
            }

            $count = 0;

            foreach ($arrPedido as $arp) 
            {
                $this->db->set('cotizacion_Cot_IdCotizacion',$idCotizacion);
                $this->db->set('SKU_IdSKU',$arp->SKU_IdSKU);
                $this->db->set('Precio_Cotizacion',$arrPrecio[$count]);
                $this->db->set('PrecioDscto_Cotizacion',$arrPrecioDscto[$count]);
                $this->db->set('Cantidad_Cotizacion',$arp->Ped_Cantidad);
                $this->db->where('cotizacion_Cot_IdCotizacion',$idCotizacion);
                $this->db->replace('cotizacion_detalle');
                $count++;
            }
            
            $this->db->trans_complete();

            return $this->db->trans_status();
        }

        function get_cotizacion_usuario($Usu_IdUsuario)
        {
            $this->db->select('*');
            $this->db->from('cotizacion as cot');
            $this->db->join('pedido AS ped', 'ped ON ped.Ped_IdPedido = cot.pedido_Ped_IdPedido'); 
            $this->db->where('ped.Usu_IdUsuario',$Usu_IdUsuario);
        
            $query = $this->db->get();
            return $query->result();
        }

        function get_estado_cotizacion_venta($Cot_IdCotizacion)
        {
            $this->db->select('cot.Cot_Estado, ven.Ven_IdVenta');
            $this->db->from('cotizacion cot');
            $this->db->join('venta AS ven', 'ven.Cot_IdCotizacion = cot.Cot_IdCotizacion'); 
            $this->db->where('cot.Cot_IdCotizacion', $Cot_IdCotizacion);
        
            $query = $this->db->get();
            return $query->row();
        }     

        function cotizacionRespondido($idPedido)
        {
            $this->db->select('*');
            $this->db->from('cotizacion as cot');
            $this->db->join('cotizacion_detalle as cod','cod ON cod.cotizacion_Cot_IdCotizacion = cot.Cot_IdCotizacion');
            $this->db->where('pedido_Ped_IdPedido',$idPedido);       
            // $this->db->limit(1);
            $this->db->order_by("Precio_Cotizacion", "DESC");
            $query = $this->db->get();
            return $query->result();
        }
    
        function get_dcotizacion($Cot_IdCotizacion)
        {
            $this->db->select('pro.Pro_Nombre, sku.SKU_Img, cde.Precio_Cotizacion, cde.Cantidad_Cotizacion, unm.Unm_Nombre, var.Var_Nombre, vao.Vao_Nombre, cde.PrecioDscto_Cotizacion, cde.SKU_IdSKU, tie.Tie_PorcentajeCotizacion, cot.Cot_Estado');
            $this->db->from('cotizacion as cot');
            $this->db->join('cotizacion_detalle AS cde', 'cde ON cde.cotizacion_Cot_IdCotizacion = cot.Cot_IdCotizacion'); 
            $this->db->join('sku AS sku', 'sku ON sku.SKU_IdSKU = cde.SKU_IdSKU');
            $this->db->join('sku_pvo AS spv', 'spv ON spv.SKU_SKU_IdSKU = sku.SKU_IdSKU');
            $this->db->join('producto_variacion AS prv', 'prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion');
            $this->db->join('producto_variacion_opcion AS pvo', 'pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO');
            $this->db->join('variacion AS var', 'var ON var.Var_IdVariacion = prv.Prv_IdVariacion');
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao');
            $this->db->join('producto AS pro', 'pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto');
            $this->db->join('producto_tienda AS prt', 'prt ON prt.Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('tienda AS tie', 'tie ON tie.Tie_IdTienda = prt.Tie_IdTienda');
            $this->db->join('producto_unidad AS pru', 'pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('unidadmedida AS unm', 'unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad');
            $this->db->where('cot.Cot_IdCotizacion',$Cot_IdCotizacion);
            
            $query = $this->db->get();

            return $query->result();
        }

        function get_comentarios_usuario($Usu_IdUsuario)
        {
            $this->db->select('*'); 
            $this->db->from('comentario as com');
            $this->db->join('respuesta as res','res ON res.Com_IdComentario = com.Com_IdComentario','left');
            $this->db->join('producto as pro','pro ON pro.Pro_IdProducto = com.Pro_IdProducto');
            $this->db->join('producto_foto as prf','prf ON prf.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->where('Usu_IdUsuario',$Usu_IdUsuario);
            $this->db->group_by('com.Com_IdComentario');
            $this->db->order_by('com.Com_FechaCreacion', 'DESC');
            $query = $this->db->get();
            return $query->result();
        }  
        function get_comentarios_usuario_img($Usu_IdUsuario)
        {
            $this->db->select('*'); 
            $this->db->from('comentario as com');
            $this->db->join('respuesta as res','res ON res.Com_IdComentario = com.Com_IdComentario','left');
            $this->db->join('producto as pro','pro ON pro.Pro_IdProducto = com.Pro_IdProducto');
            $this->db->join('producto_foto as prf','prf ON prf.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->where('Usu_IdUsuario',$Usu_IdUsuario);
            $this->db->order_by('com.Com_FechaCreacion', 'DESC');
            $query = $this->db->get();
            return $query->result();
        }  

        function get_usuario_porIdTienda($tienda_Tie_IdTienda)
        {
            $this->db->select('Usu_Correo');
            $this->db->from('usuario');
            $this->db->where('Tie_IdTienda', $tienda_Tie_IdTienda);
            $query = $this->db->get();
            return $query->row();
        }

        function registrar_tienda_pedido($Tie_IdTienda, $Ped_IdPedido)
        {
            $this->db->set('Tie_IdTienda', $Tie_IdTienda);
            $this->db->set('Ped_IdPedido', $Ped_IdPedido);
            $this->db->insert('tienda_pedido');
        }

        function registrar_pedido_detalle($Ped_IdPedido, $SKU_IdSKU, $Ped_Cantidad)
        {
            $this->db->set('pedido_Ped_IdPedido', $Ped_IdPedido);
            $this->db->set('SKU_IdSKU', $SKU_IdSKU);
            $this->db->set('Ped_Cantidad', $Ped_Cantidad);
            $this->db->insert('pedido_detalle');
        }
    }
?>