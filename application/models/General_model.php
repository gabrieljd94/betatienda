<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class General_model extends CI_Model
    {

        function __construct()
        {           
            parent::__construct();
        }        


        function deleteRow($nombre_tabla,$id,$nombre_variable){

            $this->db->trans_start();
            
            $this->db->where($nombre_variable, $id);
            $this->db->delete($nombre_tabla);

            $this->db->trans_complete();

            if ($this->db->trans_status() == TRUE) {
               return true;
            }

            return false;

        }
      
    }
?>