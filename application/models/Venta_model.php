<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Venta_model extends CI_Model
    {
        private $nombre_tabla = 'venta';

        function __construct()
        {
            parent::__construct();
        }

        function get_venta($Ven_IdVenta)
        {
            $this->db->select('ven.Ven_IdVenta, ven.Ven_Estado, ven.Ven_FechaRegistro, ven.Usu_IdUsuario, ven.Tie_IdTienda');
            $this->db->from('venta as ven');
            $this->db->where('ven.Ven_IdVenta',$Ven_IdVenta);
            $query = $this->db->get();

            return $query->row();
        }

        function get_ventas($Usu_IdUsuario)
        {
            $this->db->select('Ven_IdVenta, Ven_Estado, Ven_FechaRegistro');
            $this->db->from('venta');
            $this->db->where('Usu_IdUsuario',$Usu_IdUsuario);
            $query = $this->db->get();

            return $query->result();
        }

         function get_calificacion($Ven_IdVenta)
        {
            $this->db->select('*');
            $this->db->from('califica_compra');            
            $this->db->where('Ven_IdVenta',$Ven_IdVenta);         
            $query = $this->db->get();

            return $query->row();
        }

          function get_venta_info($Ven_IdVenta)
        {
            $this->db->select('vef.Vef_DirEnvio, vef.Vef_Banco, vef.Vef_RUC, vef.Vef_RazonSocial');
            $this->db->from('venta_info as vef');
            $this->db->where('vef.Vef_Ven_IdVenta',$Ven_IdVenta);
            $query = $this->db->get();

            return $query->row();
        }
        
        
        function get_ventas_x_tienda($Tie_IdTienda)
        {
            $this->db->select('Ven_IdVenta, Ven_Estado, Ven_FechaRegistro, per.Per_Nombre, usu.Usu_IdUsuario');
            $this->db->from('venta AS ven');
            $this->db->join('usuario AS usu', 'usu.Usu_IdUsuario = ven.Usu_IdUsuario');
            $this->db->join('persona AS per', 'per.Per_IdPersona = usu.Per_IdPersona');
            $this->db->join('cotizacion AS cot', 'cot.Cot_IdCotizacion = ven.Cot_IdCotizacion');
            $this->db->join('tienda_pedido AS tip', 'tip.Tip_IdTiendaPedido = cot.Tip_IdTiendaPedido');
            $this->db->where('tip.Tie_IdTienda', $Tie_IdTienda);
            $query = $this->db->get();

            return $query->result();
        }

        function insert_calificacion($Cal_Compra,$Cal_Tienda,$Usu_IdUsuario,$Tie_IdTienda,$Ven_IdVenta,$Cal_Comentario)
        {
            $this->db->set('Cal_Compra', $Cal_Compra);
            $this->db->set('Cal_Tienda', $Cal_Tienda);
            $this->db->set('Usu_IdUsuario', $Usu_IdUsuario);
            $this->db->set('Tie_IdTienda', $Tie_IdTienda);
            $this->db->set('Ven_IdVenta', $Ven_IdVenta);
            $this->db->set('Cal_Comentario', $Cal_Comentario);

            $this->db->insert('califica_compra');

            $this->db->set('Ven_Estado', 3);
            $this->db->where('Ven_IdVenta', $Ven_IdVenta);
            $this->db->update('venta');

            return $Ven_IdVenta;

        }

        
        function registrar_venta($array_compra, $Ven_Total, $Cot_IdCotizacion, $Usu_IdUsuario)
        {
            $this->db->trans_start();

            //registro venta
            $this->db->set('Cot_IdCotizacion', $Cot_IdCotizacion);
            $this->db->set('Usu_IdUsuario', $Usu_IdUsuario);
            $this->db->set('Ven_Total', $Ven_Total);
            
            if ($this->db->insert($this->nombre_tabla))
            {
                $Ven_IdVenta = $this->db->insert_id();
            };

            //registro detalle venta
            foreach ($array_compra as $item) 
            {
                $this->db->set('Ven_IdVenta', $Ven_IdVenta);
                $this->db->set('SKU_IdSKU', $item[0]);
                $this->db->set('Ved_Precio', $item[1]);
                $this->db->set('Ved_Cantidad', $item[2]);

                $this->db->insert('venta_detalle');
            }

            //actualizar estado cotizacion
            $this->db->set('Cot_Estado', 2);
            $this->db->where('Cot_IdCotizacion', $Cot_IdCotizacion);
            $this->db->update('cotizacion');

            $this->db->trans_complete();

            if ($this->db->trans_status()) 
            {
               return $Ven_IdVenta;
            }

            return null;
        }

        function registrar_venta_acuerdo($array_compra, $Ven_Total, $Cot_IdCotizacion, $Usu_IdUsuario, $Tie_IdTienda)
        {
            $this->db->trans_start();

            //registro venta
            $this->db->set('Cot_IdCotizacion', $Cot_IdCotizacion);
            $this->db->set('Tie_IdTienda', $Tie_IdTienda);
            $this->db->set('Usu_IdUsuario', $Usu_IdUsuario);
            $this->db->set('Ven_Total', $Ven_Total);

            if ($this->db->insert($this->nombre_tabla))
            {
                $Ven_IdVenta = $this->db->insert_id();
                $CodCompra = 0;
                $this->db->set('Ven_CodCompra', $CodCompra);
                $this->db->update('venta');
            };

            //registro detalle venta
            foreach ($array_compra as $item) 
            {
                $this->db->set('Ven_IdVenta', $Ven_IdVenta);
                $this->db->set('SKU_IdSKU', $item[0]);
                $this->db->set('Ved_Precio', $item[1]);
                $this->db->set('Ved_Cantidad', $item[2]);

                $this->db->insert('venta_detalle');
            }


            //actualizar estado cotizacion
            $this->db->set('Cot_Estado', 2);
            $this->db->where('Cot_IdCotizacion', $Cot_IdCotizacion);
            $this->db->update('cotizacion');

            $this->db->trans_complete();

            if ($this->db->trans_status()) 
            {
               return $Ven_IdVenta;
            }

            return null;
        }
    }
?>