<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Producto_model extends CI_Model
    {
        private $nombre_tabla = 'producto';
        function __construct()
        { 
            parent::__construct();
        }    
        function productos($where="", $limit="")
        {
            $sql = "SELECT pro.Pro_IdProducto, pro.Pro_Descripcion, pro.Pro_Nombre, pro.Pro_Estado, pro.Pro_Oferta, pro.Pro_Valoracion, pro.Pro_PrecioRango, pro.Pro_MostrarPrecio, unm.Unm_Nombre, pun.Pru_Valor, prt.Tie_IdTienda
                FROM `producto` as pro 
                INNER JOIN producto_unidad AS pun ON pun.producto_Pro_IdProducto = pro.Pro_IdProducto 
                INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pun.Unm_IdUnidadMedidad 
                INNER JOIN producto_tienda AS prt ON prt.Pro_IdProducto = pro.Pro_IdProducto $where";
            $query = $this->db->query($sql);   
            return $query->result();
        }

        function productos2($where="", $limit="")
        {
            $sql = "SELECT pro.Pro_IdProducto, pro.Pro_Descripcion, pro.Pro_Nombre, pro.Pro_Estado, pro.Pro_Oferta, pro.Pro_Valoracion, pro.Pro_PrecioRango, pro.Pro_MostrarPrecio, unm.Unm_Nombre, pun.Pru_Valor, prf.Prf_Img
                FROM `producto` as pro 
                INNER JOIN producto_unidad AS pun ON pun.producto_Pro_IdProducto = pro.Pro_IdProducto 
                INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pun.Unm_IdUnidadMedidad 
                INNER JOIN producto_tienda AS prt ON prt.Pro_IdProducto = pro.Pro_IdProducto 
                INNER JOIN producto_foto AS prf ON prf.producto_Pro_IdProducto = pro.Pro_IdProducto
                $where";
            $query = $this->db->query($sql); 

            return $query->result();
        } 
 
        function get_fotos_producto($where="", $limit="")
        {
            $this->db->select('*');
            $this->db->from('producto_foto');
            if (empty($where)) 
            {                
             $this->db->group_by('producto_Pro_IdProducto');
            }else{
             $this->db->where('producto_Pro_IdProducto',$where);
            }
            $query = $this->db->get();
            return $query->result();
        } 
        function productos_filtro($idVariacion, $Tie_IdTienda)
        {         
            $sql = "SELECT pro.Pro_IdProducto,pro.Pro_Nombre, pro.Pro_Estado, pro.Pro_Oferta, pro.Pro_Valoracion, pro.Pro_PrecioRango, pro.Pro_MostrarPrecio, unm.Unm_Nombre FROM `producto` as pro 
            INNER JOIN producto_unidad AS pun ON pun.producto_Pro_IdProducto = pro.Pro_IdProducto 
            INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pun.Unm_IdUnidadMedidad
            INNER JOIN producto_categoria AS pca ON pca.producto_Pro_IdProducto = pro.Pro_IdProducto
            INNER JOIN producto_tienda as prt ON prt.Pro_IdProducto = pro.Pro_IdProducto
            INNER JOIN detalle_subcategoria AS dsu ON pca.Des_IdDetalle_SubCategoria = dsu.Des_IdDetalle_SubCategoria
            WHERE pca.Des_IdDetalle_SubCategoria = $idVariacion and prt.Tie_IdTienda = $Tie_IdTienda group by pro.Pro_IdProducto";
            $query = $this->db->query($sql);   
            return $query->result();
        } 
        function filtro_producto_nombre($nombre_producto, $Tie_IdTienda)
        {
            $sql = "SELECT pro.Pro_IdProducto,pro.Pro_Nombre, pro.Pro_Estado, pro.Pro_Oferta, pro.Pro_Valoracion, pro.Pro_PrecioRango, pro.Pro_MostrarPrecio, unm.Unm_Nombre FROM producto as pro INNER JOIN producto_unidad AS pun ON pun.producto_Pro_IdProducto = pro.Pro_IdProducto INNER JOIN unidadmedida AS unm ON unm.Unm_IdUnidadMedida = pun.Unm_IdUnidadMedidad INNER JOIN producto_variacion as prv ON prv.producto_Pro_IdProducto = pro.Pro_IdProducto INNER JOIN producto_variacion_opcion as pvo ON pvo.producto_variacion_Prv_IdProductoVariacion = prv.Prv_IdProductoVariacion INNER JOIN producto_tienda as prt ON prt.Pro_IdProducto = pro.Pro_IdProducto WHERE pro.Pro_Nombre LIKE '%$nombre_producto%' and prt.Tie_IdTienda = $Tie_IdTienda group by pro.Pro_IdProducto";
            $query = $this->db->query($sql);   
            return $query->result();
        }        
        function get_Categoria()
        {
            $this->db->select('*');
            $this->db->from('categoria'); 
            $query = $this->db->get();
            return $query->result();
        }
        function get_cat_sub($nombreProducto)
        {

            $this->db->select('*');
            $this->db->from('subcategoria');
            $this->db->like('Suc_Nombre',$nombreProducto[0]);
        
            $query = $this->db->get();
            return $query->row();
        }
        function get_UnidadMedida()
        {
            $this->db->select('*');
            $this->db->from('unidadmedida');
            $query = $this->db->get();
            return $query->result();
        }
        function get_subCategoria($Cat_IdCategoria)
        {
            $this->db->select('*');
            $this->db->from('subcategoria');
            $this->db->where('Categoria_Cat_IdCategoria',$Cat_IdCategoria);
            $query = $this->db->get();
            return $query->result();
        }        
        function get_Categorias_defaul()
        {
            $this->db->select('*');
            $this->db->from('subcategoria');
            $query = $this->db->get();
            return $query->result();
        }
        function get_p_c_d_s($producto_Pro_IdProducto)
        {
            $this->db->select('Des_IdDetalle_SubCategoria');
            $this->db->from('producto_categoria');
            $this->db->where('producto_Pro_IdProducto',$producto_Pro_IdProducto);
            $query = $this->db->get();
            return $query->row();
        }
        function get_subCategoria_detalle($subcategoria_Suc_IdSubCategoria)
        {
            $this->db->select('*');
            $this->db->from('detalle_subcategoria');
            $this->db->where('subcategoria_Suc_IdSubCategoria',$subcategoria_Suc_IdSubCategoria);  
            $query = $this->db->get();
            return $query->result();
        }
        function get_var_cat($Des_IdDetalle_SubCategoria,$Tie_IdTienda)
        { 
            $sql = "SELECT vao.Vao_IdVaricion_Opcion, vao.Vao_Nombre, vao.variacion_Var_IdVariacion 
            FROM variacion_subcategoria as vas 
            INNER JOIN variacion_opcion as vao ON vao.Vao_IdVaricion_Opcion = vas.Vao_IdVaricion_Opcion 
            INNER JOIN detalle_subcategoria as des ON des.Des_IdDetalle_SubCategoria = vas.Des_IdDetalle_SubCategoria
            INNER JOIN variacion as var ON var.Var_IdVariacion = vao.variacion_Var_IdVariacion 
            WHERE vas.Des_IdDetalle_SubCategoria = $Des_IdDetalle_SubCategoria or var.Tie_IdTienda = $Tie_IdTienda";    
            $query = $this->db->query($sql); 
            return $query->result();
        }
        function get_variacion_sub($Des_IdDetalle_SubCategoria)
        {
            $sql = "SELECT var.Var_IdVariacion, var.Var_Nombre FROM variacion_subcategoria AS vas 
            INNER JOIN variacion_opcion AS vao ON vao.Vao_IdVaricion_Opcion = vas.Vao_IdVaricion_Opcion 
            INNER JOIN detalle_subcategoria AS des ON des.Des_IdDetalle_SubCategoria = vas.Des_IdDetalle_SubCategoria
            INNER JOIN variacion AS var ON var.Var_IdVariacion = vao.variacion_Var_IdVariacion
            WHERE vas.Des_IdDetalle_SubCategoria = $Des_IdDetalle_SubCategoria group by var.Var_IdVariacion";
            $query = $this->db->query($sql);   
            return $query->result();
        }
        function get_variaciones_filtro($Tie_IdTienda)
        {
            $sql = "SELECT vao.Vao_Nombre, vao.Vao_IdVaricion_Opcion, var.Var_Nombre FROM producto as pro INNER JOIN producto_variacion as prv ON prv.producto_Pro_IdProducto = pro.Pro_IdProducto INNER JOIN producto_variacion_opcion as pvo ON pvo.producto_variacion_Prv_IdProductoVariacion = prv.Prv_IdProductoVariacion INNER JOIN variacion as var ON var.Var_IdVariacion = prv.Prv_IdVariacion INNER JOIN variacion_opcion as vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao INNER JOIN producto_tienda as prt ON prt.Pro_IdProducto = pro.Pro_IdProducto WHERE prt.Tie_IdTienda = $Tie_IdTienda group by vao.Vao_IdVaricion_Opcion";
            $query = $this->db->query($sql);
            return $query->result();
        }        
        function get_filtro_tienda($Tie_IdTienda)
        {
            $this->db->select('*');           
            $this->db->from('producto_categoria as pca');      
            $this->db->join('detalle_subcategoria AS dsu', 'dsu.Des_IdDetalle_SubCategoria = pca.Des_IdDetalle_SubCategoria');
            $this->db->join('producto_tienda AS prt', 'prt.Pro_IdProducto = pca.producto_Pro_IdProducto');
            $this->db->where('prt.Tie_IdTienda',$Tie_IdTienda);
            $this->db->group_by('dsu.Des_IdDetalle_SubCategoria');
            $query = $this->db->get();
            return $query->result();
        }
        function get_sku($where="",$limit="",$limit2="")
        {
            $this->db->select('sku.SKU_IdSku, sku.SKU_Nombre, sku.SKU_Img, sku.Producto_Pro_IdProducto, sku.SKU_Cantidad, sku.SKU_Precio, var.Var_Nombre, vao.Vao_Nombre, sku.SKU_Color');
            $this->db->from('sku as sku');
            $this->db->join('sku_pvo AS spv', 'spv.SKU_SKU_IdSKU = sku.SKU_IdSKU');
            $this->db->join('producto_variacion AS prv', 'prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion');
            $this->db->join('producto_variacion_opcion AS pvo', 'pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO');  
            $this->db->join('variacion AS var', 'var ON var.Var_IdVariacion = prv.Prv_IdVariacion'); 
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao');
            $this->db->limit($limit, $limit2);
            if (empty($where)) 
            {  
            }else{
            $this->db->where('sku.producto_Pro_IdProducto', $where);
            }
            $query = $this->db->get();
            return $query->result();
        }
        function get_sku3($limit="",$limit2="",$Tie_IdTienda)
        {
            $this->db->select('sku.SKU_IdSku, sku.SKU_Nombre, sku.SKU_Img, sku.Producto_Pro_IdProducto, sku.SKU_Cantidad, sku.SKU_Precio, var.Var_Nombre, vao.Vao_Nombre, sku.SKU_Color'); 
            $this->db->from('sku as sku');
            $this->db->join('sku_pvo AS spv', 'spv.SKU_SKU_IdSKU = sku.SKU_IdSKU');
            $this->db->join('producto_variacion AS prv', 'prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion');
            $this->db->join('producto_variacion_opcion AS pvo', 'pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO');  
            $this->db->join('variacion AS var', 'var ON var.Var_IdVariacion = prv.Prv_IdVariacion'); 
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao');
            $this->db->join('producto AS pro', 'pro ON pro.Pro_IdProducto = sku.producto_Pro_IdProducto');
            $this->db->join('producto_tienda AS prt', 'prt ON prt.Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->where('prt.Tie_IdTienda', $Tie_IdTienda);
            $this->db->limit($limit2, $limit);
            $query = $this->db->get();
            return $query->result();
        }

        function get_sku_cantidad($Tie_IdTienda)
        {
            $sql = "SELECT COUNT(SKU_IdSKU) AS Cantidad FROM sku INNER JOIN producto as pro ON pro.Pro_IdProducto = sku.producto_Pro_IdProducto INNER JOIN producto_tienda as prt ON prt.Pro_IdProducto = pro.Pro_IdProducto where prt.Tie_IdTienda = $Tie_IdTienda";
            $query = $this->db->query($sql);  
            return $query->row();
        }
        
        function get_producto_cantidad($Tie_IdTienda)
        {
            $sql = "SELECT COUNT(pro.Pro_IdProducto) AS Cantidad FROM producto as pro INNER JOIN producto_tienda as prt ON prt.Pro_IdProducto = pro.Pro_IdProducto where prt.Tie_IdTienda = $Tie_IdTienda";
            $query = $this->db->query($sql);  
            return $query->row();
        }

        function get_sku2($where="")
        {
            $this->db->select('sku.SKU_IdSku, sku.SKU_Nombre, sku.SKU_Img, sku.Producto_Pro_IdProducto, sku.SKU_Cantidad, sku.SKU_Precio, var.Var_Nombre, vao.Vao_Nombre,sku.SKU_Color'); 
            $this->db->from('sku as sku');
            $this->db->join('sku_pvo AS spv', 'spv.SKU_SKU_IdSKU = sku.SKU_IdSKU');
            $this->db->join('producto_variacion AS prv', 'prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion');
            $this->db->join('producto_variacion_opcion AS pvo', 'pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO');  
            $this->db->join('variacion AS var', 'var ON var.Var_IdVariacion = prv.Prv_IdVariacion');
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao');
            if (empty($where)) 
            {   
            }else{
            $this->db->where('sku.SKU_IdSKU', $where);
            }
            $query = $this->db->get();
            return $query->row();
        }
        function get_variacion()
        {
            $this->db->select('*'); 
            $this->db->from('variacion');
            $query = $this->db->get();
            return $query->result();
        }          
        function addComentario($comentario,$Pro_IdProducto,$Usu_IdUsuario)
        {
            $this->db->set('Com_Comentario',$comentario); 
            $this->db->set('Pro_IdProducto',$Pro_IdProducto); 
            $this->db->set('Usu_IdUsuario',$Usu_IdUsuario); 
            $this->db->insert('comentario');           
        }

        function insertNuevaVariacion($Var_Nombre,$Array_Vao_Nombre,$Tie_IdTienda, $Des_IdDetalle_SubCategoria) 
        {
            $this->db->trans_start();
            
            $this->db->set('Var_Nombre', $Var_Nombre);
            $this->db->set('Tie_IdTienda', $Tie_IdTienda);
            if ($this->db->insert('variacion')) {              
              $Var_IdVariacion = $this->db->insert_id();
            }
            foreach ($Array_Vao_Nombre as $item)                    
                {
                    $this->db->set('Vao_Nombre', $item);
                    $this->db->set('variacion_Var_IdVariacion', $Var_IdVariacion);
                    if ($this->db->insert('variacion_opcion')) {              
                      $Vao_IdVaricion_Opcion = $this->db->insert_id();
                    }
                    $this->db->set('Vao_IdVaricion_Opcion', $Vao_IdVaricion_Opcion);
                    $this->db->set('Des_IdDetalle_SubCategoria', $Des_IdDetalle_SubCategoria);
                    $this->db->insert('variacion_subcategoria');

                }            

            $this->db->trans_complete();
            if ($this->db->trans_status() == TRUE) {
               return $Var_IdVariacion;
            }
            return false; 
        }

        function getVariacionNueva($Var_IdVariacion,$Tie_IdTienda){
            
            $this->db->select('var.Var_Nombre, Var_IdVariacion, vao.Vao_IdVaricion_Opcion, vao.Vao_Nombre');
            $this->db->from('variacion AS var'); 
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.variacion_Var_IdVariacion = var.Var_IdVariacion');
            $this->db->where('var.Var_IdVariacion', $Var_IdVariacion);
            $query = $this->db->get();
            return $query->result();

        }

        function responderComentario($Com_IdComentario, $Respuesta)
        {
            $this->db->set('Com_IdComentario',$Com_IdComentario); 
            $this->db->set('Res_Respuesta',$Respuesta); 
            $this->db->insert('respuesta');           
        }        
        function get_comentarios($Pro_IdProducto)
        {
            $this->db->select('*'); 
            $this->db->from('comentario');
            $this->db->where('Pro_IdProducto',$Pro_IdProducto);
            $this->db->order_by('Com_FechaCreacion', 'DESC');
            $query = $this->db->get();
            return $query->result();
        } 

        function ofertaProducto($Pro_IdProducto,$Pro_Oferta)
        {
            $this->db->set('Pro_Oferta',$Pro_Oferta);
            $this->db->where('Pro_IdProducto',$Pro_IdProducto);
            $this->db->update('producto');  
        }        

        function get_comentarios_tienda($Tie_IdTienda)
        {
            $this->db->select('*'); 
            $this->db->from('comentario as com');
            $this->db->join('producto as pro','pro ON pro.Pro_IdProducto = com.Pro_IdProducto');
            $this->db->join('producto_tienda as prt', 'prt ON prt.Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('producto_foto as prf','prf ON prf.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->where('prt.Tie_IdTienda',$Tie_IdTienda);
            $this->db->group_by('com.Com_IdComentario');
            $this->db->order_by('com.Com_FechaCreacion', 'DESC');
            $query = $this->db->get();
            return $query->result();
        }

        function get_respuestas_tienda($Tie_IdTienda)
        {
            $this->db->select('*'); 
            $this->db->from('respuesta as res');
            $this->db->join('comentario as com','com ON com.Com_IdComentario = res.Com_IdComentario','right');
            $this->db->join('producto as pro','pro ON pro.Pro_IdProducto = com.Pro_IdProducto');
            $this->db->join('producto_tienda as prt', 'prt ON prt.Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('producto_foto as prf','prf ON prf.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('usuario as usu','usu ON usu.Usu_IdUsuario = com.Usu_IdUsuario');
            $this->db->where('prt.Tie_IdTienda',$Tie_IdTienda);
            $this->db->group_by('com.Com_IdComentario');
            $this->db->order_by('com.Com_FechaCreacion', 'DESC');
            $query = $this->db->get();
            return $query->result();
        }

        function get_variacion_opcion()
        {
            $this->db->select('*');           
            $this->db->from('variacion_opcion');      
            $query = $this->db->get();
            return $query->result();
        }
        function addProductoTienda($Pro_IdProducto,$Tie_IdTienda)
        {
            $this->db->set('Pro_IdProducto', $Pro_IdProducto);
            $this->db->set('Tie_IdTienda', $Tie_IdTienda);
            $this->db->insert('producto_tienda');            
          
        }
        function addProducto($Pro_Nombre,$Pro_Descripcion,$Pro_PrecioRango)
        {
            $this->db->trans_start();
            
            $this->db->set('Pro_Nombre', $Pro_Nombre);
            $this->db->set('Pro_Descripcion', $Pro_Descripcion);
            $this->db->set('Pro_PrecioRango', $Pro_PrecioRango);
            if ($this->db->insert('producto')) {              
              $Pro_IdProducto = $this->db->insert_id();
            }    

            $this->db->trans_complete();
            if ($this->db->trans_status() == TRUE) {
               return $Pro_IdProducto;
            }
            return false;
        }
        function addProductoFotos($Pro_IdProducto,$Prf_Img)
        {
            $this->db->set('producto_Pro_IdProducto', $Pro_IdProducto);
            $this->db->set('Prf_Img', $Prf_Img);
            $this->db->insert('producto_foto');            
          
        }
        function addSkuFoto($SKU_IdSku,$SKU_Img)
        {
            $this->db->set('SKU_Img', $SKU_Img);
            $this->db->where('SKU_IdSKU', $SKU_IdSku);
            $this->db->update('sku');            
        }
        function skuEdit($SKU_IdSKU,$SKU_Cantidad)
        {
            $this->db->set('SKU_Cantidad', $SKU_Cantidad);
            $this->db->where('SKU_IdSKU', $SKU_IdSKU);
            $this->db->update('sku');            
        }
        function addProductoUnidad($Pro_IdProducto,$Unidad,$valorUnm)
        {
            $this->db->set('producto_Pro_IdProducto', $Pro_IdProducto);
            $this->db->set('Unm_IdUnidadMedidad', $Unidad);
            $this->db->set('Pru_Valor', $valorUnm);
            $this->db->insert('producto_unidad');            
          
        }
        function addProductoCategoria($Pro_IdProducto,$Cat_IdCategoria,$Suc_IdSubCategoria,$Des_IdDetalle_SubCategoria)
        {
            $this->db->set('producto_Pro_IdProducto', $Pro_IdProducto);
            $this->db->set('Cat_IdCategoria', $Cat_IdCategoria);
            $this->db->set('Suc_IdSubCategoria', $Suc_IdSubCategoria);
            $this->db->set('Des_IdDetalle_SubCategoria', $Des_IdDetalle_SubCategoria);
            $this->db->insert('producto_categoria');            
          
        }
        
        function var_producto($Pro_IdProducto){
            $this->db->select('Prv_IdVariacion');           
            $this->db->from('producto_variacion');           
            $this->db->where('producto_Pro_IdProducto',$Pro_IdProducto);      
            $query = $this->db->get();
            return $query->result();
        }

        function eliminarSku($SKU_IdSKU){

            $this->db->where('SKU_IdSKU', $SKU_IdSKU);
            $this->db->delete('sku'); 
            $this->db->where('sku_SKU_IdSKU', $SKU_IdSKU);
            $this->db->delete('sku_pvo');      
        
        }

        function get_comentarios_respuestas($Pro_IdProducto)
        {
            $this->db->select('*'); 
            $this->db->from('comentario as com');
            $this->db->join('respuesta as res','res ON res.Com_IdComentario = com.Com_IdComentario','left');
            $this->db->where('com.Pro_IdProducto',$Pro_IdProducto);
            $this->db->order_by('com.Com_FechaCreacion', 'DESC');
            $query = $this->db->get();
            return $query->result();
        }

        function eliminarPro($Pro_IdProducto){

            $this->db->where('Pro_IdProducto', $Pro_IdProducto);
            $this->db->delete('producto'); 
            $this->db->where('producto_Pro_IdProducto', $Pro_IdProducto);
            $this->db->delete('producto_categoria');
            $this->db->where('producto_Pro_IdProducto', $Pro_IdProducto);
            $this->db->delete('producto_foto'); 
            $this->db->where('Pro_IdProducto', $Pro_IdProducto);
            $this->db->delete('producto_tienda'); 
            $this->db->where('producto_Pro_IdProducto', $Pro_IdProducto);
            $this->db->delete('producto_unidad');
            $this->db->where('producto_Pro_IdProducto', $Pro_IdProducto);
            $this->db->delete('producto_variacion');
        }
        
        function addProductoVariacionVao($array_detalle,$Pro_IdProducto){
            
            $this->db->trans_start();
               
            if(count($array_detalle) > 0)
            {
                foreach ($array_detalle as $item)                    
                {
 
                    $Var_IdVariacion = $this->var_producto($Pro_IdProducto);
                    
                    if ($Var_IdVariacion != $item['varId']) {
                        $this->db->set('producto_Pro_IdProducto', $Pro_IdProducto);
                        $this->db->set('Prv_IdVariacion', (int)$item['varId']);                        
                        if ($this->db->insert('producto_variacion')) {              
                          $Prv_IdProductoVariacion = $this->db->insert_id();
                        } 
                    }
                        $this->db->set('producto_variacion_Prv_IdProductoVariacion', $Prv_IdProductoVariacion);
                        $this->db->set('PVO_IdVao', (int)$item['vaoId']); 
                        if ($this->db->insert('producto_variacion_opcion')) {              
                          $PVO_IdPVO = $this->db->insert_id();
                        } 
                        $this->db->set('producto_Pro_IdProducto',$Pro_IdProducto);
                        $this->db->set('SKU_Nombre', 'P' . $Pro_IdProducto . 'V'. $Prv_IdProductoVariacion);
                        $this->db->set('SKU_Cantidad',$item['cantidadVariacion']);
                        $this->db->set('SKU_Color',$item['colorVar']);
                        if ($this->db->insert('sku')) {              
                          $SKU_IdSKU = $this->db->insert_id();
                        } 
                        $this->db->set('producto_variacion_Prv_IdProductoVariacion',$Prv_IdProductoVariacion);
                        $this->db->set('producto_variacion_opcion_PVO_IdPVO', $PVO_IdPVO);
                        $this->db->set('sku_SKU_IdSKU', $SKU_IdSKU);
                        $this->db->insert('sku_pvo');
                }
 
            }
            $this->db->trans_complete();
            if($this->db->trans_status() == TRUE)
            {
               return true;
            }
            return false;
        }
    }
?>