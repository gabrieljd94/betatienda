<?php 
    if(!defined('BASEPATH')) exit('No direct script access allowed');
 
    class Venta_Detalle_model extends CI_Model
    {
        private $nombre_tabla = 'venta_detalle';

        function __construct()
        {
            parent::__construct();
        }

        function get_detalle_venta($Ven_IdVenta)
        {
            $this->db->select('pro.Pro_Nombre, sku.SKU_Img, ved.Ved_Precio, ved.Ved_Cantidad, unm.Unm_Nombre, var.Var_Nombre, vao.Vao_Nombre, ved.SKU_IdSKU, sku.SKU_Color');
            $this->db->from('venta_detalle as ved');
            $this->db->join('sku AS sku', 'sku ON sku.SKU_IdSKU = ved.SKU_IdSKU');
            $this->db->join('sku_pvo AS spv', 'spv ON spv.SKU_SKU_IdSKU = sku.SKU_IdSKU');
            $this->db->join('producto_variacion AS prv', 'prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion');
            $this->db->join('producto_variacion_opcion AS pvo', 'pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO');
            $this->db->join('variacion AS var', 'var ON var.Var_IdVariacion = prv.Prv_IdVariacion');
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao');
            $this->db->join('producto AS pro', 'pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto');            
            $this->db->join('producto_unidad AS pru', 'pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('unidadmedida AS unm', 'unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad');
            $this->db->where('ved.Ven_IdVenta', $Ven_IdVenta);

            $query = $this->db->get();

            return $query->result();
        }

        function get_detalle_venta_x_usuario($Usu_IdUsuario)
        {
            $this->db->select('pro.Pro_Nombre, sku.SKU_Img, ved.Ved_Precio, ved.Ved_Cantidad, unm.Unm_Nombre, var.Var_Nombre, vao.Vao_Nombre, ved.SKU_IdSKU, sku.SKU_Color, ved.Ven_IdVenta');
            $this->db->from('venta_detalle as ved');
            $this->db->join('venta AS ven', 'ven.Ven_IdVenta = ved.Ven_IdVenta');
            $this->db->join('sku AS sku', 'sku ON sku.SKU_IdSKU = ved.SKU_IdSKU');
            $this->db->join('sku_pvo AS spv', 'spv ON spv.SKU_SKU_IdSKU = sku.SKU_IdSKU');
            $this->db->join('producto_variacion AS prv', 'prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion');
            $this->db->join('producto_variacion_opcion AS pvo', 'pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO');
            $this->db->join('variacion AS var', 'var ON var.Var_IdVariacion = prv.Prv_IdVariacion');
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao');
            $this->db->join('producto AS pro', 'pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto');            
            $this->db->join('producto_unidad AS pru', 'pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('unidadmedida AS unm', 'unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad');
            $this->db->where('ven.Usu_IdUsuario', $Usu_IdUsuario);

            $query = $this->db->get();

            return $query->result();
        }

        function get_detalle_venta_x_tienda($Tie_IdTienda)
        {
            $this->db->select('pro.Pro_Nombre, sku.SKU_Img, ved.Ved_Precio, ved.Ved_Cantidad, unm.Unm_Nombre, var.Var_Nombre, vao.Vao_Nombre, ved.SKU_IdSKU, sku.SKU_Color, ved.Ven_IdVenta');
            $this->db->from('venta_detalle as ved');
            $this->db->join('venta AS ven', 'ven.Ven_IdVenta = ved.Ven_IdVenta');
            $this->db->join('cotizacion AS cot', 'cot.Cot_IdCotizacion = ven.Cot_IdCotizacion');
            $this->db->join('tienda_pedido AS tip', 'tip.Tip_IdTiendaPedido = cot.Tip_IdTiendaPedido');
            $this->db->join('sku AS sku', 'sku ON sku.SKU_IdSKU = ved.SKU_IdSKU');
            $this->db->join('sku_pvo AS spv', 'spv ON spv.SKU_SKU_IdSKU = sku.SKU_IdSKU');
            $this->db->join('producto_variacion AS prv', 'prv ON prv.Prv_IdProductoVariacion = spv.Producto_Variacion_Prv_IdProductoVariacion');
            $this->db->join('producto_variacion_opcion AS pvo', 'pvo ON pvo.PVO_IdPVO = spv.Producto_Variacion_Opcion_PVO_IdPVO');
            $this->db->join('variacion AS var', 'var ON var.Var_IdVariacion = prv.Prv_IdVariacion');
            $this->db->join('variacion_opcion AS vao', 'vao ON vao.Vao_IdVaricion_Opcion = pvo.PVO_IdVao');
            $this->db->join('producto AS pro', 'pro ON pro.Pro_IdProducto = sku.Producto_Pro_IdProducto');
            $this->db->join('producto_unidad AS pru', 'pru ON pru.producto_Pro_IdProducto = pro.Pro_IdProducto');
            $this->db->join('unidadmedida AS unm', 'unm ON unm.Unm_IdUnidadMedida = pru.Unm_IdUnidadMedidad');
            $this->db->where('tip.Tie_IdTienda', $Tie_IdTienda);

            $query = $this->db->get();

            return $query->result();
        }
    }
?>