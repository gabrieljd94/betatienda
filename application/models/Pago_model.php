<?php 
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
    class Pago_model extends CI_Model
    {
        private $nombre_tabla = 'pago';

        function __construct()
        {
            parent::__construct();
        }

        function get_pagos($condicion="")
        {
            $sql = "SELECT * FROM $this->nombre_tabla $condicion";
            $query = $this->db->query($sql);   
            return $query->result();
        }

        public function registrar_pago($Tie_IdTienda, $Usu_IdUsuario, $Pag_Descripcion, $Pag_Correo, $Pag_Monto, $Pag_Cuota)
        {
            $this->db->trans_start();

            $this->db->set('Tie_IdTienda', $Tie_IdTienda);
            $this->db->set('Usu_IdUsuario', $Usu_IdUsuario);
            $this->db->set('Pag_Descripcion', $Pag_Descripcion);
            $this->db->set('Pag_Correo', $Pag_Correo);
            $this->db->set('Pag_Monto', $Pag_Monto);
            $this->db->set('Pag_Cuota', $Pag_Cuota);
            
            $this->db->insert($this->nombre_tabla);

            $this->db->trans_complete();

            $this->db->set('Tie_Cuota', "`Tie_Cuota`+ $Pag_Cuota", FALSE);
            $this->db->where('Tie_IdTienda', $Tie_IdTienda);
            $this->db->update('tienda');



            if ($this->db->trans_status() == TRUE) {

               return true;

            }



            return false;

            
            try 
            {
                

                return true;
            }
            catch (Exception $e) 
            {
                return false;
            }
            
        }
    }
?>