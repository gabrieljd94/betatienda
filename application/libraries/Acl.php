<?php if ( ! defined('BASEPATH')) exit('Acceso denegado');

class Acl
{
    private $CI;

 	public function __construct()
 	{
 		$this->CI =& get_instance();
 	}

 	public function load_datos($categorias='no', $intranet='vendedor')
 	{
 		$data = array();

        $data['dominio'] = "pormayor.pe";

        if ($categorias == 'si')
        {
            $data['categorias'] = $this->CI->productoModel->get_Categoria();
            $data['subcategoria'] = $this->CI->productoModel->get_subCategoria2();
        }

        // $dominio = $_SERVER['SERVER_NAME'];

        // $array_dominio = explode("." , $dominio);
        // $subdominio = $array_dominio[0];

        //$data['ruta_img']="http://cocos.pe/img/";
        
        //------- localhost

        //VENDEDOR
        $data['usuario']['logueado'] = true;
        $data['usuario']['rol'] = 4;
        $data['usuario']['nombre'] = 'WWW';
        $data['usuario']['id_tienda'] = 1;
        $data['usuario']['id_usuario'] = 3;

        //COMPRADOR
        // $data['usuario']['id_usuario'] = 2;
        // $data['usuario']['rol'] = 4;
        // $data['usuario']['logueado'] = true;
        // $data['usuario']['id_tienda'] = null;
        // $data['usuario']['nombre'] = 'Gabriel';

        $id_tienda = 2;

        $data['tienda'] = $this->CI->tiendaModel->get_tienda_info($id_tienda);

        if(is_null($data['tienda']))
        {
            show_404();
        }
        else
        {
            $data['configuracion'] = $this->CI->configuracionModel->configuracion($id_tienda);

            if(is_null($data['configuracion']))
            {
                $data['configuracion'] = (object) [
                    'Con_ColorHexa' => '#607d8b',
                    'Con_ColorBtn' => '#4caf50'
                ];
            }

            $data['ruta_img']="http://localhost:8080/img/";
            
            //--------- fin de localhost

            // $data['tienda'] = $this->CI->tiendaModel->get_tienda_x_subdominio($subdominio);
            // $data['configuracion'] = $this->CI->configuracionModel->configuracion($data['tienda']->Tie_IdTienda);

            $redirect = true;

            // if($this->CI->session->userdata('logueado'))
            if(1)
            {
                $data['subdominio_tienda'] = $this->CI->tiendaModel->get_tienda($data['usuario']['id_tienda'])->Tie_Subdominio;
                $data['pedidos_pendientes'] = $this->CI->tiendaModel->get_cantidad_pedidos_pendientes($data['tienda']->Tie_IdTienda);
                // $data['usuario']['logueado'] = $this->CI->session->userdata('logueado');
                // $data['usuario']['rol'] = $this->CI->session->userdata('rol_usuario');
                // $data['usuario']['nombre'] = $this->CI->session->userdata('nombre_usuario');
                // $data['usuario']['id_tienda'] = $this->CI->session->userdata('tienda_usuario');
                // $data['usuario']['id_usuario'] = $this->CI->session->userdata('id_usuario');

                if($intranet)
                {
                    if($intranet == "vendedor")
                    {
                        if($data['usuario']['rol'] == 2 OR $data['usuario']['rol'] == 3)
                        {
                            if($data['tienda']->Tie_IdTienda == $data['usuario']['id_tienda'])
                            {
                                $redirect = false;
                            }
                        }                    
                    }
                    else if($intranet == "duenio")
                    {
                        if($data['usuario']['rol'] == 2)
                        {
                            if($data['tienda']->Tie_IdTienda == $data['usuario']['id_tienda'])
                            {
                                $redirect = false;
                            }
                        }                    
                    }
                    else if($intranet == "comprador")
                    {
                        if($data['tienda']->Tie_IdTienda != $data['usuario']['id_tienda'])
                        {
                            $redirect = false;
                        } 
                    }
                    else
                    {
                        $redirect = false;
                    }

                    $data['cantidad_pagos'] = $this->CI->tiendaModel->get_cantidad_pagos_x_tienda($data['tienda']->Tie_IdTienda)->Cantidad;
                }
                else
                {
                    $redirect  = false;
                }
            }
            else
            {
                if(!$intranet)
                {
                    $redirect = false;
                }
            }
            
            if($redirect)
            {
                //redirect("http://".$dominio);
                redirect();
            }

            return $data;   
        }
       
 	}
 }