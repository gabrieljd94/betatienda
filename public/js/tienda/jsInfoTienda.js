$(document).ready(function(){
 	function etiquetaEnter(textareaID) {
	    (function($) {        
	        if (!textareaID) var textareaID = "#descripcionDetalle";
	        var textarea = $(textareaID);
	        
	        textarea.val(
	            $(textarea).val().replace(/(\r\n|\n)/g, "<br/>")
	        );
	        
	    })(jQuery);        
	}

	$('.btnInsertDetalle').click(function(){
		etiquetaEnter($('#descripcionDetalle').val());
        var DetalleTienda = $('#descripcionDetalle').val();
		var Tie_IdTienda = $(this).data('id');
		$.ajax({
		 	data:{
			'Tie_IdTienda' : Tie_IdTienda,
			'DetalleTienda' : DetalleTienda
			},
		    type: "POST",
		    url: base_url+'usuario/tienda/insert_detalle_tienda', 
		    dataType: "json",
		    success: function(r){
				var files = $('.detalleImg')[0].files;		
		  		var error = '';
		  		var form_data = new FormData();
				  for(var count = 0; count<files.length; count++)
				  {
				   var name = files[count].name;
				   var extension = name.split('.').pop().toLowerCase();
				   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
				   {
				    error += "Imagen  " + count + " invalida";
				    Materialize.toast('Agrege una imagen correcta', 3000, 'rounded');
				   }
				   else
				   {
				    form_data.append("files[]", files[count]);
				   }
				  }
				  if(error == '')
				  {
				   $.ajax({
				    url: base_url + 'usuario/tienda/subirImagenTienda/'+r,
				    method:"POST",
				    data:form_data,
				    contentType:false,
				    cache:false,
				    processData:false,
				    beforeSend:function()
				    {				  		
				    	$("#loadTiendaCont").html('<div class="progress customColorA"><div class="indeterminate customColorB"></div></div>');

				    },
				    success:function(r)
				    {
				    	Materialize.toast('<i class="material-icons left ">check_circle</i>Datos guardados exitosamente', 1500, 'rounded successToast', function(){
				      		location.reload(true);
				      	});
				    }
				   })
				  }
				  else
				  {
				   alert(error);
				  }			
			 },
		    error: function(data) {
		      alert('error');
		    }
	  	});
	});

	$('.btnUpdateDetalle').click(function(){
        etiquetaEnter($('#descripcionDetalle').val());
        var DetalleTienda = $('#descripcionDetalle').val();

		var Tid_IdTiendaDetalle = $(this).data('id');
		var NoImg;
		$.ajax({
		 	data:{
			'Tid_IdTiendaDetalle' : Tid_IdTiendaDetalle,
			'DetalleTienda' : DetalleTienda
			},
		    type: "POST",
		    url: base_url+'usuario/tienda/update_detalle_tienda', 
		    dataType: "json",
		    success: function(r){
				var files = $('.detalleImg')[0].files;		
		  		var error = '';
		  		var form_data = new FormData();
				  for(var count = 0; count<files.length; count++)
				  {
				   var name = files[count].name;
				   var extension = name.split('.').pop().toLowerCase();
				   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
				   {
				    error += "Imagen  " + count + " invalida";
				    Materialize.toast('Agrege una imagen correcta', 3000, 'rounded');
				   }
				   else
				   {
				    form_data.append("files[]", files[count]);
				   }
				  }
				  if(error == '')
				  {
				   $.ajax({
				    url: base_url + 'usuario/tienda/subirImagenTienda/'+Tid_IdTiendaDetalle,
				    method:"POST",
				    data:form_data,
				    contentType:false,
				    cache:false,
				    processData:false,
				    beforeSend:function()
				    {				  		
				    	$("#loadTiendaCont").html('<div class="progress customColorA"><div class="indeterminate customColorB"></div></div>');
				    },
				    success:function(r)
				    {
				    	Materialize.toast('<i class="material-icons left ">check_circle</i>Datos guardados exitosamente', 1500, 'rounded successToast', function(){
				      		location.reload(true);
				      	});
				    }
				   })
				  }
				  else
				  {
				   alert(error);
				  }			
			 },
		    error: function(data) {
		      alert('error');
		    }
	  	});
	});

	$('.eliminarContenido').click(function(){
		var DtiendaId = $(this).data('idt');
		$.ajax({
		 	data:{
			'Tid_IdTiendaDetalle' : DtiendaId
			},
		    type: "POST",
		    url: base_url+'usuario/tienda/eliminar_detalle_tienda', 
		    dataType: "json",	
		    success:function(r)
		    {
		      location.reload(true);
		    }
			})	  
		});

	$('.btnAddLocal').click(function(){
		$('#modalLocal').modal();
		$('#modaladdLocal').load(base_url+'usuario/tienda/addLocal');
	});

	//Locales
	$('.btnEditLocal').click(function(){

		var Loc_IdLocal = $(this).data('id');

		$('#modalLocal2').modal();
		$('#modaleditLocal').load(base_url+'usuario/tienda/editLocal/'+Loc_IdLocal);
	});
	
	$('.btnDeleteLocal').click(function(){
		var IdLocal = $(this).data('id');

		$.confirm({
            icon: 'help',
            theme: 'modern',
            closeIcon: false,
            animation: 'scale',
            type: 'orange',
            title: 'Eliminar Local',
            columnClass: 'small',
            content: 'Esta acción luego no podrá revertirse',
            draggable: false,
            buttons: {
                Editar: {  
                	btnClass: 'btn',                  
                    action: function (){
                    $.ajax({
					 	data:{
						'IdLocal' : IdLocal
						},
					    type: "POST",
					    url: base_url+'usuario/tienda/deleteLocal', 
					    dataType: "json",
					    success: function(data){
					    	Materialize.toast('<i class="material-icons left ">check_circle</i>Local eliminado exitosamente', 1500, 'rounded successToast', function(){
  				      			location.reload(true);
    				      	});
						 },
					    error: function(data) {
					      alert('error');
					    }
					  });
                    }       
                },
                Cancelar: {
                        
                }
            }
        });
	});

	$('.dirLocal').focus(function(){
		Materialize.toast('Se lo mas <span class="dirEnfa">especifico</span> posible', 2000, 'rounded');
	});
	$('.dirLocal').blur(function(){
		Materialize.Toast.removeAll();
	});
	//Info tienda
	$('.saveDatosTienda').click(function(){
		var nTienda = $('#nombreTienda').val();
		Tie_IdTienda = $(this).data('idt');
		$.ajax({
		 	data:{
			'Tie_IdTienda' : Tie_IdTienda,
			'nTienda' : nTienda
			},
		    type: "POST",
		    url: base_url+'usuario/tienda/edit_nombre_tienda', 
		    dataType: "json",
		    success: function(data){
		      var files = $('#logoImg')[0].files;
			  console.log(files);
			  var error = '';
			  var form_data = new FormData();
			  for(var count = 0; count<files.length; count++)
			  {
			   var name = files[count].name;
			   var extension = name.split('.').pop().toLowerCase();
			   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
			   {
			    error += "Imagen  " + count + " invalida"
			   }
			   else
			   {
			    form_data.append("files[]", files[count]);
			   }
			  }
			  if(error == '')
			  {
			   $.ajax({
				    url: base_url + 'usuario/tienda/subirImagenLogo/'+Tie_IdTienda,
				    method:"POST",
				    data:form_data,
				    contentType:false,
				    cache:false,
				    processData:false,
				    beforeSend:function()
				    {
			    		 $("#loadTienda").html('<br><div class="progress"><div class="indeterminate blue"></div></div>');
				    },
				    success:function(r)
				    {
				      	Materialize.toast('¡Guardado exitosamente!', 1500, 'rounded', function(){
				      	window.location.href = base_url+'usuario/tienda';
				      });
				    }
			   })
			  }
			  else
			  {
			   alert(error);
			  }
			 }			
		});
	});

	//CONFIGURACION
	var colorPrimario = $('.colorPrin').data('cp');
	var colorSecundario = $('.colorAcen').data('ca');
	var nroClics = 1;

	//Set Precio
	if ($('#mostrarPrecio').data('precio') == 1){
		$('#mostrarPrecio').prop("checked", true);
	}else{ 
		$('#mostrarPrecio').prop("checked", false);
	}

	$(".cambiarColoresBtn").click(function(){
		$('#containerColors').modal({
			startingTop: '5%', // Starting top style attribute
      		endingTop: '5%', // Ending top style attribute
		});
	});

	$(".colorBox").click(function(){
		if (nroClics == 1){
			$(this).find(".checkSign").css("color","rgba(0,0,0,1)");
			$(this).css("transform","scale(0.8,0.8");
			colorPrimario = $(this).data('color');
			nroClics ++ ;
		}else if (nroClics == 2){
			$(this).find(".checkSign").css("color","rgba(255,255,255,0.6)");
			$(this).css("transform","scale(0.8,0.8");
			colorSecundario = $(this).data('color');
			nroClics ++;
		}else if (nroClics == 3){
			$('.checkSign').each(function() {
			    $(this).css("color","rgba(255,255,255,0.0)");

			});
			$('.colorBox').each(function() {
			    $(this).css("transform","scale(1,1");
			});
			$(this).css("transform","scale(0.8,0.8");
			$(this).find(".checkSign").css("color","rgba(0,0,0,0.6)");
			colorPrimario = $(this).data('color');
			nroClics = 2;
		}
		//alert($(this).data('color'));
	});
	$("#contBtn").click(function(){
		$(".cambiarColoresBtn").css("background-color",colorSecundario);
		$(".content_preview").css("background-color",colorPrimario);
		$(".colorPrin").css("background-color",colorPrimario);
		$(".colorAcen").css("background-color",colorSecundario);
	});

	$(".guardarConfi").click(function(){
		var Tie_IdTienda = $(this).data('idtienda');
		var colorHex = colorPrimario;
		var colorHex2 = colorSecundario;
        $.confirm({
            icon: 'check',
            theme: 'modern',
            closeIcon: false,
            animationBounce: 1.5,
            autoClose: 'Aceptar|3000',
            type: 'green',
            title: 'Cambios Guardados',
            columnClass: 'small',
            content: 'La configuracion se guardo exitosamente',
            draggable: false,
            buttons: {
                Aceptar: {
                	btnClass: 'btn',                    
                    action: function (){
					
						if($('#mostrarPrecio').is(':checked')) {
			                var mp = 1;
			            }else{
			                var mp = 0;
			            }
						$.ajax({data:{
							'Mostrar_Precio' : mp,
							'colorHex' : colorHex,
							'colorHex2' : colorHex2,
							'Tie_IdTienda' : Tie_IdTienda
							},
							type: 'POST',
							url: base_url+'usuario/tienda/configuracion_tienda',
							success: function (r){
								location.reload(true);
							}
						});
                    }       
                }
            }
        });
		});
});