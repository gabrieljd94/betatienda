$(document).ready(function(){
	var mapObj = new GMaps({
    div: '#map',
    lat: latitud,
    lng: longitud
  });
  
  mapObj.addMarker({
    lat: latitud,
    lng: longitud,
    draggable: true,
    title: 'Tu tienda',
    infoWindow: {
      content: '<h5 class="black-text">Mi local</h5>',
      maxWidth: 200
    },
    dragend: function(e) {
      var latitud = e.latLng.lat();
      var longitud = e.latLng.lng();

      $.ajax({
        data:{
          'latitud':latitud,
          'longitud':longitud
        },
        method:"POST",
        url: base_url+'usuario/tienda/ajax_obtener_direccion',
        success:function(data)
        {
          document.getElementById("direccionLocal").value = data;
        }
      });

      document.getElementById("latitud").value = latitud;
      document.getElementById("longitud").value = longitud;
    }
  });
});