$(document).ready(function(){

	$('select.grafReport').change(function(){
		var labelG;
		var labels2;
		var data2
		if ($(this).val() == 1) 
		{
			var fecha_inicio = $("#fecha_inicio").val();
		    var fecha_fin = $("#fecha_fin").val();

		    $.ajax({
		        type: "POST",
		        data:
		            {
		              "fecha_inicio": fecha_inicio,
		              "fecha_fin": fecha_fin
		            },
		        url: base_url + 'usuario/reporte/load_reporte_pedidos',
		        success: function (data) {
		        	if (data != ""){
		        		$(".reporteContainer").slideUp(500);
		        		$("#reporteLoad").html('');
		            	$("#reporteLoad").html(data);
		            	$(".reporteContainer").slideDown(500);
		        	}else{

		        	}
		        }
		    });

			labelG = 'PEDIDOS';
			labels2 = ["Pedido 1", "Pedido 2", "Pedido 3", "Pedido 4", "Pedido 5"];
			data2 = [2, 3, 1, 3, 4];
		} 
		else if ($(this).val() == 2)
		{
			
			labelG = 'COTIZACIONES';
			labels2 = ["Cotización 1", "Cotización 2", "Cotización 3"];
			data2 =[112, 120, 33];
		} 
		else
		{
			var fecha_inicio = $("#fecha_inicio").val();
		    var fecha_fin = $("#fecha_fin").val();

		    $.ajax({
		        type: "POST",
		        data:
		            {
		              "fecha_inicio": fecha_inicio,
		              "fecha_fin": fecha_fin
		            },
		        url: base_url + 'usuario/reporte/load_reporte_productos',
		        success: function (data) {
		        	if (data != ""){
		        		$(".reporteContainer").slideUp(500);
		        		$("#reporteLoad").html('');
		            	$("#reporteLoad").html(data);
		            	$(".reporteContainer").slideDown(500);
		        	}else{

		        	}
		        }
		    });
		    
			labelG = 'PRODUCTOS';
			labels2 = ["Polo 1", "Polo 2", "Polo 3", "Polo 4", "Polo 5"];
			data2 = [100, 200, 300, 150, 220];
		}

		var ctx = document.getElementById("myChart");			

		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        labels: labels2,
		        datasets: [{
		            label: labelG,
		            data: data2,
		            borderWidth: 2
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});
	});
});