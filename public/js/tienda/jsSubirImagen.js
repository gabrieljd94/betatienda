$(document).ready(function(){	
	$('.upImgSku').change(function(){

		var SKU_IdSku = $(this).data('id');
		Pro_IdProducto = $(this).data('idpro');
		var SKU_Nombre = $(this).data('sku');

		console.log(SKU_IdSku,SKU_Nombre);

	  var files = $('#sku'+SKU_IdSku)[0].files;
	  console.log(files);
	  var error = '';
	  var form_data = new FormData();
	  for(var count = 0; count<files.length; count++)
	  {
	   var name = files[count].name;
	   var extension = name.split('.').pop().toLowerCase();
	   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
	   {
	    error += "Imagen  " + count + " invalida"
	   }
	   else
	   {
	    form_data.append("files[]", files[count]);
	   }
	  }
	  if(error == '')
	  {
	   $.ajax({
	    url: base_url + 'usuario/inventario/subirImagenSku/'+SKU_IdSku,
	    method:"POST",
	    data:form_data,
	    contentType:false,
	    cache:false,
	    processData:false,
	    beforeSend:function()
	    {
	    },
	    success:function(r)
	    {
	      console.log(r);
	      // window.location.href = base_url+'usuario/inventario/productoDetalle/'+Pro_IdProducto;
	    }
	   })
	  }
	  else
	  {
	   alert(error);
	  }
	 });

});