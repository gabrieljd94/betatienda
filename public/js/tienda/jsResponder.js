  $(document).ready(function(){
	$('.rComentario').click(function(){
		var Com_IdComentario = $(this).data('idcomentario');
		var Respuesta = $('#respuesta'+Com_IdComentario).val();
		console.log(Com_IdComentario, Respuesta);
		$.ajax({
	 	data:{
		'Com_IdComentario' : Com_IdComentario,
		'Respuesta': Respuesta
		},
	    type: "POST",
	    url: base_url+'usuario/comentario/responder', 
	    dataType: "json",
	    success: function(data){
	    	Materialize.toast('<i class="material-icons left">check_circle</i> Comentario enviado', 2000, 'rounded successToast',function(){
				window.location.href = base_url+'usuario/comentario';});
		 },
	    error: function(data) {
			Materialize.toast('ERROR AL RESPONDER', 3000, 'rounded');
	    }
	  });

	});
  });