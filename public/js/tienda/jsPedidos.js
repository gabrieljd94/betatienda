  $(document).ready(function(){
    $('.collapsible').collapsible();

    $('.collapse_pedido').click(function(){

		var id_pedido = $(this).data('id_pedido');

		$.ajax({
		 	data:{
				'Ped_IdPedido' : id_pedido
			},
		    type: "POST",
		    url: base_url+'comprador/index/ajax_detalle_pedido',
		    success: function(data){
		    	$('#ajax_detalle_pedido'+id_pedido).html('');
		    	$('#ajax_detalle_pedido'+id_pedido).html(data);
			},		    
	  	});
	});
});