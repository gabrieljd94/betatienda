$(document).ready(function(){
	var colorPrimario = $('.colorPrin').data('cp');
	var colorSecundario = $('.colorAcen').data('ca');
	var nroClics = 1;

	//Set Precio
	if ($('#mostrarPrecio').data('precio') == 1){
		$('#mostrarPrecio').prop("checked", true);
	}else{ 
		$('#mostrarPrecio').prop("checked", false);
	}

	$(".cambiarColoresBtn").click(function(){
		$('#containerColors').modal();
	});

	$(".colorBox").click(function(){
		if (nroClics == 1){
			$(this).find(".checkSign").css("color","rgba(0,0,0,1)");
			$(this).css("transform","scale(0.8,0.8");
			colorPrimario = $(this).data('color');
			nroClics ++ ;
		}else if (nroClics == 2){
			$(this).find(".checkSign").css("color","rgba(255,255,255,0.6)");
			$(this).css("transform","scale(0.8,0.8");
			colorSecundario = $(this).data('color');
			nroClics ++;
		}else if (nroClics == 3){
			$('.checkSign').each(function() {
			    $(this).css("color","rgba(255,255,255,0.0)");

			});
			$('.colorBox').each(function() {
			    $(this).css("transform","scale(1,1");
			});
			$(this).css("transform","scale(0.8,0.8");
			$(this).find(".checkSign").css("color","rgba(0,0,0,0.6)");
			colorPrimario = $(this).data('color');
			nroClics = 2;
		}
		//alert($(this).data('color'));
	});
	$("#contBtn").click(function(){
		$(".mockupPreviewBtn").css("background-color",colorSecundario);
		$(".content_preview").css("background-color",colorPrimario);
		$(".colorPrin").css("background-color",colorPrimario);
		$(".colorAcen").css("background-color",colorSecundario);
	});

	$(".guardarConfi").click(function(){
		var Tie_IdTienda = $(this).data('idtienda');
		var colorHex = colorPrimario;
		var colorHex2 = colorSecundario;
        $.confirm({
            icon: 'check',
            theme: 'modern',
            closeIcon: false,
            animationBounce: 1.5,
            autoClose: 'Aceptar|3000',
            type: 'green',
            title: 'Cambios Guardados',
            columnClass: 'small',
            content: 'La configuracion se guardo exitosamente',
            draggable: false,
            buttons: {
                Aceptar: {
                	btnClass: 'btn',                    
                    action: function (){
					
						if($('#mostrarPrecio').is(':checked')) {
			                var mp = 1;
			            }else{
			                var mp = 0;
			            }
						$.ajax({data:{
							'Mostrar_Precio' : mp,
							'colorHex' : colorHex,
							'colorHex2' : colorHex2,
							'Tie_IdTienda' : Tie_IdTienda
							},
							type: 'POST',
							url: base_url+'usuario/tienda/configuracion_tienda',
							success: function (r){
								location.reload(true);
							}
						});
                    }       
                }
            }
        });
		});
	});