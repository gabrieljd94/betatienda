$(document).ready(function(){
	jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: [] 
    });

    $("#form_addLocal" ).validate({
      rules: {
        field: {
          required: true,
          file: false,
        }
      }
    });

	$('.btnUpdateLocal').click(function(){
		var Loc_IdLocal = $('#Loc_IdLocal').val();

		console.log(Loc_IdLocal);
		var nombreLocal = $('#nombreLocal').val();
		var dirLocal = $('#direccionLocal').val();
		var tlfLocal = $('#tlfLocal').val();
		var latitud = $('#latitud').val();
		var longitud = $('#longitud').val();
		
		if (!$("#form_addLocal").validate().form()){
            Materialize.toast('<i class="material-icons left ">warning</i>No puedes dejar campos vacios', 1500, 'rounded warningToast');
        }else{
        	$('#modalLocal2').modal('close');
			$.ajax({
			 	data:{
				'Loc_IdLocal' : Loc_IdLocal,
				'nombre': nombreLocal,
				'direccion': dirLocal,
				'telefono': tlfLocal,
				'latitud': latitud,
				'longitud': longitud
				},
			    type: "POST",
			    url: base_url+'usuario/tienda/update_local',
			    dataType: "json",
			    success: function(data){
			    	Materialize.toast('¡Local editado exitosamente!', 1500, 'rounded', function(){location.reload(true)});
				},
			});
		}
	});

	$('.dirLocal').focus(function(){
		Materialize.toast('Se lo mas <span class="dirEnfa">especifico</span> posible', 2000, 'rounded');
	});
});