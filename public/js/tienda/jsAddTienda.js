$(document).ready(function(){

	    let editor;

	    ClassicEditor
	        .create( document.querySelector( '#descripcionDetalle' ),{
	        	toolbar: [ 'heading', '|', 'bold', 'italic', 'blockQuote' ]
	        } )
	        .then( newEditor => {
	            editor = newEditor;
	        } )
	        .catch( error => { 
	            console.error( error );
	        } );

	    // Assuming there is a <button id="submit">Submit</button> in your application.
	    $('.btnInsertDetalle').click(function(){
	        const DetalleTienda = editor.getData();
			var Tie_IdTienda = $(this).data('id');

			$.ajax({
		 	data:{
			'Tie_IdTienda' : Tie_IdTienda,
			'DetalleTienda' : DetalleTienda
			},
		    type: "POST",
		    url: base_url+'usuario/tienda/insert_detalle_tienda', 
		    dataType: "json",
		    success: function(data){
				var files = $('.detalleImg')[0].files;		
		  		var error = '';
		  		var form_data = new FormData();
				  for(var count = 0; count<files.length; count++)
				  {
				   var name = files[count].name;
				   var extension = name.split('.').pop().toLowerCase();
				   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
				   {
				    error += "Imagen  " + count + " invalida";
				    Materialize.toast('Agrege una imagen correcta', 3000, 'rounded');
				   }
				   else
				   {
				    form_data.append("files[]", files[count]);
				   }
				  }
				  if(error == '')
				  {
				   $.ajax({
				    url: base_url + 'usuario/tienda/subirImagenTienda/'+data,
				    method:"POST",
				    data:form_data,
				    contentType:false,
				    cache:false,
				    processData:false,
				    beforeSend:function()
				    {				  		
		    		 $("#mainProducto").html('<br><br><br><br><br><div class="progress"><div class="indeterminate blue"></div></div>');
				    },
				    success:function(r)
				    {
				     window.location.href = base_url+'usuario/tienda/dTienda';
				    }
				   })
				  }
				  else
				  {
				   alert(error);
				  }			
			 },
		    error: function(data) {
		      alert('error');
		    }
		  });
		});


		$('.eliminarContenido').click(function(){
			var DtiendaId = $(this).data('idt');
			$.ajax({
			 	data:{
				'Tid_IdTiendaDetalle' : DtiendaId
				},
			    type: "POST",
			    url: base_url+'usuario/tienda/eliminar_detalle_tienda', 
			    dataType: "json",	
			    success:function(r)
			    {
			      window.location.href = base_url+'usuario/tienda/dTienda';
			    }
				})	  
			});
});