$(document).ready(function(){
  var fotosProducto= [];
  [].forEach.call(document.getElementsByClassName('fileInputProduct'), function(element) {
    element.onchange = fotoNueva;
  });

  function fotoNueva() {
    fotosProducto.push(this.files[0]);
    console.log(fotosProducto);
  };

  function etiquetaEnter(textareaID) {
      (function($) {        
          if (!textareaID) var textareaID = "#descripcionProducto";
          var textarea = $(textareaID);
          
          textarea.val(
              $(textarea).val().replace(/(\r\n|\n)/g, "<br/>")
          );
          
      })(jQuery);        
  }
    //Botones tabla  
    $('.btnDelete').click(function(){
        var SKU_IdSKU = $(this).data('sku');
        $.confirm({
            icon: 'help',
            theme: 'modern',
            closeIcon: false,
            animation: 'scale',
            type: 'orange',
            title: 'Eliminar variacion',
            columnClass: 'small',
            content: 'Luego no podras recuperar su informacion',
            draggable: false,
            buttons: {
                Eliminar: {                    
                    action: function (){
                     $.ajax({
                        data:{
                        'SKU_IdSKU' : SKU_IdSKU
                        },
                          type: "POST",
                          url: base_url+'usuario/inventario/eliminarSku', 
                          dataType: "json",
                          success: function(data){
                             location.reload();
                         },
                        });
                    }       
                },
                Cancelar: {
                    btnClass: 'btn',    
                }
            }
        });
    });

    //Botones tabla  
    $('.btnDeleteGen').click(function(){
        var Pro_IdProducto = $(this).data('idpro');
        $.confirm({
            icon: 'help',
            theme: 'modern',
            closeIcon: false,
            animation: 'scale',
            type: 'orange',
            title: 'Eliminar producto',
            columnClass: 'small',
            content: 'Luego no podras recuperar su informacion',
            draggable: false,
            buttons: {
                Eliminar: {                    
                    action: function (){
                     $.ajax({
                        data:{
                        'Pro_IdProducto' : Pro_IdProducto
                        },
                          type: "POST",
                          url: base_url+'usuario/inventario/eliminarPro', 
                          dataType: "json",
                          success: function(data){
                             location.reload();
                         },
                        });
                    }       
                },
                Cancelar: {
                    btnClass: 'btn',    
                }
            }
        });
    });

        //EDITAR PRODUCTO
    $(".editProVar").click(function(){
        var idsku = $(this).data('sku');
        $('.contentEdit').load(base_url+'usuario/inventario/sku/'+idsku);
        $('#modalEditPro').modal({
          startingTop: '1%',
          endingTop: '2%'
        });
    });

      //Producto en Oferta
      if ($('#oferta').data('precio') == 1){
        $('#oferta').prop("checked", true);
      }else{
        $('#oferta').prop("checked", false);
      }

      $('#oferta').change(function(){
          if($('#oferta').is(':checked')) {
              var oferta = 1;
          }else{
              var oferta = 0;
          }

          var Pro_IdProducto = $(this).data('idpro');

          console.log(oferta,Pro_IdProducto);

          $.ajax({
            type: "POST",
            data: {
              "Pro_IdProducto": Pro_IdProducto,
              "oferta": oferta 
            },
            url: base_url + 'usuario/inventario/oferta',
            success: function (data) {                  
              Materialize.toast('<i class="material-icons left ">check_circle</i> Cambios guardado exitosamente', 2000, 'rounded successToast');
            }
          });
      });

    /****Validacion***/
    $('.stepper').activateStepper({
        autoFocusInput: true,
        showFeedbackLoader: false,
        linearStepsNavigation: false,
    });
    
    $('#conNewPro1').click(function(){
        etiquetaEnter($('#descripcionProducto').val());
        var precioMin = parseInt($('#precioMin').val());
        var precioMax = parseInt($('#precioMax').val());
        var nombreProducto = ($('#nombreProducto').val());
        if ($('#descripcionProducto').val()=='' || $('#precioMin').val()=='' || $('#precioMax').val()=='' || nombreProducto ==''){
            Materialize.toast('<i class="material-icons left ">warning</i>No puedes dejar campos vacios', 1500, 'rounded warningToast');
        }else if (precioMin > precioMax || precioMax < precioMin) { 
            Materialize.toast('<i class="material-icons left ">warning</i>El precio minimo debe ser menor al precio mayor', 1500, 'rounded warningToast');
        }else{
            var arrNombrePro = nombreProducto.split(" ");
            $.ajax({
                  type: "POST",
                  data: {
                    "arrNombrePro": arrNombrePro
                  },
                  url: base_url + 'usuario/inventario/buscar_cat_sub',
                  success: function (data) {
                    console.log(data);
                    var b = JSON.parse(data);
                     $('#catSelect').val(b.categoria_Cat_IdCategoria);
                     $('#sucSelect').val(b.Suc_IdSubCategoria);
                     $('#sucSelect').trigger('change');
                  }
                });


            $('#addNewPro').nextStep();
        }
        console.log(precioMax+"  "+precioMin);
    });

    $('#conNewPro2').click(function(){
        var Categoria = $('#catSelect > option:selected').val();
        var SubCategoria = $('#sucSelect > option:selected').val();
        var SubCategoriaDetalle = $('#sucDetalle > option:selected').val();
        var files = new Array (); 
        $(".filePro").each(function(){
            if($(this).val()!=""){
              files.push($(this).val());
            }
        });
        // var files = $('.filePro')[0].files;
        var listVar = $("#lista_variacion").text();

        //Validar si estan vacios los datos
        if (Categoria == '' || SubCategoria == '' ||  SubCategoriaDetalle == '' ||  files.length == 0) {
            Materialize.toast('<i class="material-icons left ">warning</i>No puedes dejar campos vacios', 1500, 'rounded warningToast');
        }else{
            var vaoId = [];
            var varId = [];
            var vaoNombre = [];
            var colores = $("#coloresVar").select2("data");
            var cText = [];
            var tallasExiste;
            var varNombre = [];

            if ($('#rowTallas').hasClass('No existe')){
                tallasExiste = false;
            }else{
                tallasExiste = true;
            }

            if (colores != 'undefined')
            {
                for (var i = 0; i < colores.length; i++) {
                cText.push(colores[i]["text"]);         
                }
            }
            $('.checkboxVar:checked').each(function(){
                vaoId.push($(this).attr("id"));
                varId.push($(this).data('vari'));
                vaoNombre.push($(this).data('vaon'));
                varNombre.push($(this).data('nomvar'));

            });
            if ($(".checkboxVar:checked").length > 0 && varId.length != 0 && vaoId.length != 0 && vaoNombre.length != 0 && colores != 'undefined' && cText.length != 0 ) {      
                console.log(varNombre);
                $.ajax({
                  type: "POST",
                  data: {
                    "varId": varId,
                    "vaoId": vaoId,
                    "vaoNombre": vaoNombre,
                    "varNombre":varNombre,
                    "colorVar": cText
                  },
                  url: base_url + 'usuario/inventario/agregar_variacion',
                  success: function (data) {                  
                    $("#lista_variacion").html('');
                    $("#lista_variacion").html(data);
                    $(".nombrePro").text($("#nombreProducto").val()); 
                    $('#addNewPro').nextStep();      
                  }
                });
            }else{
                Materialize.toast('<i class="material-icons left ">warning</i>No puedes dejar campos vacios', 1500, 'rounded warningToast');
            }
        }
    });

    $('#conNewPro3').click(function(){
      etiquetaEnter($('#descripcionProducto').val());
        var Valido = true;
        $('#lista_variacion input[type="number"]').each(function() {
            if ($(this).val() == '') {
                Valido = false;
            }
        });
        if(Valido == true){
            $.confirm({
              icon: 'save',
              theme: 'modern',
              closeIcon: false,
              animation: 'scale',
              type: 'green',
              title: 'Guardar producto',
              columnClass: 'small',
              content: 'No podras regresar aqui despues',
              draggable: false,
              buttons: {
                  Guardar: {
                    btnClass: 'btn',                    
                    action: function (){
                        var NombreProducto = $('#nombreProducto').val();
                        var DescProducto = $('#descripcionProducto').val();
                        var Categoria = $('#catSelect > option:selected').val();
                        var SubCategoria = $('#sucSelect > option:selected').val();
                        var SubCategoriaDetalle = $('#sucDetalle > option:selected').val()
                        var Unidad = $('#unmSelect > option:selected').val();
                        var valorUnm = $('#valorUnm2').val();
                        var precioMin = $('#precioMin').val();
                        var precioMax = $('#precioMax').val();
                        var files = fotosProducto[0].files;
                        var combId = [];
                        var combCantidad = [];
                         $(".cntVar").each(function(){
                                 combCantidad.push($(this).val());
                                 combId.push($(this).data('idcom'));
                            });   
                        var valorUnidad = fvalorUnidad(Unidad,valorUnm);

                        $.ajax({
                            data:{
                                'Pro_Nombre' : NombreProducto,
                                'Pro_Descripcion' : DescProducto,
                                'Cat_IdCategoria' : Categoria,
                                'Suc_IdSubCategoria' : SubCategoria,
                                'Des_IdDetalle_SubCategoria' : SubCategoriaDetalle,
                                'Unidad' : Unidad,
                                'valorUnm' : valorUnidad,
                                'precioMin' : precioMin,
                                'precioMax' : precioMax,
                                'combId' : combId,
                                'combCantidad' : combCantidad
                            },
                            type: "POST",
                            url: base_url+'usuario/inventario/agregar_producto', 
                            success: function(r){
                                console.log(r);
                                var files = fotosProducto;
                                var error = '';
                                var form_data = new FormData();
                                  for(var count = 0; count<files.length; count++){
                                       var name = files[count].name;
                                       var extension = name.split('.').pop().toLowerCase();
                                       if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1){
                                        error += "Imagen  " + count + " invalida";
                                        Materialize.toast('<i class="material-icons left ">warning</i>Agrege una imagen correcta', 2000, 'rounded warningToast');
                                       }
                                       else{
                                            form_data.append("files[]", files[count]);
                                       }
                                  }
                                  if(error == ''){
                                       $.ajax({
                                            url: base_url + 'usuario/inventario/subirImagen/'+r,
                                            method:"POST",
                                            data:form_data,
                                            contentType:false,
                                            cache:false,
                                            processData:false,
                                            beforeSend:function(){                       
                                                $("#lista_variacion").html('<br><br><br><br><br><div class="progress customColorA"><div class="indeterminate customColorB"></div></div>');
                                            },
                                            success:function(r1){
                                                window.location.href = base_url+'usuario/inventario/productoDetalle/'+r1;
                                            }
                                       });
                                  }
                                  else{
                                   alert(error);
                                  }
                            },
                            error: function(data) {
                              alert('error 222');
                            }
                        });
                    }       
                  },
                  Cancelar: {
                      btnClass: 'btn-red',    
                  }
              }
            });    
        }else{
            Materialize.toast('<i class="material-icons left ">warning</i>Agrega todas las cantidades', 1500, 'rounded warningToast');
        }
    });

    $('#addNewPro').on('step2', function(){
        $.ajax({
          type: "POST",
          data: {
            'valor': 'eliminar variacion'
          },
          url: base_url + 'usuario/inventario/eliminar_variacion',
          success: function (data) {

          }
        });
    });

    //Cantidad fija en todos
    $('#addNewPro').on('step3', function(){
        $.confirm({
          title: '¿Todos los productos tienen la misma cantidad?',
          type: 'orange',
          typeAnimated: true,
          content: '' +
          '<div class="input-field">' +
            '<label>Cantidad Base</label>' +
            '<input type="number" minlenght="1" class="cantidadBase" required />' +
          '</div>',
          buttons: {
              formSubmit: {
                  text: 'Agregar',
                  btnClass: 'btn',
                  action: function () {
                    $(".cntVar").each(function(){
                      $(this).prop("value", $(".cantidadBase").val());
                    });
                    //$("#containerPro").animate({ scrollTop: $('#conNewPro3').offset().top }, 300);
                  }
              },
              Cancelar: function () {
                  //close
              },
          },
          onContentReady: function () {
              // bind to events
              var jc = this;
              this.$content.find('form').on('submit', function (e) {
                  // if the user submits the form by pressing enter in the field.
                  e.preventDefault();
                  jc.$$formSubmit.trigger('click'); // reference the button and click it
              });
          }
      });
    });

    $('#productoBtn').click(function(){
        $('#containerPro').modal({
          startingTop: '1%',
          endingTop: '2%'
        });
    });

    $("#filePro").click(function(){
        Materialize.toast('Puedes agregar <span class="dirEnfa">varias</span> fotos', 2000, 'rounded');
    });

    $(".stepCaract").on("select2:open", '#coloresVar', function() {
        Materialize.toast('Agrega <span class="dirEnfa">todos</span> los colores necesarios', 2000, 'rounded');
    });
   
    $("#catSelect").change(function(){
        var Cat_IdCategoria = $(this).val();
        if (document.getElementById("rowContentVariacion")){
            $("#rowContentVariacion").hide(1000);
        }
         $.ajax({
            data:{
            'Cat_IdCategoria' : Cat_IdCategoria
            },
            type: "POST",
            url: base_url+'usuario/inventario/select_subc', 
            dataType: "json",
            success: function(data){
                $('#divSucSelect').load(base_url+'usuario/inventario/Select_subCategoria',{datos:data});
            },
            error: function(data) {
              alert('error');
            }
          });
    });
    $("#unmSelect").change(function(){
        if ($(this).val() == 3 || $(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6 ) {
            $('.nombreUnidad').text($("#unmSelect option:selected").text());
            $('#valorUnm').removeClass('hide');
        }
        else{
            $('#valorUnm').addClass('hide');
        }
    
    });

    $('.addMasVar').click(function(){
      var Des_IdDetalle_SubCategoria = $(this).data('iddsc');
      var Pro_IdProducto = $(this).data('idpro');
      $.ajax({
        data:{
        'Des_IdDetalle_SubCategoria' : Des_IdDetalle_SubCategoria,
        'Pro_IdProducto' : Pro_IdProducto
        },
        type: "POST",
        url: base_url+'usuario/inventario/variaciones_categorias', 
        dataType: "json",
        success: function(data){
            console.log(data);
            $('#aggVar2').load(base_url+'usuario/inventario/var_cat2',{datos:data,Des_IdDetalle_SubCategoria:Des_IdDetalle_SubCategoria,Pro_IdProducto:Pro_IdProducto});
        },
        error: function(data) {
          alert('error');
        }
      });
    });

    $('.upImgSku').change(function(){
    var SKU_IdSku = $(this).data('id');
    Pro_IdProducto = $(this).data('idpro');
    var SKU_Nombre = $(this).data('sku');
    console.log(SKU_IdSku,SKU_Nombre);

      var files = $('#sku'+SKU_IdSku)[0].files;
      console.log(files);
      var error = '';
      var form_data = new FormData();
      for(var count = 0; count<files.length; count++)
      {
       var name = files[count].name;
       var extension = name.split('.').pop().toLowerCase();
       if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
       {
        error += "Imagen  " + count + " invalida"
       }
       else
       {
        form_data.append("files[]", files[count]);
       }
      }
      if(error == '')
      {
       $.ajax({
        url: base_url + 'usuario/inventario/subirImagenSku/'+SKU_IdSku,
        method:"POST",
        data:form_data,
        contentType:false,
        cache:false,
        processData:false,
        beforeSend:function()
        {
            $('#loadSkuImg'+SKU_IdSku).removeClass();
            $('#skuImg'+SKU_IdSku).addClass('hide');
        },
        success:function(r)
        {
            // $('#loadSkuImg').removeClass().addClass('preloader-wrapper big active');
          window.location.href = base_url+'usuario/inventario/productoDetalle/'+Pro_IdProducto;
        }
       })
      }
      else
      {
       alert(error);
      }
    });
    $(".subirImgVarCat2").click(function(){
        Materialize.toast('El cambio sera <span class="dirEnfa">inmediato</span>', 2000, 'rounded');
    });
});  
  
    function fvalorUnidad(idUnidad, vUnidad){
        var valorUnidad = 0;
        if (idUnidad == 1) {
            valorUnidad = 1;
        } else if (idUnidad == 2){
            valorUnidad = 12;
        
        } else if (idUnidad == 3){  
            if (vUnidad == '') {
                valorUnidad = 1;
            }else{
                valorUnidad = vUnidad;
            }
        } else if (idUnidad == 4){
            if (vUnidad == '') {
                valorUnidad = 1;
            }else{
                valorUnidad = vUnidad;
            }
        } else if (idUnidad == 5){
            if (vUnidad == '') {
                valorUnidad = 1;
            }else{
                valorUnidad = vUnidad;
            }
        } else if (idUnidad == 6){  
            if (vUnidad == '') {
                valorUnidad = 1;
            }else{
                valorUnidad = vUnidad;
            }
        } else if (idUnidad == 7){
            valorUnidad = 100;
        } else if (idUnidad == 8){
            valorUnidad = 1000;
        } else if (idUnidad == 9){
            valorUnidad = 1;
        } else if (idUnidad == 10){
            valorUnidad = 1;
        } else if (idUnidad == 11){ 
            valorUnidad = 1;
        } else if (idUnidad == 12){
            valorUnidad = 1;
        } else if (idUnidad == 13){
            valorUnidad = 1;
        } else if (idUnidad == 14){
            valorUnidad = 1;
        }
        return valorUnidad;
    }
