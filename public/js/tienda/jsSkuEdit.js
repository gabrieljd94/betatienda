$(document).ready(function(){
	var numNeg = false;
	$('.materialboxed').materialbox();
	$('.upImgSku').change(function(){
		SKU_IdSku = $(this).data('sku');
		console.log(SKU_IdSku);
		Sku = parseInt(SKU_IdSku);
		  var files = $('#sku'+SKU_IdSku)[0].files;
		  console.log(files);
		  var error = '';
		  var form_data = new FormData();
		  for(var count = 0; count<files.length; count++)
		  {
		   var name = files[count].name;
		   var extension = name.split('.').pop().toLowerCase();
		   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
		   {
		    error += "Imagen  " + count + " invalida"
		   } 
		   else
		   {
		    form_data.append("files[]", files[count]);
		   }
		  }
		  if(error == '')
		  {
		   $.ajax({
		    url: base_url + 'usuario/inventario/subirImagenSku/'+Sku,
		    method:"POST",
		    data:form_data,
		    contentType:false,
		    cache:false,
		    processData:false,
		    beforeSend:function()
		    {
		 		$('#imgSkuEdit').hide(1000);
		 		$('#loadImgSku').show(1000);
		 		$('#loadImgSku').html('<div class="progress customColorA"><div class="indeterminate customColorB"></div></div>');

		    },
		    success:function(r)
		    {
		    	$('#loadImgSku').hide(1000);
		      	$('#imgSkuEdit').show(1000);
		    }
		   })
	  }
	  else
	  {
	   alert(error);
	  }
	 });
	$('#cantidadSKUact').focus(function(){
		Materialize.toast('Cant. de unidades agregar/disminuir', 2000, 'rounded center');
	});

	$('input[name="skuact"]').click(function(){
		var CantidadNueva = $('#cantidadSKUact').val();
		var sum_res = $('input[name="skuact"]:checked').attr('id');
		var SKU_CantidadActual = $('#cantidadSKU2').val();
		var operacion = 0;
		if (sum_res == 'sumar') {
			operacion = parseInt(SKU_CantidadActual) + parseInt(CantidadNueva);
		}else{
			operacion = parseInt(SKU_CantidadActual) - parseInt(CantidadNueva);
		}
			$('#cantidadSKU').val(operacion);

		if(operacion > 0){
			numNeg = true;
		}else{
			numNeg = false;
		}
	});
	$('.saveSkuEdit').click(function(){
		var SKU_IdSku = $(this).data('idsku');
		var SKU_Cantidad = $('#cantidadSKU').val();
		if (numNeg == true || $('#cantidadSKU').val()==''){
			$.ajax({
            data:{
            'SKU_IdSku' : SKU_IdSku,
            'SKU_Cantidad' : SKU_Cantidad
            },
            type: "POST",
            url: base_url+'usuario/inventario/skuEdit', 
            success: function(data){
            	$.confirm({
		            icon: 'check',
		            theme: 'modern',
		            closeIcon: false,
		            animationBounce: 1.5,
		            autoClose: 'Aceptar|3000',
		            type: 'green',
		            title: 'Cambios Guardados',
		            columnClass: 'small',
		            content: 'La variacion se edito exitosamente',
		            draggable: false,
		            buttons: {
		                Aceptar: {
		                	btnClass: 'btn',                    
		                    action: function (){
		                    	location.reload(true);
		                    }       
		                }
		            }
		        });
               
            },
            error: function(data) {
              alert('error');
            }
          });
		}else{
			Materialize.toast('<i class="material-icons left ">warning</i>El stock esta en negativo o no hay datos', 2000, 'rounded warningToast');
		}
	});
   });	