$(document).ready(function(){
    $('.nav-content').hide(500); 
    $('.comentsBtn').addClass('hide');
    $('#pedidoItems').stick_in_parent({
        inner_scrolling: false,
        offset_top: 65
      });

$('#selecRegion').material_select();
    $("#siDireccion").on( 'change', function() {
    if( $(this).is(':checked') ) {
          $("#direccionVista").removeClass('hide');
        // Hacer algo si el checkbox ha sido seleccionado
        // alert("El checkbox con valor " + $(this).val() + " ha sido seleccionado");
    } else {
        $("#direccionVista").addClass('hide');
        // Hacer algo si el checkbox ha sido deseleccionado
        // alert("El checkbox con valor " + $(this).val() + " ha sido deseleccionado");
    }
});
    $("#selecRegion").change(function(){
        var idRegion = $(this).val();
        $.ajax({
           data:{
           'idRegion' : idRegion
           },
           type: "POST",
           url: base_url+'tienda/productos/load_provincia', 
           dataType: "json",
           success: function(data){
               $('#provincia').load(base_url+'tienda/productos/Select_Provincia',{datos:data});
           },
           error: function(data) {
             alert('error');
           }
        });
 
    });
    
    $("#btnCotizar").click(function(){
       var nombre = [ $('#nombreComprador').val(), $('#nombreComprador')];
       // var apellido = [ $('#apellidoComprador').val(), $('#apellidoComprador')];
       var telf = [ $('#tlf').val(), $('#tlf')];
       var idtienda = $(this).data('idtienda');
       console.log(idtienda);
       var correo = [ $('#correo').val(), $('#correo')]; 
       var comentario = [];         
        if( $("#siDireccion").is(':checked') ) {
          console.log('1c');
          var region = [ $('#selecRegion').val(), $('#selecRegion')];
          var provincia = [ $('#selProvincia').val(), $('#selProvincia')];
          var distrito = [ $('#selDistrito').val(), $('#selDistrito')];
          var direccion = [ $('#direccionComprador').val(), $('#direccionComprador')];
          
        } else {  
          var region = ["0", $('#campoNO')];
          var provincia = ["0", $('#campoNO')];
          var distrito = ["0", $('#campoNO')];
          var direccion = ["0", $('#campoNO')];
          console.log('2o');
          
        }
        if ($('textarea#comentario').val() === "undefined") {
         comentario = [$('textarea#comentario').val(), $('textarea#comentario')];   
         console.log('3w');  
        } else {
         comentario = [ "Sin comentario", $('textarea#comentario')];       
         console.log('4w');  
        }
          console.log(comentario);
       var arr = [];
       
       arr.push(nombre, telf, correo, region, provincia, distrito, direccion,comentario);
       var error = false;
       //console.log(arr);
       arr.forEach(function(element){
            if ((element[0]) == ""){
                error = true;
                element[1].addClass(" invalid");
            }
       });
        if (error){
            Materialize.toast('<i class="material-icons left ">warning</i>No puedes dejar campos vacios', 2000, 'rounded warningToast');
        }else{
            arr2 = [];
            i=0;
            arr.forEach(function(element){
                arr2[i] = element[0];
                i++;
            });
            console.log(arr2);
            $.confirm({
                icon: 'send',
                theme: 'modern',
                closeIcon: false,
                animation: 'scale',
                type: 'green',
                title: 'Enviar Cotizacion',
                columnClass: 'small',
                content: 'Los datos ingresados deben ser correctos y veridicos',
                draggable: false,
                buttons: {
                    Enviar: {
                      btnClass: 'btn',                    
                      action: function (){
                        $.ajax({data:{
                          'arrData': arr2,
                          'idtienda':idtienda
                          },
                          type: 'POST',
                          url: base_url+'tienda/productos/enviarPedido',
                          success: function (r){
                            $.confirm({
                              icon: 'check',
                              theme: 'modern',
                              closeIcon: false,
                              animationBounce: 1.5,
                              autoClose: 'Aceptar|3000',
                              type: 'green',
                              title: 'Cotizacion enviada',
                              columnClass: 'small',
                              content: 'La cotizacion fue enviada exitosamente',
                              draggable: false,
                              buttons: {
                                  Aceptar: {
                                    btnClass: 'btn',                    
                                      action: function (){
                                        window.location.href = base_url+'index';
                                          
                                      }       
                                  }
                              }
                            });
                              
                          }
                      });   
                      }       
                    },
                    Cancelar: {
                
                    }
                }
              });
            
        }
    });
});