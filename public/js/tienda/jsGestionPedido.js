$(document).ready(function(){
	$('.pedidoId').click(function(){
		var id = $(this).data('id');
		window.location.href = base_url+'usuario/cotizar/gestion_pedido_detalle/'+id;
    });
	
	$('#enviarCotizacion').click(function(){
		var correo = $(this).data('email');
		var idPedido = $(this).data('idpedido');
		var Valido = true;
        $('.validacionPrecio input[type="number"]').each(function() {
            if ($(this).val() == '') {
                Valido = false;
            }
        });
        var arrayPrecios = [];
        var arrayPreciosDscto = [];
		var precio;

		$('.valorU').each(function(){
			precio = parseInt($(this).val());
			arrayPrecios.push(precio);
		});

		$('.valorUDscto').each(function(){
			precioDscto = parseInt($(this).val());
			arrayPreciosDscto.push(precioDscto);
		});
		// console.log(correo);
		if (Valido == false){
	    	Materialize.toast('<i class="material-icons left ">warning</i>Coloca los precios del producto', 2000, 'rounded warningToast');
	    }
	    else{
	    	$('#enviarCotizacion').addClass('disabled');
		    $.confirm({
	        	icon: 'help',
	            theme: 'modern',
	            closeIcon: false,
	            animation: 'scale',
	            type: 'orange',
	            title: 'Enviar cotización',
	            columnClass: 'small',
	            content: 'El comprador recibira un correo con los precios ingresados',
	            draggable: false,
	            buttons: {
	                Aceptar: {
	                btnClass: 'btn',
	                    action: function (){
	                    	console.log('entro');
		                	$.ajax({
						        data:{
						        'correo' : correo,
						        'idPedido' : idPedido,
						        'arrayPrecios' : arrayPrecios,
						        'arrayPreciosDscto' : arrayPreciosDscto
						        },
						        type: "POST",
						        url: base_url+'usuario/cotizar/enviarCotizacion', 
						        dataType: "json",
						        success: function(data){
						        	console.log(data);
								 $.confirm({
						            icon: 'email',
						            theme: 'modern',
						            closeIcon: false,
						            animationBounce: 1.5,
						            autoClose: 'Aceptar|3000',
						            type: 'green',
						            title: 'Cotización enviada',
						            columnClass: 'small',
						            content: 'El correo se envio exitosamente',
						            draggable: false,
						            buttons: {
						                Aceptar: {
						                	btnClass: 'btn',                    
						                    action: function (){
						                    	window.location.href = base_url+'usuario/cotizar';
						                    }       
						                }
						            }
						        });      
						        }
						     });
	                    }
	                },
	                Cancelar: {
	                    
	                }
	            }
	        });
		}
    });

	$('#enviarCotizacion2').click(function(){
		var correo = $(this).data('email');
		var idPedido = $(this).data('idpedido');
		var idCotizacion = $(this).data('idcotizacion');
		var Valido = true;

        $('.validacionPrecio input[type="number"]').each(function() {
            if ($(this).val() == '') {
                Valido = false;
            }
        });

        var arrayPrecios= [];
        var arrayPreciosDscto = [];
		var precio;

		$('.valorU').each(function(){
			precio = parseInt($(this).val());
			arrayPrecios.push(precio);
		});

		$('.valorUDscto').each(function(){
			precioDscto = parseInt($(this).val());
			arrayPreciosDscto.push(precioDscto);
		});

		// console.log(correo);
		if (Valido == false){
	    	Materialize.toast('<i class="material-icons left ">warning</i>Coloca los precios del producto', 2000, 'rounded warningToast');
	    }else{
	    	$('#enviarCotizacion2').addClass('disabled');
	      	$.confirm({
            	icon: 'help',
	            theme: 'modern',
	            closeIcon: false,
	            animation: 'scale',
	            type: 'orange',
	            title: 'Enviar cotización editada',
	            columnClass: 'small',
	            content: 'El comprador recibira un correo con los precios ingresados',
	            draggable: false,
	            buttons: {
	                Aceptar: {
	                btnClass: 'btn',
	                    action: function (){

	                    	console.log(arrayPreciosDscto);
	                    	
		                	$.ajax({
						        data:{
						        'correo' : correo,
						        'idPedido' : idPedido,
						        'arrayPrecios' : arrayPrecios,
						        'arrayPreciosDscto' : arrayPreciosDscto,
						        'idCotizacion' : idCotizacion
						        },
						        type: "POST",
						        url: base_url+'usuario/cotizar/enviarCotizacion2', 
						        dataType: "json",
						        success: function(data){
						        	$.confirm({
							            icon: 'email',
							            theme: 'modern',
							            closeIcon: false,
							            animationBounce: 1.5,
							            autoClose: 'Aceptar|3000',
							            type: 'green',
							            title: 'Cotización editada enviada',
							            columnClass: 'small',
							            content: 'El correo se envio exitosamente',
							            draggable: false,
							            buttons: {
							                Aceptar: {
							                	btnClass: 'btn',
							                    action: function (){
							                    	window.location.href = base_url+'usuario/cotizar';
							                    }
							                }
							            }
							        });
						        }
						     });
	                    }
	                },
	                Cancelar: {

	                }
	            }
	        	});
	      	}
        });
		$('#imprimirCotizacion').click(function(){
			$("#datosCotizacion").print(/*options*/);
     	});
		
		$('.calcTotal').click(function(){
			var total = 0;
			var cantidad = 0;
			var pi = 1;
			$('.valorU').each(function(){
				cantidad = parseInt($(this).data('cantidad'));
				totalUni = parseInt($(this).val()) * cantidad;
				total = total + parseInt($(this).val()) * cantidad;
				$('#valU'+pi).text($(this).val());
				$('#importe'+pi).text(totalUni);
				
				pi = pi + 1;
			});
			$('#total').text(total);
			$('#total2').text(total);
	});

	$('.precioigual').click(function(){

		var id_producto = $(this).data('producto');
		var tipo = $(this).data('tipo');
		var id_sku = $(this).data('sku');

		if(tipo == 1){
			var precio = $('#precio'+id_sku).val();	
		}
		else
		{
			var precio = $('#preciod'+id_sku).val();	
		}
		
		if(precio == '')
		{
			Materialize.toast('<i class="material-icons left ">warning</i>Ingrese el precio', 2000, 'rounded warningToast');
		}
		else
		{
			$.confirm({
	        	icon: 'help',
	            theme: 'modern',
	            closeIcon: false,
	            animation: 'scale',
	            type: 'orange',
	            title: '¿El precio es igual para los mismos productos?',
	            columnClass: 'small',
	            content: 'El precio actual se añadirá a los mismos productos, con diferentes variaciones',
	            draggable: false,
	            buttons: {
	                Aceptar: {
	                btnClass: 'btn',
	                    action: function (){

	                    	var variable = tipo+''+id_producto;

	                    	if(tipo == 1){
	                    		$('.producto'+id_producto).each(function(){
									$('.producto'+id_producto).val(precio);
								});
	                    	}
	                    	else
	                    	{
	                    		$('.productoDscto'+id_producto).each(function(){
									$('.productoDscto'+id_producto).val(precio);
								});
	                    	}

							calcular_precio(tipo);
	                    }
	                },
	                Cancelar: {
	                    
	                }
	            }
	        });	
		}
    });

	$('.valorU').keyup(function(){
		
		calcular_precio();		
	});

	$('.valorUDscto').keyup(function(){
		var total = 0;
		var totalUni = 0;
		var cantidad = 0;
		var valActual;
		var totalActual = parseInt($('#totalDscto').text());
		var idActual = $(this).data('idactual');		
	
		$('#importeProDscto'+idActual).text("Subtotal = "+(parseInt($(this).data('cantidad')) * $(this).val()));
		var pi = 1;
		$('.valorUDscto').each(function(){

			cantidad = parseInt($(this).data('cantidad'));

			if ($(this).val()==""){
				valActual = 0;
			}else{
				valActual = $(this).val();
			}
			totalUni = parseInt(valActual) * cantidad;
			total = total + parseInt(valActual) * cantidad;
			$('#valU'+pi).text(valActual);
			//
			$('#importe'+pi).text(totalUni);
			pi = pi + 1;
		});

		$('#totalDscto').text(total.toFixed(2));
		$('#total2').text(total.toFixed(2));
	});

	$('.valorU').focusout(function(){
		
		var precioMinimo = $(this).data('preciominimo');
		var precioMaximo = $(this).data('preciomaximo');
		var idActual = $(this).data('idactual');

		if($(this).val() != '')
		{
			if($(this).val() > precioMaximo || $(this).val() < precioMinimo){
				Materialize.toast('<i class="material-icons left ">warning</i>Precio fue del rango', 2000, 'rounded warningToast');

				$(this).val('');
				$('#importePro'+idActual).text("Subtotal");
			}
		}
	});

	$('.valorUDscto').focusout(function(){
		
		var precioMinimo = $(this).data('preciominimo');
		var precioMaximo = $(this).data('preciomaximo');
		var idActual = $(this).data('idactual');

		if($(this).val() != '')
		{
			if($(this).val() > precioMaximo || $(this).val() < precioMinimo){
				Materialize.toast('<i class="material-icons left ">warning</i>Precio fue del rango', 2000, 'rounded warningToast');

				$(this).val('');
				$('#importeProDscto'+idActual).text("Subtotal Dscto");
			}
		}
	});

	$('#porcentaje').focusout(function(){
		
		var porcentaje = $(this).val();

		if(porcentaje > 0)
		{
			$.confirm({
	        	icon: 'help',
	            theme: 'modern',
	            closeIcon: false,
	            animation: 'scale',
	            type: 'orange',
	            title: 'Cambiar Porcentaje',
	            columnClass: 'small',
	            content: 'El % minimo en una cotización',
	            draggable: false,
	            buttons: {
	                Aceptar: {
	                btnClass: 'btn',
	                    action: function (){
	                    	$.ajax({
						        data:{
						        	'porcentaje' : porcentaje
						        },
						        type: "POST",
						        url: base_url+'usuario/cotizar/cambiarPorcentaje',
						        dataType: "json",
						        success: function(data){
						        	if(data)
						        	{
										Materialize.toast('<i class="material-icons left ">success</i>Actualizado correctamente', 2000, 'rounded warningToast');
						        	}
						        	else
						        	{
										Materialize.toast('<i class="material-icons left ">warning</i>Error', 2000, 'rounded warningToast');						        		
						        	}
						        }
						     });
	                    }
	                },
	                Cancelar: {

	                }
	            }
	        });
		}
		else
		{
			Materialize.toast('<i class="material-icons left ">warning</i>Dato incorrecto', 2000, 'rounded warningToast');
		}
	});
});

function calcular_precio(tipo)
{
	var total = 0;
	var totalUni = 0;
	var valActual;
	var totalActual = parseInt($('#total').text());
	
	$('.valorU').each(function(){

		var id_sku = $(this).data('idactual');
		var cantidad = $(this).data('cantidad');
		var precio = $(this).val();

		cantidad = parseInt(cantidad);

		if (precio == ""){
			valActual = 0;
		}else{
			valActual = precio;
		}

		totalUni = parseInt(valActual) * cantidad;
		total = total + totalUni;

		if(tipo == 1)
		{
			$('#valU'+id_sku).text(valActual);
			$('#importePro'+id_sku).text("Subtotal = "+totalUni);
		}
		else
		{
			$('#valorUDscto'+id_sku).text(valActual);
			$('#importeProDscto'+id_sku).text("Subtotal = "+totalUni);
		}
	});

	$('#total').text(total.toFixed(2));
	$('#total2').text(total.toFixed(2));
}