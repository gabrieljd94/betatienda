$(document).ready(function(){
  $('.materialboxed').materialbox();  
  $('#modal_resumenCotizacion').modal();

  $('.detalleProducto').click(function(){
    var Pro_IdProducto = $(this).data('idproducto');
    window.location.href = base_url+'tienda/productos/dp/'+Pro_IdProducto;
  });

  $('#selectVarPro').change(function(){
      var idVariacion = $(this).val();
      console.log(idVariacion);
       $('#listProducto').load(base_url+'tienda/productos/filtro_producto_variacion', {idVariacion: idVariacion});
    });
   

   $('.enviarComentario').click(function(){
    var comentario = $('textarea#comentarioPro').val();
    var Pro_IdProducto = $(this).data('idproducto');
    var Usu_IdUsuario = $(this).data('usuario');
    if (comentario == '') {
      Materialize.toast('<i class="material-icons left ">warning</i>Escribe tu pregunta', 2000, 'rounded warningToast');
    } else {
      $.ajax({
        data:{
          'comentario':comentario,
          'Pro_IdProducto':Pro_IdProducto,
          'Usu_IdUsuario':Usu_IdUsuario
        },
        method:"POST",
        url: base_url+'tienda/productos/comentario',
        success:function(data)
        {
          Materialize.toast('<i class="material-icons left">check_circle</i> Comentario enviado', 2000, 'rounded successToast');
          $('#divComentario').load(base_url+'tienda/productos/comentarioVer',{data:data});
          $('textarea#comentarioPro').val('');
        }
      });
    }

  
 });

  $('.cantSku').change(function(){
  totalSku = 0;
  var valor = 0;
    $('.cantSku').each(function()
    {
       if ($(this).val() > 0) {
      valor = parseInt($(this).val()); 
      totalSku = totalSku+valor;
      } 
    });
      console.log(totalSku);

    $('#totalcantidad').text(totalSku);
 });
 
 $('.add_cart').click(function(){

  // $(this).addClass('green darken-4');

  var product_id = [];
  var product_name = [];
  var product_price = [];
  var variacion = [];
  var unidad =[];
  var img = [];
  var color =[];
  var tipovariacion = [];
  var idtienda = [];
  var quantity = [];
  var siproducto = 0;

  $('.cantSku').each(function(){
    if ($(this).val() > 0) {
      siproducto = 1;
      product_id.push($(this).data("productid"));
      product_name.push($(this).data("productname"));
      product_price.push($(this).data("price"));
      variacion.push($(this).data("variacion"));
      unidad.push($(this).data("unidad"));
      img.push($(this).data("img"));
      color.push($(this).data("color"));
      tipovariacion.push($(this).data("tipovariacion"));
      idtienda.push($(this).data("idtienda"));
      quantity.push($(this).val());
      }
  });
   

  if(siproducto == 1)
  {
   $.ajax({
    url: base_url+'tienda/productos/add',
    method:"POST",
    data:{product_id:product_id, product_name:product_name, product_price:product_price, quantity:quantity, 
      variacion:variacion,img:img,unidad:unidad, idtienda:idtienda, color:color, tipovariacion:tipovariacion},
    success:function(data)
    {
     Materialize.toast('<i class="material-icons left ">check_circle</i> Agregado al carrito',2000);

     $('#cart_details').html(data);
     $('.cantSku').val('');
     $('#totalcantidad').text('');
    }
   });
  }
  else
  {
    Materialize.toast('<i class="material-icons left ">warning</i>Coloca una cantidad', 2000, 'rounded warningToast');
  }
 });
 $('#cart_details').load( base_url+'tienda/productos/load');
 
 $(document).on('click', '.remove_inventory', function(){
    var row_id = $(this).attr("id");
    $.confirm({
      icon: 'help',
      theme: 'modern',
      closeIcon: false,
      animation: 'scale',
      type: 'orange',
      title: 'Eliminar de pedido',
      columnClass: 'small',
      content: 'Eliminar producto del pedido actual',
      draggable: false,
      buttons: {
          Eliminar:    {   
              btnClass: 'btn',                 
              action: function (){
                $.ajax({
                  url: base_url+'tienda/productos/remove',
                  method:"POST",
                  data:{row_id:row_id},
                  success:function(data)
                  {
                    Materialize.toast('<i class="material-icons left ">check_circle</i>Producto eliminado del pedido', 2000, 'rounded successToast');
                   $('#cart_details').html(data);
                  }
                });
              }       
          },
          Cancelar: {
              
          }
      }
    });
 });

 $(document).on('click', '.clear_cart', function(){
  $.confirm({
      icon: 'help',
      theme: 'modern',
      closeIcon: false,
      animation: 'scale',
      type: 'red',
      title: 'Vaciar pedido',
      columnClass: 'small',
      content: 'Eliminar todos los productos del pedido actual',
      draggable: false,
      buttons: {
          Vaciar:    {   
              btnClass: 'btn',                 
              action: function (){
                $.ajax({
                  url:base_url+'tienda/productos/clear',
                  success:function(data)
                  {
                   Materialize.toast('<i class="material-icons left ">check_circle</i>Pedido vaciado', 2000, 'rounded successToast');
                   $('#cart_details').html(data);
                  }
                 });
              }       
          },
          Cancelar: {
              
          }
      }
    });
  });
});