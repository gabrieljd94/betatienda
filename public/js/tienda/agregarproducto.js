$(document).ready(function(){
	// $('select').material_select();
	$("#catSelect").change(function(){
		var Cat_IdCategoria = $(this).val();
		 $.ajax({
		 	data:{
			'Cat_IdCategoria' : Cat_IdCategoria
			},
		    type: "POST",
		    url: base_url+'usuario/inventario/select_subc', 
		    dataType: "json",
		    success: function(data){
				$('#divSucSelect').load(base_url+'usuario/inventario/Select_subCategoria',{datos:data});
		    },
		    error: function(data) {
		      alert('error');
		    }
		  });
 
	});
	$("#unmSelect").change(function(){
		if ($(this).val() == 3 || $(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6 ) {
			$('#valorUnm').removeClass().addClass('input-field col s12 l6');
		}
		else
		{
			$('#valorUnm').addClass('hide');
		}
	
	});
	$('.addMasVar').click(function(){
	  var Des_IdDetalle_SubCategoria = $(this).data('iddsc');
	  var Pro_IdProducto = $(this).data('idpro');
	  $.ajax({
        data:{
        'Des_IdDetalle_SubCategoria' : Des_IdDetalle_SubCategoria,
        'Pro_IdProducto' : Pro_IdProducto
        },
        type: "POST",
        url: base_url+'usuario/inventario/variaciones_categorias', 
        dataType: "json",
        success: function(data){
            console.log(data);
            $('#aggVar2').load(base_url+'usuario/inventario/var_cat2',{datos:data,Des_IdDetalle_SubCategoria:Des_IdDetalle_SubCategoria,Pro_IdProducto:Pro_IdProducto});
        },
        error: function(data) {
          alert('error');
        }
      });
	});
	 let editor;
	    ClassicEditor
	        .create( document.querySelector( '#descripcionProducto' ),{
	        	toolbar: [ 'heading', '|', 'italic', 'numberedList', 'blockQuote' ]
	        } )
	        .then( newEditor => {
	            editor = newEditor;
	        } )
	        .catch( error => { 
	            console.error( error );
	        } );
	
	$('.addProductoBD').click(function(){
		var NombreProducto = $('#nombreProducto').val();
		var DescProducto = editor.getData();
		var Categoria = $('#catSelect > option:selected').val();
		var SubCategoria = $('#sucSelect > option:selected').val();
		var SubCategoriaDetalle = $('#sucDetalle > option:selected').val()
		var Unidad = $('#unmSelect > option:selected').val();
		var valorUnm = $('#valorUnm2').val();
		var precioMin = $('#precioMin').val();
		var precioMax = $('#precioMax').val();
		var files = $('.filePro')[0].files;
		var listVar = $("#lista_variacion").text();
		var combId = [];
		var combCantidad = [];
		 $(".cntVar").each(function(){
		         combCantidad.push($(this).val());
		         combId.push($(this).data('idcom'));
		    });
		if ( NombreProducto == '' ||  DescProducto == '' ||  Categoria == '' || 
		 SubCategoria == '' ||  SubCategoriaDetalle == '' ||  Unidad == ''  || files.length == 0 ) {
			Materialize.toast('Complete todos los campos', 3000, 'rounded');
		} else if (listVar == '') {
			Materialize.toast('Agregue al menos 1 variación de su producto', 3000, 'rounded');
		} else if (precioMin > precioMax || precioMax < precioMin || precioMin == ''  ||  precioMax == '') {	
			Materialize.toast('El precio minimo debe ser menos al precio mayor', 3000, 'rounded');
		} else {	
		var valorUnidad = fvalorUnidad(Unidad,valorUnm);
		$.ajax({
		 	data:{
			'Pro_Nombre' : NombreProducto,
			'Pro_Descripcion' : DescProducto,
			'Cat_IdCategoria' : Categoria,
			'Suc_IdSubCategoria' : SubCategoria,
			'Des_IdDetalle_SubCategoria' : SubCategoriaDetalle,
			'Unidad' : Unidad,
			'valorUnm' : valorUnidad,
			'precioMin' : precioMin,
			'precioMax' : precioMax,
			'combId' : combId,
			'combCantidad' : combCantidad
			},
		    type: "POST",
		    url: base_url+'usuario/inventario/agregar_producto', 
		    success: function(r){
		    	console.log(r);
				var files = $('.filePro')[0].files;		
		  		var error = '';
		  		var form_data = new FormData();
				  for(var count = 0; count<files.length; count++)
				  {
				   var name = files[count].name;
				   var extension = name.split('.').pop().toLowerCase();
				   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
				   {
				    error += "Imagen  " + count + " invalida";
				    Materialize.toast('Agrege una imagen correcta', 3000, 'rounded');
				   }
				   else
				   {
				    form_data.append("files[]", files[count]);
				   }
				  }
				  if(error == '')
				  {
				   $.ajax({
				    url: base_url + 'usuario/inventario/subirImagen/'+r,
				    method:"POST",
				    data:form_data,
				    contentType:false,
				    cache:false,
				    processData:false,
				    beforeSend:function()
				    {				  		
		    		 $("#mainProducto").html('<br><br><br><br><br><div class="progress"><div class="indeterminate blue"></div></div>');
				    },
				    success:function(r1)
				    {
				     window.location.href = base_url+'usuario/inventario/productoDetalle/'+r1;
				    }
				   })
				  }
				  else
				  {
				   alert(error);
				  }
		    },
		    error: function(data) {
		      alert('error 222');
		    }
		  });
		}
	});	
	$('.upImgSku').change(function(){
	var SKU_IdSku = $(this).data('id');
	Pro_IdProducto = $(this).data('idpro');
	var SKU_Nombre = $(this).data('sku');
	console.log(SKU_IdSku,SKU_Nombre);
	  var files = $('#sku'+SKU_IdSku)[0].files;
	  console.log(files);
	  var error = '';
	  var form_data = new FormData();
	  for(var count = 0; count<files.length; count++)
	  {
	   var name = files[count].name;
	   var extension = name.split('.').pop().toLowerCase();
	   if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
	   {
	    error += "Imagen  " + count + " invalida"
	   }
	   else
	   {
	    form_data.append("files[]", files[count]);
	   }
	  }
	  if(error == '')
	  {
	   $.ajax({
	    url: base_url + 'usuario/inventario/subirImagenSku/'+SKU_IdSku,
	    method:"POST",
	    data:form_data,
	    contentType:false,
	    cache:false,
	    processData:false,
	    beforeSend:function()
	    {
	    	$('#loadSkuImg'+SKU_IdSku).removeClass();
	    	$('#skuImg'+SKU_IdSku).addClass('hide');
	    },
	    success:function(r)
	    {
	    	// $('#loadSkuImg').removeClass().addClass('preloader-wrapper big active');
	      window.location.href = base_url+'usuario/inventario/productoDetalle/'+Pro_IdProducto;
	    }
	   })
	  }
	  else
	  {
	   alert(error);
	  }
	 });
   });	
	function fvalorUnidad(idUnidad, vUnidad){
		var valorUnidad = 0;
		if (idUnidad == 1) {
			valorUnidad = 1;
		} else if (idUnidad == 2){
			valorUnidad = 12;
		
		} else if (idUnidad == 3){	
			if (vUnidad == '') {
				valorUnidad = 1;
			}else{
				valorUnidad = vUnidad;
			}
		} else if (idUnidad == 4){
			if (vUnidad == '') {
				valorUnidad = 1;
			}else{
				valorUnidad = vUnidad;
			}
		} else if (idUnidad == 5){
			if (vUnidad == '') {
				valorUnidad = 1;
			}else{
				valorUnidad = vUnidad;
			}
		} else if (idUnidad == 6){	
			if (vUnidad == '') {
				valorUnidad = 1;
			}else{
				valorUnidad = vUnidad;
			}
		} else if (idUnidad == 7){
			valorUnidad = 100;
		} else if (idUnidad == 8){
			valorUnidad = 1000;
		} else if (idUnidad == 9){
			valorUnidad = 1;
		} else if (idUnidad == 10){
			valorUnidad = 1;
		} else if (idUnidad == 11){	
			valorUnidad = 1;
		} else if (idUnidad == 12){
			valorUnidad = 1;
		} else if (idUnidad == 13){
			valorUnidad = 1;
		} else if (idUnidad == 14){
			valorUnidad = 1;
		}
		return valorUnidad;
	}
