$(document).ready(function(){
  $('.collapsible').collapsible();

  $('.cbProducto').click(function(){
    calcular_totales();
  });

  $('.cantidad').focusout(function(){
    calcular_totales();
  });

  $('#btnProcesarCompra').click(function(){

    var valido = false;
    var array_compra = [];

    var tipo_precio = $("#tdTotalDscto").hasClass("tachado");

    if(tipo_precio){
      var total = $('#totalReal').val();
    }
    else
    {
      var total = $('#totalDscto').val();
    }
    
    var i = 0;

    // var total_compra = 0;

    $('.cbProducto').each(function(){
      if($(this).is(':checked')){
        var id_sku = $(this).val();
        var cantidad = $('#cantidad'+id_sku).val();

        if(tipo_precio){
          var precio = $('#precioventa'+id_sku).text();
        }
        else{
          var precio = $('#preciocotizado'+id_sku).text();
        }

        array_compra[i] = [id_sku, precio, cantidad];

        i++;
        // total_compra = precio*cantidad + total;
        valido = true;
      }
    });

    if(valido)
    {
      if($("#tdTotalDscto").hasClass("tachado"))
      {
        var total_compra = $("#totalDscto").text()
      }
      else
      {
        var total_compra = $("#totalReal").text()
      }

      $.confirm({
        icon: 'help',
          theme: 'modern',
          closeIcon: false,
          animation: 'scale',
          type: 'orange',
          title: 'Procesar Compra',
          columnClass: 'small',
          content: '¿Estas seguro de procesar la compra?',
          draggable: false,
          buttons: {
            Aceptar: {
            btnClass: 'btn',
            action: function (){
              $.ajax({
                data:{
                  'array_compra' : array_compra,
                  'total': total_compra,
                  'id_cotizacion': $('#id_cotizacion').val()
                },
                type: "POST",
                url: base_url+'comprador/compra/ajax_procesar_compra', 
                dataType: "json",
                success: function(data){
                  if(data){
                    $.confirm({
                      icon: 'email',
                      theme: 'modern',
                      closeIcon: false,
                      animationBounce: 1.5,
                      autoClose: 'Aceptar|3000',
                      type: 'green',
                      title: 'Compra exitosa',
                      columnClass: 'small',
                      content: 'Compra realizada correctamente',
                      draggable: false,
                      buttons: {
                        Aceptar: {
                          btnClass: 'btn',
                          action: function (){
                            window.location.href = base_url+'comprador/compra/detalle/'+data;
                          }
                        }
                      }
                    });
                  }
                  else{
                    $.confirm({
                      icon: 'close',
                      theme: 'modern',
                      closeIcon: false,
                      animationBounce: 1.5,
                      autoClose: 'Aceptar|3000',
                      type: 'danger',
                      title: 'Error en la compra',
                      columnClass: 'small',
                      content: 'Ocurrió un error, intente nuevamente',
                      draggable: false,
                      buttons: {
                        Aceptar: {
                          btnClass: 'btn',
                          action: function (){
                            window.location.href = base_url+'comprador/index/dCotizacion';
                          }
                        }
                      }
                    });
                  }
                  
                }
             });
            }
          },
          Cancelar: {
                  
          }
        }
      });
    }
    else
    {
      Materialize.toast('<i class="material-icons left">warning</i>Ningún producto esta seleccionado', 2000, 'rounded warningToast');
    }
  });
});

function calcular_totales(){
  var totalCotizado = calcular_total('cotizado');
  var totalCotizado_ = $('#totalDscto_').val();
  var porcentaje = $('#porcentaje').val()/100;

  if(totalCotizado < totalCotizado_*porcentaje){
    $('.cbProducto').each(function(){
      var id_sku = $(this).val();
      var cantidad = $('#cantidad'+id_sku).val();

      if($(this).is(':checked')){
        $('#precioventa'+id_sku).removeClass('tachado');
        $('#preciocotizado'+id_sku).addClass('tachado');
        $('#tdimporte'+id_sku).removeClass('tachado');
        $('#cantidad'+id_sku).prop( "disabled", false );
      }
      else{
        $('#precioventa'+id_sku).addClass('tachado');
        $('#preciocotizado'+id_sku).addClass('tachado');
        $('#tdimporte'+id_sku).addClass('tachado');
        $('#cantidad'+id_sku).prop( "disabled", true );
      }

      var importe = $(this).data('precioventa') * cantidad;
      $('#importe'+id_sku).text(importe.toFixed(2));
    });

    $('#tdTotalReal').removeClass('tachado');
    $('#tdTotalDscto').addClass('tachado');
    $('#tdTotalAhorrado').addClass('tachado');
  }
  else{
    $('.cbProducto').each(function(){
      var id_sku = $(this).val();
      var cantidad = $('#cantidad'+id_sku).val();

      if($(this).is(':checked')){
        $('#precioventa'+id_sku).addClass('tachado');
        $('#preciocotizado'+id_sku).removeClass('tachado');
        $('#tdimporte'+id_sku).removeClass('tachado');
        $('#cantidad'+id_sku).prop( "disabled", false );
      }
      else{
        $('#precioventa'+id_sku).addClass('tachado');
        $('#preciocotizado'+id_sku).addClass('tachado');
        $('#tdimporte'+id_sku).addClass('tachado');
        $('#cantidad'+id_sku).prop( "disabled", true );
      }

      var importe = $(this).data('preciocotizado') * cantidad
      $('#importe'+id_sku).text(importe.toFixed(2));
    });

    $('#tdTotalReal').addClass('tachado');
    $('#tdTotalDscto').removeClass('tachado');
    $('#tdTotalAhorrado').removeClass('tachado');
  }

  var totalReal = calcular_total("real");
  var totalDscto = calcular_total("cotizado");
  $('#totalReal').text(totalReal.toFixed(2));
  $('#totalDscto').text(totalDscto.toFixed(2));
  $('#totalAhorrado').text((totalReal-totalDscto).toFixed(2));
}

function calcular_total(tipo_precio){

  var total = 0;
  var valActual; 

  $('.cbProducto').each(function(){

    var id_sku = $(this).data('id');

    if($(this).is(':checked')){
      if(tipo_precio == 'cotizado'){
        valActual = $(this).data('preciocotizado');
      }
      else{
        valActual = $(this).data('precioventa');
      }
    }
    else{
      valActual = 0;
    }

    var cantidad = $('#cantidad'+id_sku).val();

    total = total + parseInt(valActual) * cantidad;
  });

  return total;
}